# German translation of the Debian webwml modules
# Copyright (c) 2004 Software in the Public Interest, Inc.
# Dr. Tobias Quathamer <toddy@debian.org>, 2004, 2005, 2006, 2016, 2019.
# Helge Kreutzmann <debian@helgefjell.de>, 2007, 2008.
# Holger Wansing <hwansing@mailbox.org>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml templates\n"
"PO-Revision-Date: 2020-04-08 20:37+0100\n"
"Last-Translator: Holger Wansing <hwansing@mailbox.org>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../../english/template/debian/votebar.wml:13
msgid "Date"
msgstr "Datum"

#: ../../english/template/debian/votebar.wml:16
msgid "Time Line"
msgstr "Zeitrahmen"

#: ../../english/template/debian/votebar.wml:19
msgid "Summary"
msgstr "Zusammenfassung"

#: ../../english/template/debian/votebar.wml:22
msgid "Nominations"
msgstr "Nominationen"

#: ../../english/template/debian/votebar.wml:25
msgid "Withdrawals"
msgstr "Rücktritte"

#: ../../english/template/debian/votebar.wml:28
msgid "Debate"
msgstr "Debatte"

#: ../../english/template/debian/votebar.wml:31
msgid "Platforms"
msgstr "Wahlreden"

#: ../../english/template/debian/votebar.wml:34
msgid "Proposer"
msgstr "Antragsteller"

#: ../../english/template/debian/votebar.wml:37
msgid "Proposal A Proposer"
msgstr "Antragsteller von Vorschlag A"

#: ../../english/template/debian/votebar.wml:40
msgid "Proposal B Proposer"
msgstr "Antragsteller von Vorschlag B"

#: ../../english/template/debian/votebar.wml:43
msgid "Proposal C Proposer"
msgstr "Antragsteller von Vorschlag C"

#: ../../english/template/debian/votebar.wml:46
msgid "Proposal D Proposer"
msgstr "Antragsteller von Vorschlag D"

#: ../../english/template/debian/votebar.wml:49
msgid "Proposal E Proposer"
msgstr "Antragsteller von Vorschlag E"

#: ../../english/template/debian/votebar.wml:52
msgid "Proposal F Proposer"
msgstr "Antragsteller von Vorschlag F"

#: ../../english/template/debian/votebar.wml:55
msgid "Proposal G Proposer"
msgstr "Antragsteller von Vorschlag G"

#: ../../english/template/debian/votebar.wml:58
msgid "Proposal H Proposer"
msgstr "Antragsteller von Vorschlag H"

#: ../../english/template/debian/votebar.wml:61
msgid "Seconds"
msgstr "Unterstützer"

#: ../../english/template/debian/votebar.wml:64
msgid "Proposal A Seconds"
msgstr "Unterstützer von Vorschlag A"

#: ../../english/template/debian/votebar.wml:67
msgid "Proposal B Seconds"
msgstr "Unterstützer von Vorschlag B"

#: ../../english/template/debian/votebar.wml:70
msgid "Proposal C Seconds"
msgstr "Unterstützer von Vorschlag C"

#: ../../english/template/debian/votebar.wml:73
msgid "Proposal D Seconds"
msgstr "Unterstützer von Vorschlag D"

#: ../../english/template/debian/votebar.wml:76
msgid "Proposal E Seconds"
msgstr "Unterstützer von Vorschlag E"

#: ../../english/template/debian/votebar.wml:79
msgid "Proposal F Seconds"
msgstr "Unterstützer von Vorschlag F"

#: ../../english/template/debian/votebar.wml:82
msgid "Proposal G Seconds"
msgstr "Unterstützer von Vorschlag G"

#: ../../english/template/debian/votebar.wml:85
msgid "Proposal H Seconds"
msgstr "Unterstützer von Vorschlag H"

#: ../../english/template/debian/votebar.wml:88
msgid "Opposition"
msgstr "Opposition"

#: ../../english/template/debian/votebar.wml:91
msgid "Text"
msgstr "Text"

#: ../../english/template/debian/votebar.wml:94
msgid "Proposal A"
msgstr "Vorschlag A"

#: ../../english/template/debian/votebar.wml:97
msgid "Proposal B"
msgstr "Vorschlag B"

#: ../../english/template/debian/votebar.wml:100
msgid "Proposal C"
msgstr "Vorschlag C"

#: ../../english/template/debian/votebar.wml:103
msgid "Proposal D"
msgstr "Vorschlag D"

#: ../../english/template/debian/votebar.wml:106
msgid "Proposal E"
msgstr "Vorschlag E"

#: ../../english/template/debian/votebar.wml:109
msgid "Proposal F"
msgstr "Vorschlag F"

#: ../../english/template/debian/votebar.wml:112
msgid "Proposal G"
msgstr "Vorschlag G"

#: ../../english/template/debian/votebar.wml:115
msgid "Proposal H"
msgstr "Vorschlag H"

#: ../../english/template/debian/votebar.wml:118
msgid "Choices"
msgstr "Auswahl"

#: ../../english/template/debian/votebar.wml:121
msgid "Amendment Proposer"
msgstr "Antragsteller der Ergänzung"

#: ../../english/template/debian/votebar.wml:124
msgid "Amendment Seconds"
msgstr "Unterstützer der Ergänzung"

#: ../../english/template/debian/votebar.wml:127
msgid "Amendment Text"
msgstr "Text der Ergänzung"

#: ../../english/template/debian/votebar.wml:130
msgid "Amendment Proposer A"
msgstr "Antragsteller A der Ergänzung"

#: ../../english/template/debian/votebar.wml:133
msgid "Amendment Seconds A"
msgstr "Unterstützer A der Ergänzung"

#: ../../english/template/debian/votebar.wml:136
msgid "Amendment Text A"
msgstr "Text A der Ergänzung"

#: ../../english/template/debian/votebar.wml:139
msgid "Amendment Proposer B"
msgstr "Antragsteller B der Ergänzung"

#: ../../english/template/debian/votebar.wml:142
msgid "Amendment Seconds B"
msgstr "Unterstützer B der Ergänzung"

#: ../../english/template/debian/votebar.wml:145
msgid "Amendment Text B"
msgstr "Text B der Ergänzung"

#: ../../english/template/debian/votebar.wml:148
msgid "Amendment Proposer C"
msgstr "Antragsteller C der Ergänzung"

#: ../../english/template/debian/votebar.wml:151
msgid "Amendment Seconds C"
msgstr "Unterstützer C der Ergänzung"

#: ../../english/template/debian/votebar.wml:154
msgid "Amendment Text C"
msgstr "Text C der Ergänzung"

#: ../../english/template/debian/votebar.wml:157
msgid "Amendments"
msgstr "Ergänzungen"

#: ../../english/template/debian/votebar.wml:160
msgid "Proceedings"
msgstr "Fortgänge"

#: ../../english/template/debian/votebar.wml:163
msgid "Majority Requirement"
msgstr "Mehrheitsanforderung"

#: ../../english/template/debian/votebar.wml:166
msgid "Data and Statistics"
msgstr "Daten und Statistiken"

#: ../../english/template/debian/votebar.wml:169
msgid "Quorum"
msgstr "Mindestanzahl"

#: ../../english/template/debian/votebar.wml:172
msgid "Minimum Discussion"
msgstr "Mindestens erforderliche Diskussion"

#: ../../english/template/debian/votebar.wml:175
msgid "Ballot"
msgstr "Stimmzettel"

#: ../../english/template/debian/votebar.wml:178
msgid "Forum"
msgstr "Forum"

#: ../../english/template/debian/votebar.wml:181
msgid "Outcome"
msgstr "Ergebnis"

#: ../../english/template/debian/votebar.wml:185
msgid "Waiting&nbsp;for&nbsp;Sponsors"
msgstr "Auf Unterstützung wartend"

#: ../../english/template/debian/votebar.wml:188
msgid "In&nbsp;Discussion"
msgstr "In Diskussion"

#: ../../english/template/debian/votebar.wml:191
msgid "Voting&nbsp;Open"
msgstr "In der Abstimmung"

#: ../../english/template/debian/votebar.wml:194
msgid "Decided"
msgstr "Entschieden"

#: ../../english/template/debian/votebar.wml:197
msgid "Withdrawn"
msgstr "Zurückgezogen"

#: ../../english/template/debian/votebar.wml:200
msgid "Other"
msgstr "Sonstiges"

#: ../../english/template/debian/votebar.wml:204
msgid "Home&nbsp;Vote&nbsp;Page"
msgstr "Home&nbsp;Abstimmungsseite"

#: ../../english/template/debian/votebar.wml:207
msgid "How&nbsp;To"
msgstr "Wie"

#: ../../english/template/debian/votebar.wml:210
msgid "Submit&nbsp;a&nbsp;Proposal"
msgstr "Vorschlag&nbsp;einsenden"

#: ../../english/template/debian/votebar.wml:213
msgid "Amend&nbsp;a&nbsp;Proposal"
msgstr "Vorschlag&nbsp;ergänzen"

#: ../../english/template/debian/votebar.wml:216
msgid "Follow&nbsp;a&nbsp;Proposal"
msgstr "Vorschlag&nbsp;verfolgen"

#: ../../english/template/debian/votebar.wml:219
msgid "Read&nbsp;a&nbsp;Result"
msgstr "Resultat&nbsp;lesen"

#: ../../english/template/debian/votebar.wml:222
msgid "Vote"
msgstr "Abstimmen"
