# German translation of the Debian webwml modules
# Copyright ©
# Dr. Tobias Quathamer <toddy@debian.org>, 2017.
# Holger Wansing <hwansing@mailbox.org>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2022-05-23 20:36+0200\n"
"Last-Translator: Holger Wansing <hwansing@mailbox.org>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.0\n"

#: ../../english/blends/blend.defs:15
msgid "Metapackages"
msgstr "Metapakete"

#: ../../english/blends/blend.defs:18
msgid "Downloads"
msgstr "Downloads"

#: ../../english/blends/blend.defs:21
msgid "Derivatives"
msgstr "Derivate"

#: ../../english/blends/released.data:15
msgid ""
"The goal of Debian Astro is to develop a Debian based operating system that "
"fits the requirements of both professional and hobby astronomers. It "
"integrates a large number of software packages covering telescope control, "
"data reduction, presentation and other fields."
msgstr ""
"Das Ziel von Debian Astro ist, ein Debian-basiertes Betriebssystem zu"
" entwickeln, "
"das die Anforderungen sowohl von professionellen wie auch von Hobbyastronomen "
"erfüllt. Es enthält eine große Zahl von Software-Paketen für"
" Teleskopsteuerung, "
"Datenreduktion, Präsentation und andere Bereiche."

#: ../../english/blends/released.data:23
msgid ""
"The goal of DebiChem is to make Debian a good platform for chemists in their "
"day-to-day work."
msgstr ""
"Das Ziel von DebiChem ist, mit Debian eine gute Plattform für Chemiker bei "
"ihrer täglichen Arbeit zur Verfügung zu stellen."

#: ../../english/blends/released.data:31
msgid ""
"The goal of Debian Games is to provide games in Debian from arcade and "
"adventure to simulation and strategy."
msgstr ""
"Das Ziel von Debian Games ist, Spiele für Debian bereitzustellen: von "
"Spielhallenklassikern über Adventures bis hin zu Simulationen und "
"Strategiespielen."

#: ../../english/blends/released.data:39
msgid ""
"The goal of Debian Edu is to provide a Debian OS system suitable for "
"educational use and in schools."
msgstr ""
"Das Ziel von Debian Edu ist, ein Debian-Betriebssystem zu erstellen, das für "
"Bildungszwecke und Schulen geeignet ist."

#: ../../english/blends/released.data:47
msgid ""
"The goal of Debian GIS is to develop Debian into the best distribution for "
"Geographical Information System applications and users."
msgstr ""

#: ../../english/blends/released.data:57
msgid ""
"The goal of Debian Junior is to make Debian an OS that children will enjoy "
"using."
msgstr ""
"Das Ziel von Debian Junior ist, aus Debian ein Betriebssystem zu machen, das "
"Kinder gerne verwenden."

#: ../../english/blends/released.data:65
msgid ""
"The goal of Debian Med is a complete free and open system for all tasks in "
"medical care and research. To achieve this goal Debian Med integrates "
"related free and open source software for medical imaging, bioinformatics, "
"clinic IT infrastructure, and others within the Debian OS."
msgstr ""
"Das Ziel von Debian Med ist ein komplett freies und offenes System für "
"alle Aufgabenbereiche in medizinischer Pflege und Forschung. Dazu integriert "
"Debian Med freie und quelloffene Software für medizinische Bildgebung, "
"Bioinformatik, klinische IT-Infrastruktur und weiteres in das"
" Debian-Betriebssystem."

#: ../../english/blends/released.data:73
msgid ""
"The goal of Debian Multimedia is to make Debian a good platform for audio "
"and multimedia work."
msgstr ""
"Das Ziel von Debian Multimedia ist, mit Debian eine gute Plattform für "
"Audio- und Multimedia-Arbeiten zur Verfügung zu stellen."

#: ../../english/blends/released.data:81
msgid ""
"The goal of Debian Science is to provide a better experience when using "
"Debian to researchers and scientists."
msgstr ""
"Das Ziel von Debian Science ist, Forschern und Wissenschaftlern bessere"
" Nutzererfahrungen bei der "
"Verwendung von Debian zu ermöglichen."

#: ../../english/blends/released.data:89
msgid ""
"The goal of FreedomBox is to develop, design and promote personal servers "
"running free software for private, personal communications. Applications "
"include blogs, wikis, websites, social networks, email, web proxy and a Tor "
"relay on a device that can replace a wireless router so that data stays with "
"the users."
msgstr ""
"Ziele von FreedomBox sind Entwicklung, Design und Förderung von "
"Personal-Servern, auf denen freie Software für private, persönliche"
" Kommunikation läuft. "
"Zu den Anwendungen gehören Blogs, Wikis, Websites, soziale Netzwerke, E-Mail, "
"Web-Proxys und ein Tor-Relay auf einem Gerät, das einen WLAN-Router ersetzen "
"kann, so dass die Daten bei dem Benutzer bleiben."

#: ../../english/blends/unreleased.data:15
msgid ""
"The goal of Debian Accessibility is to develop Debian into an operating "
"system that is particularly well suited for the requirements of people with "
"disabilities."
msgstr ""
"Das Ziel von Debian Accessibility ist, Debian in ein Betriebssystem zu "
"verwandeln, das speziell angepasst ist auf die Anforderungen von Leuten "
"mit Behinderungen."

#: ../../english/blends/unreleased.data:23
msgid ""
"The goal of Debian Design is to provide applications for designers. This "
"includes graphic design, web design and multimedia design."
msgstr ""
"Ziel von Debian Design ist die Bereitstellung von Anwendungen für Designer. "
"Dazu gehört Grafik-, Web- und Multimedia-Design."

#: ../../english/blends/unreleased.data:30
msgid ""
"The goal of Debian EzGo is to provide culture-based open and free technology "
"with native language support and appropriate user friendly, lightweight and "
"fast desktop environment for low powerful/cost hardwares to empower human "
"capacity building and technology development  in many areas and regions, "
"like Africa, Afghanistan, Indonesia, Vietnam  using Debian."
msgstr ""

#: ../../english/blends/unreleased.data:38
msgid ""
"The goal of Debian Hamradio is to support the needs of radio amateurs in "
"Debian by providing logging, data mode and packet mode applications and more."
msgstr ""
"Das Ziel von Debian Hamradio ist, die Anforderungen von Radioamateuren "
"in Debian zu erfüllen; dazu werden unter anderem Logging-, Datenmodus- und"
" Paketmodus-Anwendungen "
"bereitgestellt."

#: ../../english/blends/unreleased.data:47
msgid ""
"The goal of DebianParl is to provide applications to support the needs of "
"parliamentarians, politicians and their staffers all around the world."
msgstr ""
"Das Ziel von DebianParl ist die Bereitstellung von Anwendungen für "
"Parlamentarier, Politiker und deren Mitarbeiter rund um den Globus."
