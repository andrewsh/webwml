#use wml::debian::translation-check translation="c670c4f1f13445c8c1a86684f531e34c0b9d4db6" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Wordpress, un outil de
blog. Elles permettaient à des attaquants distants de réaliser des
injections de code SQL, d'exécuter des requêtes SQL non vérifiées, de
contourner le durcissement ou de réaliser des attaques par script intersite
(XSS).</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 4.7.22+dfsg-0+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wordpress.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de wordpress, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/wordpress">\
https://security-tracker.debian.org/tracker/wordpress</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2884.data"
# $Id: $
