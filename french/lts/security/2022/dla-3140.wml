#use wml::debian::translation-check translation="ed44ef7034a05481f0cf6538b3890c83910bacb7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il y avait une possible vulnérabilité d'injection SQL dans libpgjava,
une bibliothèque Java pour la connexion à des bases de données PostgreSQL.</p>

<p>Un utilisateur malveillant pouvait avoir contrefait un schéma qui
faisait exécuter à une application des commandes en tant qu'utilisateur
privilégié du fait de l'absence d'échappement des noms de colonnes dans
certaines opérations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-31197">CVE-2022-31197</a>

<p>Le pilote JDBC de PostgreSQL (PgJDBC pour abréger) permet aux programmes
Java de se connecter à une base de données PostgreSQL en utilisant du code
Java standard indépendant de la base de données. L'implémentation de PGJDBC
de la méthode <q>java.sql.ResultRow.refreshRow()</q> ne réalise pas
l'échappement des noms de colonnes de sorte qu'un nom de colonne malveillant
qui contient un terminateur d'instruction, par exemple <q>;</q>, pouvait
conduire à une injection SQL. Cela pouvait conduire à l'exécution de
commandes SQL supplémentaires en tant qu'utilisateur JDBC de l'application.
Les applications de l'utilisateur qui n'invoquent pas la méthode
<q>ResultSet.refreshRow()</q> ne sont pas impactées. Les applications de
l'utilisateur qui invoquent cette méthode sont impactées si la base de
données sous-jacente qu'elles requêtent au moyen de leur application JDBC,
peuvent être contrôlées par un attaquant. L'attaque nécessite que
l'attaquant amène l'utilisateur à exécuter une requête SQL sur un nom de
table dont les noms de colonne contenaient le code SQL malveillant et
ensuite invoque la méthode <q>refreshRow()</q> sur le ResultSet. Notez que
l'utilisateur JDBC de l'application et le propriétaire du schéma n'ont pas
besoin d'être les mêmes. Une application JDBC qui s'exécute en tant
qu'utilisateur privilégié requêtant des schémas de bases de données
propriétés d'utilisateurs éventuellement moins privilégiés malveillants
pouvait être vulnérable. Dans cette situation, il peut être possible à
l'utilisateur malveillant de falsifier un schéma qui fait que l'application
exécute des commandes en tant qu'utilisateur privilégié. Les versions
corrigées seront publiées sous les numéros <q>42.2.26</q> et <q>42.4.1</q>.
Il est conseillé à tous les utilisateurs de mettre à niveau leur paquet. Il
n'y a pas de contournement pour ce problème.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans la
version 42.2.5-2+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libpgjava.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :\
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3140.data"
# $Id: $
