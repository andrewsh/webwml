#use wml::debian::translation-check translation="0724bb634c9a00df3c7323875023eb45ae582ada" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans pjproject, une
bibliothèque de communication multimédia libre et à code source ouvert.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-32686">CVE-2021-32686</a>

<p>Situation de compétition entre <q>callback</q> et <q>destroy</q> à cause
d'un socket accepté n'ayant pas de verrouillage de bloc. Deuxièmement, le
socket SSL parent ou récepteur peut être détruit pendant l'initialisation
de connexion. Ces problèmes provoquent un plantage ayant pour conséquence
un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37706">CVE-2021-37706</a>

<p>Un message STUN entrant contient un attribut ERROR-CODE, la longueur de
l'en-tête n'est pas vérifiée avant de réaliser une opération de
soustraction, avec pour conséquence éventuellement un scénario de
dépassement d'entier par le bas. Ce problème affecte tous les utilisateurs
de STUN. Un acteur malveillant situé dans le réseau de la victime peut
fabriquer et envoyer un message UDP (STUN) contrefait pour l'occasion qui
pourrait exécuter à distance du code arbitraire sur la machine de la
victime.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41141">CVE-2021-41141</a>

<p>Dans diverses parties de PJSIP, quand une erreur ou un échec survient,
il a été découvert que la fonction se termine sans que les verrous en cours
ne soient relâchés. Cela pourrait avoir pour conséquence un blocage du
système qui provoque un déni de service pour les utilisateurs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43299">CVE-2021-43299</a>

<p>Dépassement de pile dans l'API PJSUA lors de l'appel de
pjsua_player_create. Un argument<q>filename</q> contrôlé par un attaquant
peut provoquer un dépassement de tampon dans la mesure où il est copié dans
un tampon de pile à taille fixée sans aucune vérification de taille.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43300">CVE-2021-43300</a>

<p>Dépassement de pile dans l'API PJSUA lors de l'appel de
pjsua_recorder_create. Un argument<q>filename</q> contrôlé par un attaquant
peut provoquer un dépassement de tampon dans la mesure où il est copié dans
un tampon de pile à taille fixée sans aucune vérification de taille.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43301">CVE-2021-43301</a>

<p>Dépassement de pile dans l'API PJSUA lors de l'appel de
pjsua_playlist_create. Un argument <q>file_names</q> contrôlé par un
attaquant peut provoquer un dépassement de tampon dans la mesure où il est
copié dans un tampon de pile à taille fixée sans aucune vérification de
taille.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43302">CVE-2021-43302</a>

<p>Lecture hors limites dans l'API PJSUA lors de l'appel de
pjsua_recorder_create. Un argument<q>filename</q> contrôlé par un attaquant
peut provoquer une lecture hors limites lorsque le nom de fichier a moins
de quatre caractères.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43303">CVE-2021-43303</a>

<p>Dépassement de tampon dans l'API PJSUA lors de l'appel de
pjsua_call_dump. Un argument <q>buffer</q> contrôlé par un attaquant
peut provoquer un dépassement de tampon, dans la mesure où fournir un
tampon de sortie de moins de 128 caractères peut déborder le tampon de
sortie, quel que soit l'argument <q>maxlen</q> fourni.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43804">CVE-2021-43804</a>

<p>Un message RTCP BYE entrant contient une longueur de raison, cette
longueur déclarée n'est pas vérifiée par rapport à la taille réelle du
paquet reçu, avec pour conséquence éventuellement un accès en lecture hors
limites. Un acteur malveillant peut envoyer un message RTCP BYE avec une
longueur de raison non valable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43845">CVE-2021-43845</a>

<p>Si un message RTCP XR entrant contient un bloc, le champ de données
n'est pas vérifié par rapport à la taille du paquet reçu, avec pour
conséquence éventuellement un accès en lecture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21722">CVE-2022-21722</a>

<p>Il est possible que certains paquets RTP/RTCP entrants provoquent
éventuellement un accès en lecture hors limites. Ce problème affecte tous
les utilisateurs qui se servent de PJMEDIA et acceptent les paquets
RTP ou RTCP entrants.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-21723">CVE-2022-21723</a>

<p>L'analyse d'un message SIP entrant qui contient une entité fragmentée
mal formée peut provoquer éventuellement un accès en lecture hors limites.
Ce problème affecte tous les utilisateurs qui se servent de PJSIP et
acceptent les messages SIP <q>multipart</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23608">CVE-2022-23608</a>

<p>Quand, dans un scénario de configuration (ou de fourche) de dialogue,
une clé de hachage partagée par de multiples dialogues de contrôle de
compte utilisateur (UAC) peut éventuellement être libérée prématurément,
alors un des dialogues est détruit. Ce problème peut faire qu'une
configuration de dialogue soit enregistrée plusieurs fois dans la table de
hachage (avec des clés de hachage différentes) menant à un comportement
indéfini tel qu'une collision de liste de dialogues qui mène éventuellement
à une boucle infinie.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24754">CVE-2022-24754</a>

<p>Il y a une vulnérabilité de dépassement de tampon de pile qui n'affecte
que les utilisateurs de PJSIP qui acceptent les identifiants <q>digest</q>
hachés (identifiants avec le data_type « PJSIP_CRED_DATA_DIGEST »).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24764">CVE-2022-24764</a>

<p>Une vulnérabilité de dépassement de tampon de pile qui affecte les
utilisateurs de PJSUA2 ou les utilisateurs qui invoquent l'API
« pjmedia_sdp_print(), pjmedia_sdp_media_print() ».</p></li>
</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 2.5.5~dfsg-6+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets pjproject.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de pjproject, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/pjproject">\
https://security-tracker.debian.org/tracker/pjproject</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2962.data"
# $Id: $
