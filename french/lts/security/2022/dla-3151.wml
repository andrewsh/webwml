#use wml::debian::translation-check translation="6984f64209740b25e5e29cb98b47fd2f6d18499d" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans squid, un cache de
serveur mandataire web.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41317">CVE-2022-41317</a>

<p>Du fait d'un traitement incohérent des URI internes, Squid était
vulnérable à l'exposition d'informations sensibles sur les clients
utilisant le mandataire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41318">CVE-2022-41318</a>

<p>Du fait d'une protection incorrecte contre les dépassements d'entier,
les assistants d'authentification SSPI et SMB de Squid étaient vulnérables
à une attaque par dépassement de tampon.</p></li>

</ul>

<p>Pour Debian 10 Buster, ces problèmes ont été corrigés dans la version
4.6-1+deb10u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets squid.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de squid, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/squid">\
https://security-tracker.debian.org/tracker/squid</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3151.data"
# $Id: $
