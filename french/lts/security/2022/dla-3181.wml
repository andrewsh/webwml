#use wml::debian::translation-check translation="a6de6cb2cd4545ad5d80db3996e57f49a00419b4" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un moyen de divulgation d'informations
dans sudo, un outil pour des privilèges limités de superutilisateur à des
utilisateurs particuliers. Un utilisateur local non privilégié pouvait
réaliser des tests arbitraires d’existence de répertoires en exploitant une
situation de compétition dans sudoedit.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23239">CVE-2021-23239</a>

<p>La personnalité de sudoedit de Sudo avant 1.9.5 pouvait permettre à un
utilisateur local non privilégié de réaliser des tests arbitraires d’existence
de répertoires en gagnant une situation de compétition dans sudo_edit.c en
remplaçant un répertoire contrôlé par l’utilisateur par un lien symbolique vers
un chemin arbitraire.</p></li>

</ul>

<p>Pour Debian 10 « Buster », ce problème a été corrigé dans
la version 1.8.27-1+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sudo.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3181.data"
# $Id: $
