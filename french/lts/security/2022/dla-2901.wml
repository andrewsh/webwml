#use wml::debian::translation-check translation="65a1a693d57a737d6d4e9ae85577e6a7485254fd" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Un problème a été découvert dans libxfont, une bibliothèque de
rastérisation de fontes X11. En créant des liens symboliques, un attaquant
local pouvait ouvrir (mais pas lire) des fichiers locaux en tant que
superutilisateur. Cela pouvait créer des actions non désirées avec des
fichiers spéciaux comme /dev/watchdog.</p>


<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
1:2.0.1-3+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libxfont.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libxfont, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libxfont">\
https://security-tracker.debian.org/tracker/libxfont</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2901.data"
# $Id: $
