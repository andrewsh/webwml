#use wml::debian::translation-check translation="0b812419d8515ffd33a42df48a8a95f764438392" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans mbedtls, une
bibliothèque légère de chiffrement et SSL/TLS, qui pouvaient permettre à des
attaquants d’obtenir des informations sensibles telles que la clé RSA privée ou
de causer un déni de service (plantage d’application ou de serveur).</p>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.16.9-0~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mbedtls.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de mbedtls,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/mbedtls">\
https://security-tracker.debian.org/tracker/mbedtls</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3249.data"
# $Id: $
