#use wml::debian::translation-check translation="27421fa91da8e9f7b263163fdf0e1de82009f4f4" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>g810-led, un outil de configuration de LED pour les claviers Gx10 de
Logitech, contenait une règle udev pour que les nœuds de périphérique gérés
soient universellement lisibles et éditables, permettant à n’importe quel
processus du système de lire le trafic du clavier, dont les données sensibles.</p>

<p>Pour Debian 10 « Buster », ce problème a été corrigé dans
la version 0.3.3-2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets g810-led.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de g810-led,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/g810-led">\
https://security-tracker.debian.org/tracker/g810-led</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3217.data"
# $Id: $
