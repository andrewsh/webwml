#use wml::debian::translation-check translation="c13ebbdcf9470d383f8661704e76f2cafb7583cd" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité potentielle d’élévation
des privilèges dans l’utilitaire d’archivage <q>amanda</q>.</p>

<p>Le binaire SUID situé dans <code>/lib/amanda/rundump</code> exécutait
<code>/usr/sbin/dump</code> en tant que superutilisateur avec des arguments
contrôlés par l’attaquant, ce qui pouvait conduire à une élévation des
privilèges, un déni de service (DoS) ou une divulgation d'informations.</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-37704">CVE-2022-37704</a></li>
</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1:3.5.1-2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets amanda.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3330.data"
# $Id: $
