#use wml::debian::translation-check translation="1e84cdc35cabf494da53f7dccc13378018ea6686" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait certains problèmes dans graphite-web, un
outil pour des graphiques de statistiques en temps réel, etc.</p>

<p>Une série de vulnérabilités de script intersite (XSS) existait qui pouvait
être exploitée à distance. Ces problèmes existaient dans les composants Cookie
Handler, Template Name Handler et Absolute Time Range Handler.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4728">CVE-2022-4728</a>

<p>Une vulnérabilité classée comme problématique a été découverte dans
SourceCodester Blood Bank Management System, version 1.0. Était affectée une
fonction inconnue dans le fichier index.php?page=users du composant User
Registration Handler. La manipulation de l’argument Name conduisait à un
script intersite. L’attaque pouvait être lancée à distance. VDB-216774 est
l’identifiant assigné à cette vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4729">CVE-2022-4729</a>

<p>Une vulnérabilité classée comme critique a été découverte dans SourceCodester
School Dormitory Management System, version 1.0. Était affectée une fonction
inconnue dans le composant Admin Login. La manipulation conduisait à une
injection SQL. L’attaque pouvait être lancée à distance. VDB-216775 est
l’identifiant assigné à cette vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4730">CVE-2022-4730</a>

<p>Une vulnérabilité a été découverte dans Graphite Web, classée comme
problématique. Était affectée une fonction inconnue dans le composant Absolute
Time Range Handler. La manipulation conduisait à un script intersite. L’attaque
pouvait être lancée à distance. L’exploit a été révélé publiquement et peut être
utilisé. Le nom du correctif est 2f178f490e10efc03cd1d27c72f64ecab224eb23. Il
est recommandé d’appliquer le correctif pour régler ce problème. VDB-216744 est
l’identifiant assigné à cette vulnérabilité.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.1.4-3+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets graphite-web.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3309.data"
# $Id: $
