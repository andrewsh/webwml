#use wml::debian::translation-check translation="f6a434bf244be42f1d7b84f2b626192ab4cbe6a7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été découvertes dans Git, un système de gestion de
versions distribué. Un attaquant pouvait déclencher l’exécution de code dans
des situations particulières.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23521">CVE-2022-23521</a>

<p>gitattributes est le mécanisme qui permet de définir des attributs pour les
chemins. Ces attributs peuvent être définis en ajoutant un fichier
<q>.gitattributes</q> au dépôt qui contient un ensemble de modèles de fichier
et les attributs qui devraient être définis pour les chemins correspondant à ces
modèles. Lors de l’analyse de gitattributes, plusieurs dépassements d'entier
pouvaient se produire s'il existait un grand nombre de modèles de chemin, un
grand nombre d’attributs pour un seul modèle ou quand les noms d’attribut
déclaré étaient très grands. Ces dépassements pouvaient être déclenchés à l'aide
d'un fichier <q>.gitattributes</q> contrefait faisant partie de l’historique
des commits. Git coupait silencieusement les lignes supérieures à 2Ko lors de
l’analyse de gitattributes à partir d’un fichier, mais pas lors de son analyse
à partir de l’index. Par conséquent, le défaut dépendait de l'existence du fichier
dans l’arbre de travail, dans l’index ou dans les deux. Ce dépassement d'entier
pouvait aboutir à des lectures ou des écritures arbitraires de tas et
conduire à une exécution de code à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41903">CVE-2022-41903</a>

<p><q>git log</q> peut afficher des commits dans un format arbitraire en
utilisant ses spécificateurs <q>--format</q>. Cette fonctionnalité est aussi
exposée à <q>git archive</q> à l’aide du gitattribute <q>export-subst</q>. Lors
du traitement des opérateurs de remplissage, un dépassement d'entier existait
dans <q>pretty.c::format_and_pad_commit()</q> où un <q>size_t</q> était stocké
incorrectement comme <q>int</q>, puis ajouté comme décalage à <q>memcpy()</q>.
Ce dépassement pouvait être déclenché directement par un utilisateur exécutant
une commande invoquant le mécanisme de formatage de commit (par exemple, <q>git
log --format=...</q>). Il pouvait aussi l’être indirectement à travers git
archive à l’aide du mécanisme export-subst qui développe les spécificateurs de
format dans les fichiers du dépôt lors d’un git archive. Ce dépassement d'entier
pouvait aboutir à des lectures arbitraires de tas et conduire à une exécution
de code arbitraire.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1:2.20.1-2+deb10u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets git.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de git,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/git">\
https://security-tracker.debian.org/tracker/git</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3282.data"
# $Id: $
