#use wml::debian::translation-check translation="c8457a59a89d51637f9c29eabc2b64c8a52130b6" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>La fonction read_binary dans buffer.c dans pgpdump, un visualisateur de
paquet PGP, permet à des attaquants en fonction du contexte de provoquer un
déni de service (boucle infinie et consommation de processeur) au moyen
d'une entrée contrefaite. Le
<a href="https://security-tracker.debian.org/tracker/CVE-2016-4021">CVE-2016-4021</a>
a été assigné à ce problème.</p>

<p>Également, la fonction read_radix64 pourrait lire des données au-delà de
la fin d'un tampon à partir d'une entrée contrefaite.</p>


<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 0.27-1+deb7u1.</p>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes seront corrigés dans la
version 0.28-1+deb8u1, comme élément de la version intermédiaire à venir.</p>

<p>Pour Debian 9 <q>Stretch</q> et <q>Sid</q>, ces problèmes ont été
corrigés dans la version 0.31-0.1</p>

<p>Nous vous recommandons de mettre à jour vos paquets pgpdump.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-768.data"
# $Id: $
