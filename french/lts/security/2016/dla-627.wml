#use wml::debian::translation-check translation="16a228d71674819599fa1d0027d1603056286470" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans pdns, un serveur DNS
faisant autorité. Le projet « Common Vulnerabilities and Exposures » (CVE)
identifie les problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5426">CVE-2016-5426</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-5427">CVE-2016-5427</a>

<p>Florian Heinz et Martin Kluge ont signalé que le serveur PowerDNS
faisant autorité accepte des requêtes d'une longueur de qname supérieure
à 255 octets et ne gère pas correctement les points dans les étiquettes. Un
attaquant distant non authentifié peut tirer avantage de ces défauts pour
provoquer une charge anormale sur le dorsal PowerDNS en envoyant des
requêtes DNS contrefaites pour l'occasion, menant éventuellement à un déni
de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-6172">CVE-2016-6172</a>

<p>Il a été signalé qu'un serveur DNS primaire malveillant peut faire
planter un serveur PowerDNS secondaire à cause d'une restriction incorrecte
de limites de taille de zone. Cette mise à jour ajoute une fonctionnalité
pour limiter la taille des AXFR en réponse à ce défaut.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 3.1-4.1+deb7u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets pdns.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-627.data"
# $Id: $
