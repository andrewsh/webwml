#use wml::debian::translation-check translation="36df778cd565c0b0740a8e172a015c42ff1ee212" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux problèmes de sécurité ont été découverts dans nghttp2 : serveur,
mandataire et client mettant en œuvre HTTP/2.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000168">CVE-2018-1000168</a>

<p>Une vulnérabilité CWE-20 de validation incorrecte d’entrée a été trouvée
dans le traitement de trame ALTSVC, qui pourrait provoquer une erreur de
segmentation conduisant à un déni de service. Cette attaque paraît exploitable
à l’aide d’un client réseau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11080">CVE-2020-11080</a>

<p>Une charge excessivement large de trame HTTP/2 SETTINGS provoque un déni
de service. Le concept de l’attaque peut être démontré en faisant appel à un client
malveillant construisant une trame SETTINGS avec une longueur de 14 400 octets
(2 400 entrées de réglage individuelles) encore et encore. Cette attaque
induit un pic d'utilisation du CPU à 100 %.</p></li>

</ul>

<p>Pour Debian 9 « Stretch », ces problèmes ont été corrigés dans
la version 1.18.1-1+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets nghttp2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de nghttp2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/nghttp2">\
https://security-tracker.debian.org/tracker/nghttp2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2786.data"
# $Id: $
