#use wml::debian::translation-check translation="0f38efd07aba001ac19918fa3444db8f364ac6fe" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans LibreCAD, une
application de conception assistée par ordinateur (CAO) en deux dimensions.
Un attaquant pourrait déclencher l'exécution de code au moyen de fichiers
.dwg et .dxf malveillants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21898">CVE-2021-21898</a>

<p>Une vulnérabilité d'exécution de code existe dans la fonctionnalité
dwgCompressor::decompress18() de libdxfrw de LibreCad.  Un fichier .dwg
contrefait pour l'occasion peut conduire à une écriture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21899">CVE-2021-21899</a>

<p>Une vulnérabilité d'exécution de code existe dans la fonctionnalité
dwgCompressor::copyCompBytes21 de libdxfrw de LibreCad. Un fichier .dwg
contrefait pour l'occasion peut conduire à un dépassement de tampon de tas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21900">CVE-2021-21900</a>

<p>Une vulnérabilité d'exécution de code existe dans la fonctionnalité
dxfRW::processLType() de libdxfrw de LibreCad. Un fichier .dxf contrefait
pour l'occasion peut conduire à une vulnérabilité d'utilisation de mémoire
après libération.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 2.1.2-1+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets librecad.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de librecad, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/librecad">\
https://security-tracker.debian.org/tracker/librecad</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2838.data"
# $Id: $
