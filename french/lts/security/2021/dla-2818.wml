#use wml::debian::translation-check translation="9eaf0a59f6574fa82d32395f0ef76401e7a82612" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été découverts dans ffmpeg − outils pour
transcoder, diffuser et lire des fichiers multimédia.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-20445">CVE-2020-20445</a>

<p>Problème de division par zéro au moyen de libavcodec/lpc.h. Cela
permet à un utilisateur distant malveillant de provoquer un déni de
service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-20446">CVE-2020-20446</a>

<p>Problème de division par zéro au moyen de libavcodec/aacpsy.c. Cela
permet à un utilisateur distant malveillant de provoquer un déni de
service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-20451">CVE-2020-20451</a>

<p>Problème de déni service due à des erreurs de gestion de ressources au
moyen de fftools/cmdutils.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-20453">CVE-2020-20453</a>

<p>Problème de division par zéro au moyen de libavcodec/aaccoder. Cela
permet à un utilisateur distant malveillant de provoquer un déni de
service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22037">CVE-2020-22037</a>

<p>Vulnérabilité de déni de service due à une fuite de mémoire dans
options.c de avcodec_alloc_context3.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22041">CVE-2020-22041</a>

<p>Vulnérabilité de déni de service due à une fuite de mémoire dans la
fonction av_buffersrc_add_frame_flags dans buffersrc.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22044">CVE-2020-22044</a>

<p>Vulnérabilité de déni de service due à une fuite de mémoire dans la
fonction url_open_dyn_buf_internal dans libavformat/aviobuf.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22046">CVE-2020-22046</a>

<p>Vulnérabilité de déni de service due à une fuite de mémoire dans la
fonction avpriv_float_dsp_allocl dans libavutil/float_dsp.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22048">CVE-2020-22048</a>

<p>Vulnérabilité de déni de service due à une fuite de mémoire dans la
fonction ff_frame_pool_get dans framepool.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22049">CVE-2020-22049</a>

<p>Vulnérabilité de déni de service due à une fuite de mémoire dans la
fonction wtvfile_open_sector dans wtvdec.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22054">CVE-2020-22054</a>

<p>Vulnérabilité de déni de service due à une fuite de mémoire dans la
fonction av_dict_set dans dict.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38171">CVE-2021-38171</a>

<p>adts_decode_extradata dans libavformat/adtsenc.c ne vérifie pas la
valeur de retour d'init_get_bits, ce qui est une étape nécessaire parce que
le second paramètre d'init_get_bits peut être contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38291">CVE-2021-38291</a>

<p>Un échec d'assertion a été détecté dans src/libavutil/mathematics.c,
interrompant ffmpeg. Dans certains cas extrêmes, comme avec des
échantillons adpcm_ms avec un nombre de canaux extrêmement élevé,
get_audio_frame_duration() peut renvoyer une valeur négative de durée de
trame.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 7:3.2.16-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ffmpeg.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ffmpeg, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ffmpeg">\
https://security-tracker.debian.org/tracker/ffmpeg</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2818.data"
# $Id: $
