#use wml::debian::translation-check translation="155d762aa2120bbe9e199f7a1a045a7187604d41" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-0256">CVE-2020-0256</a>

<p>Dans LoadPartitionTable de gpt.cc, il existe une lecture possible hors
limites due à une absence de vérification de limites. Cela pourrait conduire
à une augmentation locale de privilèges sans que des privilèges supplémentaires
d’exécution soient nécessaires.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-0308">CVE-2021-0308</a>

<p>Dans ReadLogicalParts de basicmbr.cc, il existe une lecture possible hors
limites due à une absence de vérification de limites. Cela pourrait conduire
à une augmentation locale de privilèges sans que des privilèges supplémentaires
d’exécution soient nécessaires.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1.0.1-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gdisk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de gdisk, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/gdisk">\
https://security-tracker.debian.org/tracker/gdisk</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2549.data"
# $Id: $
