#use wml::debian::translation-check translation="ae3bfd6ac864c10153356a4a7aeadb3684ccd896" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été corrigées dans Kerberos du MIT, un système
pour authentifier les utilisateurs et les services dans un réseau.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5729">CVE-2018-5729</a>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5730">CVE-2018-5730</a>

<p>Correction de défauts dans la vérification du DN de LDAP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20217">CVE-2018-20217</a>

<p>Ignorer les attributs de mot de passe pour les requêtes S4U2Self.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37750">CVE-2021-37750</a>


<p>Correction de déréférencement de pointeur NULL dans KDC dû à l’envoi
de requête FAST TGS sans champ de serveur.</p></li>

</ul>

<p>Pour Debian 9 « Stretch », ces problèmes ont été corrigés dans
la version 1.15-1+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets krb5.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de krb5,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/krb5">\
https://security-tracker.debian.org/tracker/krb5</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2771.data"
# $Id: $
