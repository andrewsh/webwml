#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Leon a signalé cinq vulnérabilités de dépassement de tas dans FreeXL.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7435">CVE-2018-7435</a>

<p>Il y a un dépassement de tas en lecture dans la fonction
freexl::destroy_cell.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7436">CVE-2018-7436</a>

<p>Il y a un dépassement de tas en lecture dans un déréférencement de
pointeur de la fonction parse_SST.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7437">CVE-2018-7437</a>

<p>Il y a un dépassement de tas en lecture dans un appel memcpy de la
fonction parse_SST.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7438">CVE-2018-7438</a>

<p>Il y a un dépassement de tas en lecture dans la fonction
parse_unicode_string.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-7439">CVE-2018-7439</a>

<p>Il y a un dépassement de tas en lecture dans la fonction
read_mini_biff_next_record.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.0.0b-1+deb7u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets freexl.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1297.data"
# $Id: $
