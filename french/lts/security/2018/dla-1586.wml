#use wml::debian::translation-check translation="fb36d8d30ef43fdd50a0075131d4387545f6daaa" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-0735">CVE-2018-0735</a>

<p>Samuel Weiser a signalé une vulnérabilité d’attaque temporelle dans la
génération de signature ECDSA d’OpenSSL qui pouvait laisser fuir des informations
permettant de récupérer la clef privée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5407">CVE-2018-5407</a>

<p>Alejandro Cabrera Aldaya, Billy Brumley, Sohaib ul Hassan, Cesar
Pereida Garcia et Nicola Tuveri ont signalé une vulnérabilité d’attaque
temporelle par canal auxiliaire qui peut être utilisée pour récupérer
la clef privée.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 1.0.1t-1+deb8u10.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openssl.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1586.data"
# $Id: $
