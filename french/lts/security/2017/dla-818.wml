#use wml::debian::translation-check translation="16a228d71674819599fa1d0027d1603056286470" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été découverts dans PHP (acronyme récursif pour PHP :
Hypertext Preprocessor), un langage de script généraliste au source libre
couramment utilisé et particulièrement bien adapté pour le développement web et
pouvant être incorporé dans du HTML.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-2554">CVE-2016-2554</a>

<p>Un dépassement de pile dans ext/phar/tar.c permet à des attaquants distants
de provoquer un déni de service (plantage d'application) ou, éventuellement,
d’avoir un impact non précisé à l'aide d'une archive TAR contrefaite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3141">CVE-2016-3141</a>

<p>Une vulnérabilité d’utilisation de mémoire après libération dans wddx.c dans
l’extension WDDX permet à des attaquants distants de provoquer un déni de
service (corruption de mémoire et plantage d'application) ou, éventuellement,
d’avoir un impact non précisé en déclenchant un appel wddx_deserialize sur des
données XML contenant un élément var contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3142">CVE-2016-3142</a>

<p>La fonction phar_parse_zipfile dans zip.c dans l’extension PHAR dans PHP
avant 5.5.33 et 5.6.x avant 5.6.19 permet à des attaquants distants d’obtenir
des informations sensibles de la mémoire du processus ou de causer un déni de
service (lecture hors limites et plantage d'application) en positionnant une
signature PK\x05\x06 dans un emplacement non valable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4342">CVE-2016-4342</a>

<p>ext/phar/phar_object.c dans PHP avant 5.5.32, 5.6.x avant 5.6.18 et 7.x
avant 7.0.3 gère incorrectement des données non compressées de longueur nulle.
Cela permet à des attaquants distants de provoquer un déni de service
(corruption de mémoire de tas) ou, éventuellement, d’avoir un impact non précisé
à l'aide d'une archive (1) TAR, (2) ZIP ou (3) PHAR.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9934">CVE-2016-9934</a>

<p>ext/wddx/wddx.c dans PHP avant 5.6.28 et 7.x avant 7.0.13 permet à des
attaquants distants de provoquer un déni de service (déréférencement de pointeur
NULL) à l’aide de données sérialisées contrefaites dans un document XML
wddxPacket, comme le montre la chaîne PDORow.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9935">CVE-2016-9935</a>

<p>La fonction php_wddx_push_element dans ext/wddx/wddx.c dans PHP avant 5.6.29
et 7.x avant 7.0.14 permet à des attaquants distants de provoquer un déni de
service (lecture hors limites et corruption de mémoire) ou, éventuellement,
d’avoir un impact non précisé à l'aide d'un élément booléen vide dans un
document XML wddxPacket.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10158">CVE-2016-10158</a>

<p>La fonction exif_convert_any_to_int dans ext/exif/exif.c dans PHP
avant 5.6.30, 7.0.x avant 7.0.15 et 7.1.x avant 7.1.1, permet à des attaquants
distants de provoquer un déni de service (plantage d'application) à l’aide de
données EXIF contrefaites déclenchant un essai de diviser l’entier minimal
négatif représentable par -1.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10159">CVE-2016-10159</a>

<p>Un dépassement d'entier dans la fonction phar_parse_pharfile dans
ext/phar/phar.c dans PHP avant 5.6.30 et 7.0.x avant 7.0.15, permet à des
attaquants distants de provoquer un déni de service (consommation de mémoire ou
plantage d'application) à l'aide d'une entrée de manifeste tronquée dans une
archive PHAR.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10160">CVE-2016-10160</a>

<p>Une erreur due à un décalage d'entier dans la fonction phar_parse_pharfile dans
ext/phar/phar.c dans PHP avant 5.6.30 et 7.0.x avant 7.0.15, permet à des
attaquants distants de provoquer un déni de service (corruption de mémoire) ou,
éventuellement, d’exécuter du code arbitraire à l'aide d'une archive PHAR
contrefaite avec une inadéquation d’alias.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10161">CVE-2016-10161</a>

<p>La fonction object_common1 dans ext/standard/var_unserializer.c dans PHP
avant 5.6.30, 7.0.x avant 7.0.15 et 7.1.x avant 7.1.1, permet à des attaquants
distants de provoquer un déni de service (lecture excessive de tampon et
plantage d'application) à l’aide de données sérialisées contrefaites, mal gérées
dans un appel finish_nested_data.</p></li>

<li>BOGUE n° 71323

<p>La sortie de stream_get_meta_data peut être falsifiée par son entrée.</p></li>

<li>BOGUE n° 70979

<p>Plantage pour des mauvaises requêtes SOAP.</p></li>

<li>BOGUE n° 71039

<p>Les fonctions exec ignorent la longueur mais recherchent un NULL final.</p></li>

<li>BOGUE n° 71459

<p>Dépassement d'entier dans iptcembed().</p></li>

<li>BOGUE n° 71391

<p>Déréférencement de pointeur NULL dans phar_tar_setupmetadata().</p></li>

<li>BOGUE n° 71335

<p>Vulnérabilité de confusion de type dans la désérialisation du paquet WDDX.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 5.4.45-0+deb7u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets php5.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-818.data"
# $Id: $
