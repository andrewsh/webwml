#use wml::debian::translation-check translation="5a40d9e7ce1735be2ce6c7cbf2f10b178e659b58" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans Python2.7, un langage
interactif de haut niveau orienté objet.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20852">CVE-2018-20852</a>

<p>En utilisant un serveur malveillant, un attaquant pourrait dérober les
cookies destinés à d’autres domaines.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5010">CVE-2019-5010</a>

<p>Déréférencement de pointeur NULL en utilisant un certificat X509 contrefait
pour l'occasion.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9636">CVE-2019-9636</a>

<p>Traitement incorrect d’encodage Unicode (avec un netloc incorrect)
lors d’une normalisation NFKC aboutissant à une divulgation d'informations
(accréditations, cookies, etc., mis en cache pour un nom d’hôte donné). Une URL
contrefaite pour l'occasion pourrait être analysée incorrectement pour identifier
des cookies ou des données d’authentification et envoyer ces informations à un
hôte différent de celui analysé correctement.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9740">CVE-2019-9740</a>

<p>Un problème a été découvert dans urllib2 où une injection CRLF est possible
si l’attaquant contrôle un paramètre d’URL, comme le montre le premier argument
d’urllib.request.urlopen avec \r\n (particulièrement avec la chaîne de requête
après un caractère ?) suivi par un en-tête HTTP ou une commande Redis.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9947">CVE-2019-9947</a>

<p>Un problème a été découvert dans urllib2 où une injection CRLF est possible
si l’attaquant contrôle un paramètre d’URL, comme le montre le premier argument
d’urllib.request.urlopen avec \r\n (particulièrement avec la chaîne de requête
après un caractère ?) suivi par un en-tête HTTP ou une commande Redis. Cela est
similaire au problème de requête de chaîne 
<a href="https://security-tracker.debian.org/tracker/CVE-2019-9740">CVE-2019-9740</a>.
</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9948">CVE-2019-9948</a>

<p>urllib gère <q>local_file: scheme</q>, ce qui facilite une attaque distante de
contournement des mécanismes de protection qui mettent en liste noire les 
<q>file: URI</q>, comme le montre un appel
à urllib.urlopen('local_file:///etc/passwd').</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10160">CVE-2019-10160</a>

<p>Une régression de sécurité
<a href="https://security-tracker.debian.org/tracker/CVE-2019-9636">CVE-2019-9636</a>
a été découverte qui permet encore à un attribut d’exploiter
<a href="https://security-tracker.debian.org/tracker/CVE-2019-9636">CVE-2019-9636</a>
les parties utilisateur et mot de passe d’une URL. Lorsqu’une application analyse
une URL fournie par l’utilisateur pour stocker les cookies, les accréditations ou 
d’autres sortes d’informations, il est possible pour un attaquant de fournir des
URL contrefaites pour l'occasion afin que l’application identifie des
informations relatives à l’hôte (par exemple, cookies, données 
d’authentification) et les envoie au mauvais hôte, contrairement à l’analyse
correcte d’une URL. Cette attaque aboutit différemment selon l’application.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16056">CVE-2019-16056</a>

<p>Le module de courriel analyse mal les addresses de courriel contenant
plusieurs caractères @. Une application utilisant le module de courriel et
mettant en œuvre une certaine sorte de vérification des en-têtes From/To
d’un message pourrait être trompée et accepter une adresse de courriel qui
aurait due être refusée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20907">CVE-2019-20907</a>

<p>L’ouverture d’un fichier tar contrefait pourrait aboutir à une boucle infinie
due à une validation manquante d’en-tête.</p>


<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2.7.13-2+deb9u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python2.7.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python2.7, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python2.7">https://security-tracker.debian.org/tracker/python2.7</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2337.data"
# $Id: $
