#use wml::debian::translation-check translation="6002e79b0db6ccdf539717c837a0a5e8efed0240" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>golang-go.crypto a été récemment mis à jour avec un correctif pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2020-9283">CVE-2020-9283</a>.
Cela conduit à ce que tous les paquets utilisant le code affecté soient
recompilés pour tenir compte du correctif de sécurité.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9283">CVE-2020-9283</a>

<p>La vérification de signature SSH pourrait causer une <q>panique</q>
lorsqu’une clé publique non valable est fournie.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 0.10.2+dfsg-6+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets packer.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de packer, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/packer">https://security-tracker.debian.org/tracker/packer</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2455.data"
# $Id: $
