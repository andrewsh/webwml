#use wml::debian::translation-check translation="9db458c1dae9942c673cc29fb111342b63a605b3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Nouveaux paquets LTS</define-tag>
<define-tag moreinfo>
<p>Linux 4.19 a été empaqueté pour Debian 9 sous le nom de linux-4.19. Ce paquet fournit
une méthode de gestion de mise à niveau pour les systèmes qui utilisent
actuellement des paquets de noyau et de micrologiciel de la suite
« Stretch-backports ».</p>

<p>Cependant, « apt full-upgrade » n’installe <q>pas</q> automatiquement les paquets
de noyau mis à jour. Vous devez installer explicitement d’abord les métapaquets
suivants en fonction de votre système :</p>

<ul>
<li>linux-image-4.19-686</li>
<li>linux-image-4.19-686-pae</li>
<li>linux-image-4.19-amd64</li>
<li>linux-image-4.19-arm64</li>
<li>linux-image-4.19-armmp</li>
<li>linux-image-4.19-armmp-lpae</li>
<li>linux-image-4.19-cloud-amd64</li>
<li>linux-image-4.19-marvell</li>
<li>linux-image-4.19-rpi</li>
<li>linux-image-4.19-rt-686-pae</li>
<li>linux-image-4.19-rt-amd64</li>
<li>linux-image-4.19-rt-arm64</li>
<li>linux-image-4.19-rt-armmp</li>
</ul>

<p>Par exemple, si la commande « uname -r » présentement affiche
« 4.19.0-0.bpo.9-amd64 », vous devez installer linux-image-4.19-amd64.</p>

<p>Il n’est nul besoin de mettre à niveau les systèmes utilisant Linux 4.9,
car cette version de noyau sera prise en charge pendant toute la durée de la
période LTS.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2324.data"
# $Id: $
