#use wml::debian::translation-check translation="d4afb4000f5f43d616e6e4bcd1d0161d8e87acea" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu il existait une vulnérabilité de script intersite (XSS)
dans ruby-gon, une bibliothèque Ruby pour envoyer ou convertir des données en
Javascript à partir d’une application en Ruby. </p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25739">CVE-2020-25739</a>

<p>Un problème a été découvert dans le gem gon avant gon-6.4.0 pour Ruby.
MultiJson ne prend pas en compte le paramètre escape_mode pour protéger les
champs en tant que mécanisme de protection contre les scripts intersites. Comme
mitigation, json_dumper.rb dans gon réalise par défaut cette protection sans
s’appuyer sur MultiJson.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 6.1.0-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-gon.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2380.data"
# $Id: $
