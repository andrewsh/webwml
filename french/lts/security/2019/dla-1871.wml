#use wml::debian::translation-check translation="75e82e8f9d9ff2917106e8b5d9416ec7012e058f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes mineurs ont été corrigés dans vim, un éditeur de texte
grandement personnalisable.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11109">CVE-2017-11109</a>

<p>Vim permet à des attaquants de provoquer un déni de service (free non
valable) ou éventuellement d’avoir un impact non précisé à l'aide d'un fichier
source contrefait (c'est-à-dire -S).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17087">CVE-2017-17087</a>

<p>Vim règle le groupe possédant un fichier .swp au groupe primaire de
l’éditeur (qui peut être différent du groupe possédant le fichier original).
Cela permet à des utilisateurs locaux d’obtenir des informations sensibles en
exploitant une appartenance de groupe valable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12735">CVE-2019-12735</a>

<p>Vim ne restreignait pas la commande « :source! » lors d’une exécution dans un
bac à sable.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2:7.4.488-7+deb8u4.</p>
<p>Nous vous recommandons de mettre à jour vos paquets vim.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1871.data"
# $Id: $
