#use wml::debian::translation-check translation="1270369d847fa3a42d375b1a4c13aed8112432a9" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été trouvées dans PHP, un langage de script
généraliste au source libre couramment utilisé.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11039">CVE-2019-11039</a>

<p>Un dépassement d'entier par le bas dans le module iconv pourrait être
exploité pour déclencher une lecture hors limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11040">CVE-2019-11040</a>

<p>Un dépassement de tampon basé sur le tas a été découvert dans le code
d’analyse d’EXIF.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 5.6.40+dfsg-0+deb8u4.</p>
<p>Nous vous recommandons de mettre à jour vos paquets php5.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1813.data"
# $Id: $
