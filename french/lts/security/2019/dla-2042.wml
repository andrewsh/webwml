#use wml::debian::translation-check translation="4f7be4cf52368cbd0d55b2d71031a634d996e6c7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il existait une vulnérabilité potentielle de détournement de compte dans
Django, le cadriciel de développement web basé sur Python.</p>

<p>Le formulaire de réinitialisation de mot de passe de Django utilisait une
requête non sensible à la casse pour récupérer les comptes correspondant
à l’adresse de courriel sollicitant la réinitialisation du mot de passe. Du fait
que cela implique des transformations implicites ou explicites de casse, un
attaquant connaissant l’adresse de courriel associée avec le compte de
l’utilisateur pourrait façonner une adresse de courriel distincte de l’adresse
associée avec ce compte, mais qui, à cause du comportement des transformations
de casse d’Unicode, cesse d’être distincte après une transformation de casse, ou
qui sinon comparera l’équivalent fourni dans la base de données de
transformation de casse ou le comportement de la comparaison. Dans une telle
situation, l’attaquant peut recevoir un jeton valable de réinitialisation de
mot de passe pour le compte de l’utilisateur.</p>

<p>Pour résoudre cela, deux modifications ont été faites dans Django :</p>

<ul>
<li>Après la récupération d’une liste de comptes correspondant éventuellement
de la base de données, la fonction de réinitialisation du mot de passe de Django
vérifie désormais l’adresse de courriel pour une équivalence en Python, en
utilisant le processus recommandé de comparaison d’identifiants d’« Unicode
Technical Report 36, section 2.11.2(B)(2) ».</li>

<li>Lors de la génération des courriels de réinitialisation de mot de passe,
Django désormais envoie à l’adresse de courriel retrouvée dans la base de
données plutôt qu’à l’adresse soumise dans le formulaire de réinitialisation de
mot de passe.</li>
</ul>

<p>Pour plus d’informations, veuillez consulter :
<a href="https://www.djangoproject.com/weblog/2019/dec/18/security-releases/">https://www.djangoproject.com/weblog/2019/dec/18/security-releases/</a>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19844">CVE-2019-19844</a>

<p>Détournement potentiel de compte à l’aide d’un formulaire de réinitialiastion
de mot de passe.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.7.11-1+deb8u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-django.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2042.data"
# $Id: $
