#use wml::debian::translation-check translation="fce40adff1381ed16a3e5ebae7ad1ff8fcbbb739" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été réglées dans libjpeg-turbo, l’implémentation
par défaut de JPEG pour Debian.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3616">CVE-2016-3616</a>

<p>L’utilitaire cjpeg dans libjpeg permet à des attaquants distants de
provoquer un déni de service (déréférencement de pointeur NULL et plantage
d'application) ou exécuter du code arbitraire à l'aide d'un fichier contrefait.</p>

<p>Ce problème était déjà corrigé par le même correctif réglant
 <a href="https://security-tracker.debian.org/tracker/CVE-2018-11213">CVE-2018-11213</a> et
<a href="https://security-tracker.debian.org/tracker/CVE-2018-11214">CVE-2018-11214</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1152">CVE-2018-1152</a>

<p>libjpeg-turbo a été découvert vulnérable à un déni de service causé par une
division par zéro lors du traitement d’une image BMP contrefaite. Ce problème a
été corrigé par une vérification des limites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11212">CVE-2018-11212</a>

<p>La fonction alloc_sarray dans jmemmgr.c permettait à des attaquants distants
de provoquer un déni de service (erreur de division par zéro) à l'aide d'un
fichier contrefait.</p>

<p>Ce problème a été traité par la vérification de la taille de l’image lors de
la lecture d’un fichier Targa et la déclaration d’une erreur quand la largeur ou
la hauteur de l’image sont égales à 0.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11213">CVE-2018-11213</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2018-11214">CVE-2018-11214</a>

<p>Les fonctions get_text_gray_row et get_text_rgb_rows dans rdppm.c
permettaient à des attaquants distants de provoquer un déni de service (défaut
de segmentation) à l'aide d'un fichier contrefait.</p>

<p>La vérification de l’intervalle des valeurs d’entiers dans les fichiers
textuels PPM et l’ajout des vérifications pour s’assurer que les valeurs soient
dans l’intervalle indiqué, résolvent les deux problèmes.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 1:1.3.1-12+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libjpeg-turbo.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1638.data"
# $Id: $
