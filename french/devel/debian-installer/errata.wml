#use wml::debian::template title="Errata pour l'installateur Debian"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="190364034624c840c0b9e316daa41653f0a86db2" maintainer="Baptiste Jammet" mindelta="1" maxdelta="1"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"

# Translators:
# Nicolas Bertolissio, 2003-2008
# Thomas Peteul, 2008, 2009
# Simon Paillard, 2010
# David Prévot, 2010-2014
# Jean-Pierre Giraud, 2013, 2021
# Baptiste Jammet, 2014-2023

<h1><i>Errata</i> de la version <humanversion /></h1>

<p>
Voici une liste des problèmes connus dans la version <humanversion />
de l'installateur Debian.
Si le problème rencontré n'y est pas recensé, veuillez envoyer un <a
href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">compte-rendu d'installation</a>
(en anglais) décrivant le problème.
</p>

<dl class="gloss">
<dt>Thème utilisé dans l'installateur</dt>
<dd>Il n'y a pas encore de thème graphique pour Bookworm et l'installateur utilise
le thème de Bullseye.<br />
<b>État :</b> fixé dans Bookworm Alpha 2, même si certains environnements de bureau
pourraient encore utiliser le thème de Debian Bullseye.
</dd>

<dt>Micrologiciels nécessaires pour certaines cartes son</dt>
<dd>Il apparaît qu'il y a un certain nombre de cartes son qui nécessitent le
chargement d'un micrologiciel pour pouvoir émettre un son
(<a href="https://bugs.debian.org/992699">n° 992699</a>).
<br />
<b>État :</b> corrigé dans Bookworm Alpha 1.
</dd>

 <dt>Les systèmes LVM chiffrés pourraient échouer en cas de faible mémoire</dt>
 <dd>Les systèmes possédant peu de mémoire (p.ex. 1 Go) peuvent échouer
 à installer un volume LVM chiffré. cryptsetup pourrait déclencher
 un dépassement de mémoire et se faire interrompre
 (par le « out-of-memory killer ») en essayant de formater une partition LUKS
 (<a href="https://bugs.debian.org/1028250">n° 1028250</a>,
 <a href="https://gitlab.com/cryptsetup/cryptsetup/-/issues/802">rapport de l'amont cryptsetup</a>).
 </dd>
 
<!-- <dt>L'installation d'un environnement de bureau pourrait échouer avec le seul premier CD</dt>
<dd>
En raison de sa taille,
la totalité des paquets du bureau GNOME ne rentre pas sur le premier CD.
Pour réussir cette installation, utilisez une source supplémentaire de paquets
(un second CD ou un miroir réseau) ou utilisez plutôt un DVD.
<br />
<b>État :</b> rien de plus ne peut être fait pour faire tenir
davantage de paquets sur le premier CD.
</dd>

<dt>LUKS2 n'est pas compatible avec cryptodisk de GRUB</dt>
<dd>Il a été découvert tardivement que GRUB ne prenait pas en charge LUKS2.
Cela veut dire qu'il n'est pas possible d'utiliser <tt>GRUB_ENABLE_CRYPTODISK</tt>
sans une partition <tt>/boot</tt> séparée et non chiffrée.
(<a href="https://bugs.debian.org/927165">n° 927165</a>).
L'installateur ne prend pas en charge cette configuration,
mais il serait intéressant de mieux documenter cette limitation,
et d'avoir au moins la possibilité de choisir LUKS1 pendant l'installation.
<br />
<b>État :</b> quelques idées ont été exprimées dans le rapport de bogue.
Les responsables de cryptsetup ont écrit
<a href="https://cryptsetup-team.pages.debian.net/cryptsetup/encrypted-boot.html">une
documentation spécifique</a> (en anglais).</dd>
 -->
 
</dl>
