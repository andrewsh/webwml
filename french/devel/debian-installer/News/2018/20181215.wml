#use wml::debian::translation-check translation="9a091cbc67457151ac60a49ad92557120fe3f69f" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de l'installateur Debian Buster Alpha 4</define-tag>
<define-tag release_date>2018-12-15</define-tag>
#use wml::debian::news

<p>
L'<a href="https://wiki.debian.org/DebianInstaller/Team">équipe</a> du
programme d'installation de Debian a le plaisir d'annoncer la parution
de la quatrième version alpha pour Debian 10 <q>Buster</q>.
</p>


<h2>Avant-propos</h2>

<p>Cyril Brulebois voudrait commencer par remercier Christian Perrier, qui
a travaillé pendant des années sur l'installateur Debian, en particulier
sur les questions d'internationalisation (i18n) et de localisation (l10n).
On se souviendra des graphiques et de ses articles sur le blog Planet
Debian avec toutes ses statistiques ; garder la trace de tous ces chiffres
pouvait passer pour une question purement mathématique, mais avoir des
traductions à jour est un élément essentiel pour que l'installateur Debian
soit accessible pour la plupart des utilisateurs.</p>

<p>Merci beaucoup Christian !</p>

<h2>Améliorations dans cette version de l'installateur</h2>

<ul>
  <li>choose-mirror :
    <ul>
      <li>deb.debian.org devient l'adresse de miroir HTTP par défaut
        (<a href="https://bugs.debian.org/797340">n° 797340</a>).</li>
    </ul>
  </li>
  <li>debian-archive-keyring-udeb :
    <ul>
      <li>retrait des clés pour Wheezy (<a href="https://bugs.debian.org/901320">n° 901320</a>) ;</li>
      <li>début de la fourniture de trousseaux de clés distincts pour chaque
        publication (<a href="https://bugs.debian.org/861695">n° 861695</a>).</li>
    </ul>
  </li>
  <li>debian-installer :
    <ul>
      <li>passage de l'ABI du noyau Linux de 4.16.0-2 à 4.18.0-3 ;</li>
      <li>structure de l'archive netboot.tar.gz pour armhf rendue identique
        à celle des autres architectures (<a href="https://bugs.debian.org/902020">n° 902020</a>) ;</li>
      <li>remplacement de ttf-freefont-udeb par fonts-freefont-udeb ;</li>
      <li>résolution de grub conservée dans EFI boot, pour éviter les
        fontes trop petites (<a href="https://bugs.debian.org/910227">n° 910227</a>) ;</li>
      <li>correction de la fonction de la touche F10 non opérationnelle
        pour les pages d'aide de l'écran d'amorçage.</li>
    </ul>
  </li>
  <li>debootstrap :
    <ul>
      <li>activation de merged-/usr par défaut (<a href="https://bugs.debian.org/839046">n° 839046</a>),
        mais désactivation lors de la préparation d'un chroot de buildd
        (<a href="https://bugs.debian.org/914208">n° 914208</a>) ;</li>
      <li>nombreuses autres améliorations et corrections, voir le journal
        des modifications pour plus de détails.</li>
    </ul>
  </li>
  <li>fonts-thai-tlwg-udeb :
    <ul>
      <li>fourniture des fontes OTF à la place des fontes TTF.</li>
    </ul>
  </li>
  <li>partman-auto-lvm :
    <ul>
      <li>ajout de la possibilité de limiter l'espace utilisé dans le groupe
        de volumes (VG) de LVM (<a href="https://bugs.debian.org/515607">n° 515607</a>,
        <a href="https://bugs.debian.org/904184">n° 904184</a>).</li>
    </ul>
  </li>
  <li>partman-crypto :
    <ul>
      <li>mise en place de l'option <q>discard</q> dans les conteneurs LUKS
        (<a href="https://bugs.debian.org/902912">n° 902912</a>).</li>
    </ul>
  </li>
  <li>pkgsel :
    <ul>
      <li>plus d'installation d'unattended-upgrades par défaut
        <a href="https://lists.debian.org/debian-boot/2018/05/msg00250.html">comme
        demandé par l'équipe de sécurité de Debian</a> ;</li>
      <li>installation de nouvelles dépendances lors de la sélection (par
        défaut) de safe-upgrade (<a href="https://bugs.debian.org/908711">n° 908711</a>).
        En particulier, cela garantit que le paquet linux-image le plus
        récent est installé ;</li>
      <li>permission pour update-initramfs d'être exécuté normalement
        pendant la mise à niveau et l'installation du paquet du noyau (<a href="https://bugs.debian.org/912073">n° 912073</a>).</li>
    </ul>
  </li>
  <li>preseed :
    <ul>
      <li>chaîne « Checksum error » marquée comme traduisible.</li>
    </ul>
  </li>
</ul>


<h2>Modifications de la prise en charge matérielle</h2>

<ul>
  <li>debian-installer :
    <ul>
      <li>[armel] désactivation des cibles OpenRD, plus présentes dans
        u-boot ;</li>
      <li>[armhf] retrait de l'image Firefly-RK3288, parce que u-boot
        échoue à démarrer à cause de <a href="https://bugs.debian.org/898520">n° 898520</a> ;</li>
      <li>[armhf] ajout de l'image pour Sinovoip_BPI_M3 ;</li>
      <li>[arm64] ajout de l'image u-boot pour pinebook ;</li>
      <li>[mips*el/loongson-3] ajout de modules d'entrée à l'image netboot
        (<a href="https://bugs.debian.org/911664">n° 911664</a>).</li>
    </ul>
  </li>
  <li>flash-kernel :
    <ul>
      <li>ajout d'une entrée dans la base de données de matériel pour le
        système NAS Helios-4 (<a href="https://bugs.debian.org/914016">n° 914016</a>) ;</li>
      <li>ajout d'une entrée dans la base de données de matériel pour la
        carte Rockchip RK3288 Tinker (<a href="https://bugs.debian.org/895934">n° 895934</a>) ;</li>
      <li>mise à jour de la carte Firefly-RK3399 (<a href="https://bugs.debian.org/899091">n° 899091</a>) ;</li>
      <li>ajout de la carte Rockchip RK3399 Evaluation (<a href="https://bugs.debian.org/899090">n° 899090</a>) ;</li>
      <li>mise à jour de l'entrée pour Marvell 8040 MACCHIATOBin (<a href="https://bugs.debian.org/899092">n° 899092</a>) ;</li>
      <li>mise à jour de Pine64+ (<a href="https://bugs.debian.org/899093">n° 899093</a>) ;</li>
      <li>mise à jour de Raspberry Pi 3 Modèle B (<a href="https://bugs.debian.org/899096">n° 899096</a>) ;</li>
      <li>ajout d'une entrée pour Raspberry PI 3 B+ (<a href="https://bugs.debian.org/905002">n° 905002</a>) ;</li>
      <li>Clearfog Pro : correction du nom de DTB (<a href="https://bugs.debian.org/902432">n° 902432</a>) ;</li>
      <li>ajout d'entrées manquantes pour les variantes de HummingBoard
        (<a href="https://bugs.debian.org/905962">n° 905962</a>) ;</li>
      <li>ajout d'entrées d'autres modèles de Cubox-i : SolidRun Cubox-i
        Dual/Quad (1.5som) et SolidRun Cubox-i Dual/Quad (1.5som+emmc).</li>
    </ul>
  </li>
  <li>linux :
    <ul>
      <li>udeb : ajout de virtio_console à virtio-modules (<a href="https://bugs.debian.org/903122">n° 903122</a>).</li>
    </ul>
  </li>
  <li>parted :
    <ul>
      <li>correction de la lecture des noms de modèle NVMe à partir de
        sysfs (<a href="https://bugs.debian.org/911273">n° 911273</a>).</li>
    </ul>
  </li>
  <li>u-boot :
    <ul>
      <li>[armhf] u-boot-rockchip : retrait de la cible firefly-rk3288
        (<a href="https://bugs.debian.org/898520">n° 898520</a>) ;</li>
      <li>[arm64] u-boot-sunxi : activation de la cible a64-olinuxino
        (<a href="https://bugs.debian.org/881564">n° 881564</a>) ;</li>
      <li>[arm64] u-boot-sunxi : ajout de la cible pinebook ;</li>
      <li>[armel] retrait des cibles openrd (plus prises en charge par
        l'amont) ;</li>
      <li>[armhf] u-boot-sunxi : activation de Sinovoip Banana Pi M3
        (<a href="https://bugs.debian.org/905922">n° 905922</a>) ;</li>
      <li>u-boot-imx : retrait de la cible mx6cuboxi4x4, parce que la ram
        est maintenant correctement détectée avec mx6cuboxi ;</li>
      <li>[armhf] u-boot-sunxi : activation de A20-OLinuXino-Lime2-eMMC
        (<a href="https://bugs.debian.org/901666">n° 901666</a>).</li>
    </ul>
  </li>
</ul>


<h2>État de la localisation</h2>

<ul>
  <li>76 langues sont prises en charge dans cette version.<br />
    Note : l'anglais n'était pas décompté dans les annonces précédentes.
  </li>
  <li>La traduction est complète pour 25 de ces langues.<br />
    Beau travail de Holger Wansing, le nouveau coordinateur des
    traductions et de tous les traducteurs impliqués !
  </li>
</ul>


<h2>Problèmes connus dans cette version</h2>

<p>
Veuillez consulter les <a href="$(DEVEL)/debian-installer/errata">errata</a>
pour plus de détails et une liste complète des problèmes connus.
</p>


<h2>Retours d'expérience pour cette version</h2>

<p>
Nous avons besoin de votre aide pour trouver des bogues et améliorer encore
l'installateur, merci de l'essayer. Les CD, les autres supports d'installation,
et tout ce dont vous pouvez avoir besoin sont disponibles sur notre
<a href="$(DEVEL)/debian-installer">site web</a>.
</p>


<h2>Remerciements</h2>

<p>
L'équipe du programme d'installation Debian remercie toutes les personnes ayant
pris part à cette publication.
</p>
