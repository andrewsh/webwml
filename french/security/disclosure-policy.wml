#use wml::debian::template title="Politique de divulgation de vulnérabilités de Debian"
#use wml::debian::translation-check translation="5a5aa2dd64b9f93c369cb9c01eafa0bef14dafa8"

Ce document décrit la politique de divulgation de vulnérabilités et de leur
embargo du projet Debian, conformément au statut de Debian comme autorité
de numérotation de CVE (Sub-CNA,
<a href="https://cve.mitre.org/cve/cna/rules.html#section_2-3_record_management_rules">rule 2.3.3</a>).
Veuillez aussi consulter la <a href="faq">FAQ de l'équipe Debian sur la
sécurité</a> pour plus d’informations sur les procédures de sécurité de Debian.

<h2>Dispositions générales</h2>

<p>L’équipe de sécurité de Debian entre en contact habituellement avec les
rapporteurs de vulnérabilité sous quelques jours. (Consultez les rubriques de la
FAQ pour <a href="faq#contact">joindre l’équipe de sécurité</a> et
<a href="faq#discover">rapporter des vulnérabilités</a>.)

<p>La plus grande partie des logiciels faisant partie du système d’exploitation
Debian n’a pas été écrite spécialement pour Debian. Le système d’exploitation
Debian dans son ensemble sert aussi de base pour d’autres distributions
GNU/Linux. Cela signifie que la plupart des vulnérabilités affectant Debian
affectent aussi d’autres distributions, et, dans beaucoup de cas, des
fournisseurs de logiciels commerciaux. En conséquence, la divulgation de
vulnérabilités doit être coordonnée avec les autres parties, pas simplement
entre le rapporteur et seulement Debian.</p>

<p>Un forum pour une telle coordination est la
<a href="https://oss-security.openwall.org/wiki/mailing-lists/distros">liste des
distributions</a>. L’équipe de sécurité de Debian espère que les chercheurs
expérimentés en sécurité contactent directement la liste des distributions et
les projets amont concernés. L’équipe de sécurité de Debian fournit une
assistance aux autres rapporteurs selon les besoins. Avant d’impliquer des
parties tierces, une permission des rapporteurs est demandée.</p>

<h2>Chronologie</h2>

<p>Comme indiqué ci-dessus, une confirmation par courriel du rapport initial ne
devrait pas prendre plus que quelques jours.</p>

<p>Puisque la correction de la plupart des vulnérabilités dans les logiciels de
Debian demande une coordination entre plusieurs parties (développeurs amont,
autres distributions), le délai entre le rapport de vulnérabilité initial et sa
divulgation publique varie beaucoup selon le logiciel et les organisations
concernées.</p>

<p>La liste des distributions limite la période d’embargo (délai entre le
rapport initial et la divulgation) à deux semaines. Cependant, des délais plus
longs ne sont pas inhabituels, en fonction de coordinations supplémentaires
avant de partager avec la liste des distributions et d’adaptations avec les
cycles mensuels voire trimestriels de publication de fournisseurs. Corriger les
vulnérabilités du protocole Internet peut même prendre plus longtemps, et il en
est de même pour développer des tentatives d’atténuation de vulnérabilités
matérielles dans des logiciels.</p>

<h2>Empêchement des embargos</h2>

<p>Dans la mesure où la coordination en privé est propice à créer des frictions
et rend difficile d’impliquer les bons experts en la matière, Debian encourage
une divulgation publique des vulnérabilités avant même le développement d’un
correctif, sauf si une telle démarche mettrait clairement en danger les
utilisateurs de Debian ou les autres parties.</p>

<p>L’équipe de sécurité de Debian demande souvent aux rapporteurs de transmettre
les rapports de bogue publics dans le(s) système(s) approprié(s) de suivi de
bogues (tels que le <a href="../Bugs/">système de suivi de bogues de Debian</a>),
en fournissant une aide selon les besoins.</p>

<p>Un embargo n’est pas nécessaire pour une affectation CVE ou pour accorder une
annonce de sécurité.</p>

<h2>Affectation CVE</h2>

<p>Debian, en tant qu’autorité auxiliaire de numérotation (sub-CNA), affecte
seulement des identifiants CVE pour les vulnérabilités de Debian. Si un rapport
de vulnérabilité ne satisfait pas ce critère, et par conséquent est en dehors du
périmètre du CNA de Debian, l’équipe de sécurité de Debian s’arrangera avec
un autre CNA pour une affectation d’identifiant CVE ou guidera le rapporteur
pour une soumission de sa propre requête d’identifiant CVE.</p>

<p>Une affectation CVE par le CNA Debian est rendue publique par la publication
d’une annonce de sécurité Debian, ou quand le bogue est transmis aux systèmes
appropriés de suivi de bogues.</p>

<h2>Vulnérabilité contre bogue normal</h2>

<p>À cause du large éventail de logiciels faisant partie du système
d’exploitation Debian, il n’est pas possible de fournir une indication sur ce
qui constitue une vulnérabilité de sécurité et sur ce qui n’est qu’un bogue
logiciel ordinaire. En cas de doute, veuillez contacter l’équipe de sécurité de
Debian.</p>

<h2>Programme de prime pour des bogues</h2>

<p>Debian ne propose pas de programme de primes pour les bogues. Les parties
extérieures peuvent encourager les rapporteurs à les contacter à propos de
vulnérabilités dans Debian, mais elles ne sont pas cautionnées par le projet
Debian.</p>

<h2>Vulnérabilités dans l’infrastructure de Debian</h2>

<p>Les rapports de vulnérabilité dans l’infrastructure propre de Debian sont
gérés de la même façon. Si cette vulnérabilité d’infrastructure n’est pas le
résultat d’une mauvaise configuration, mais une vulnérabilité dans un logiciel
utilisé, la coordination multipartie habituelle est nécessaire, avec un
calendrier similaire à celui décrit ci-dessus.</p>
