#use wml::debian::translation-check translation="fa7bd3e4644f60a727dbe8b56ea07b03f7ff7fc2" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pouvaient conduire à une élévation de privilèges, un déni de service ou des
fuites d'informations:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33655">CVE-2021-33655</a>

<p>Un utilisateur doté d'un accès au pilote de tampon de trame de la console
pouvait provoquer une écriture de mémoire hors limites au moyen du ioctl
FBIOPUT_VSCREENINFO.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2318">CVE-2022-2318</a>

<p>Une utilisation de mémoire après libération dans la prise en charge du
protocole de radio amateur X.25 PLP (Rose) peut avoir pour conséquence un
déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26365">CVE-2022-26365</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2022-33740">CVE-2022-33740</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2022-33741">CVE-2022-33741</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2022-33742">CVE-2022-33742</a>

<p>Roger Pau Monne a découvert que les frontaux de périphériques bloc et
réseau paravirtuels ne mettaient pas à zéro les régions de mémoire avant de
les partager avec le dorsal, ce qui peut avoir pour conséquence la
divulgation d'informations. En complément, il a été découvert que la
granularité de « grant table » ne permettait pas de partager des pages de
moins de 4 Ko, ce qui peut aussi avoir pour conséquence la divulgation
d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-33743">CVE-2022-33743</a>

<p>Jan Beulich a découvert qu'un traitement incorrect de la mémoire dans le
dorsal réseau de Xen pouvait conduire à déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-33744">CVE-2022-33744</a>

<p>Oleksandr Tyshchenko a découvert que les clients Xen ARM peuvent
provoquer un déni de service pour le Dom0 aux moyens de périphériques
paravirtuels.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-34918">CVE-2022-34918</a>

<p>Arthur Mongodin a découvert un dépassement de tampon de tas dans le
sous-système Netfilter qui peut avoir pour conséquence une élévation
locale de privilèges.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 5.10.127-2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5191.data"
# $Id: $
