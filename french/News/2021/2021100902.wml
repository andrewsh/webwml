#use wml::debian::translation-check translation="d9e5cd3d7df23feb17458b95c465e062e9cd6e5a" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 10.11</define-tag>
<define-tag release_date>2021-10-09</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>10</define-tag>
<define-tag codename>Buster</define-tag>
<define-tag revision>10.11</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la onzième mise à jour de sa
distribution oldstable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans ce
document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version de
Debian <release> mais seulement une mise à jour de certains des paquets qu'elle
contient. Il n'est pas nécessaire de jeter les anciens médias de la
version <codename>. Après installation, les paquets peuvent être mis à niveau
vers les versions courantes en utilisant un miroir Debian à jour.
</p>

<p>
Ceux qui installent fréquemment les mises à jour à partir de security.debian.org
n'auront pas beaucoup de paquets à mettre à jour et la plupart des mises à jour
de security.debian.org sont comprises dans cette mise à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP de
Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Corrections de bogues divers</h2>

<p>
Cette mise à jour de la version oldstable apporte quelques corrections
importantes aux paquets suivants :
</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction atftp "Correction de dépassement de tampon [CVE-2021-41054]">
<correction base-files "Mise à jour pour cette version">
<correction btrbk "Correction d'un problème d'exécution de code arbitraire [CVE-2021-38173]">
<correction clamav "Nouvelle version amont stable ; correction d'erreurs de segmentation de clamdscan quand --fdpass et --multipass sont utilisés conjointement avec ExcludePath">
<correction commons-io "Correction d'un problème de traversée de répertoire [CVE-2021-29425]">
<correction cyrus-imapd "Correction d'un problème de déni de service [CVE-2021-33582]">
<correction debconf "Vérification que whiptail ou dialog sont vraiment utilisables">
<correction debian-installer "Reconstruction avec buster-proposed-updates ; mise à jour de l'ABI de Linux vers la version 4.19.0-18">
<correction debian-installer-netboot-images "Reconstruction avec buster-proposed-updates">
<correction distcc "Correction des liens du compilateur croisé GCC dans update-distcc-symlinks et ajout de la prise en charge de clang et de CUDA (nvcc)">
<correction distro-info-data "Mise à jour des données incluses pour plusieurs publications">
<correction dwarf-fortress "Retrait de bibliothèques partagées pré-construites non distribuables de l'archive source">
<correction espeak-ng "Correction de l'utilisation d'espeak avec mbrola-fr4 quand mbrola-fr1 n'est pas installé">
<correction gcc-mingw-w64 "Correction de la gestion de gcov">
<correction gthumb "Correction d'un problème de dépassement de tas [CVE-2019-20326]">
<correction hg-git "Correction d'échec de test avec les versions récentes de Git">
<correction htslib "Correction d'autopkgtest sur i386">
<correction http-parser "Correction d'un problème de dissimulation de requête HTTP [CVE-2019-15605]">
<correction irssi "Correction d'un problème d'utilisation de mémoire après libération lors de l'envoi de connexion SASL au serveur [CVE-2019-13045]">
<correction java-atk-wrapper "Utilisation également de dbus pour détecter l'activation de l'accessibilité">
<correction krb5 "Correction d'un plantage de déréférencement de pointeur NULL de KDC dans les requêtes FAST sans champ de serveur [CVE-2021-37750] ; correction de fuite de mémoire dans krb5_gss_inquire_cred">
<correction libdatetime-timezone-perl "Nouvelle version amont stable ; mise à jour des règles DST pour les Samoa et la Jordanie ; confirmation de l'absence de seconde intercalaire le 31 décembre 2021">
<correction libpam-tacplus "Ajout évité des secrets partagés en texte clair dans le journal système [CVE-2020-13881]">
<correction linux "<q>proc : Vérification que mm_struct ouvre /proc/$pid/attr/</q>, corrigeant des problèmes avec lxc-attach ; nouvelle version amont stable ; passage de l'ABI à la version 18 ; [rt] mise à jour vers la version 4.19.207-rt88 ; usb : hso : correction du code de gestion d'erreur de hso_create_net_device [CVE-2021-37159]">
<correction linux-latest "Mise à jour vers la version 4.19.0-18 de l'ABI du noyau">
<correction linux-signed-amd64 "<q>proc : Vérification que mm_struct ouvre /proc/$pid/attr/</q>, corrigeant des problèmes avec lxc-attach ; nouvelle version amont stable ; passage de l'ABI à la version 18 ; [rt] mise à jour vers la version 4.19.207-rt88 ; usb : hso : correction du code de gestion d'erreur de hso_create_net_device [CVE-2021-37159]">
<correction linux-signed-arm64 "<q>proc : Vérification que mm_struct ouvre /proc/$pid/attr/</q>, corrigeant des problèmes avec lxc-attach ; nouvelle version amont stable ; passage de l'ABI à la version 18 ; [rt] mise à jour vers la version 4.19.207-rt88 ; usb : hso : correction du code de gestion d'erreur de hso_create_net_device [CVE-2021-37159]">
<correction linux-signed-i386 "<q>proc : Vérification que mm_struct ouvre /proc/$pid/attr/</q>, corrigeant des problèmes avec lxc-attach ; nouvelle version amont stable ; passage de l'ABI à la version 18 ; [rt] mise à jour vers la version 4.19.207-rt88 ; usb : hso : correction du code de gestion d'erreur de hso_create_net_device [CVE-2021-37159]">
<correction mariadb-10.3 "Nouvelle version amont stable ; corrections de sécurité [CVE-2021-2389 CVE-2021-2372] ; correction de chemin d'exécutable Perl dans les scripts">
<correction modsecurity-crs "Correction d'un problème de contournement de corps de requête [CVE-2021-35368]">
<correction node-ansi-regex "Correction d'un problème de déni de service basé sur les expressions rationnelles [CVE-2021-3807]">
<correction node-axios "Correction d'un problème de déni de service basé sur les expressions rationnelles [CVE-2021-3749]">
<correction node-jszip "Utilisation d'un objet prototype vide pour ses fichiers [CVE-2021-23413]">
<correction node-tar "Retrait des chemins qui ne sont pas des répertoires du cache de répertoire [CVE-2021-32803] ; retrait des chemins absolus de façon plus complète [CVE-2021-32804]">
<correction nvidia-cuda-toolkit "Correction du réglage de NVVMIR_LIBRARY_DIR sur ppc64el">
<correction nvidia-graphics-drivers "Nouvelle version amont stable ; correction de problèmes de déni de service [CVE-2021-1093 CVE-2021-1094 CVE-2021-1095] ; nvidia-driver-libs : ajout d'un paquet recommandé : libnvidia-encode1">
<correction nvidia-graphics-drivers-legacy-390xx "Nouvelle version amont stable ; correction de problèmes de déni de service [CVE-2021-1093 CVE-2021-1094 CVE-2021-1095] ; nvidia-legacy-390xx-driver-libs : ajout d'un paquet recommandé : libnvidia-legacy-390xx-encode1">
<correction postgresql-11 "Nouvelle version amont stable ; correction d'une mauvaise programmation de l'application répétée d'une étape de projection [CVE-2021-3677] ; interdiction plus complète des renégociations SSL">
<correction proftpd-dfsg "Correction de <q>mod_radius divulgue des contenus de la mémoire au serveur radius</q>>, <q>désactivation impossible des renégociations initiées par le client pour FTPS</q>, navigation dans les répertoires avec les liens symboliques, plantage de mod_sftp lors de l'utilisation de pubkey-auth avec les clés DSA">
<correction psmisc "Correction de régression dans killall qui ne trouve pas de correspondance avec les processus dont le nom compte plus de 15 caractères">
<correction python-uflash "Mise à jour de l'URL du microprogramme">
<correction request-tracker4 "Correction d'un problème d'attaque temporelle par canal auxiliaire pour la connexion [CVE-2021-38562]">
<correction ring "Correction d'un problème de déni de service dans la copie incorporée de pjproject [CVE-2021-21375]">
<correction sabnzbdplus "Échappement de répertoire évité dans la fonction de renommage [CVE-2021-29488]">
<correction shim "Ajout d'un correctif d'arm64 pour adapter l'affichage des sections et supprimer des problèmes de plantage ; en mode non sûr, pas d'abandon si la variable MokListXRT ne peut pas être créée ; pas d'abandon lors d'échec d'installation de grub : avertissement à la place">
<correction shim-helpers-amd64-signed "Ajout d'un correctif d'arm64 pour adapter l'affichage des sections et supprimer des problèmes de plantage ; en mode non sûr, pas d'abandon si la variable MokListXRT ne peut pas être créée ; pas d'abandon lors d'échec d'installation de grub : avertissement à la place">
<correction shim-helpers-arm64-signed "Ajout d'un correctif d'arm64 pour adapter l'affichage des sections et supprimer des problèmes de plantage ; en mode non sûr, pas d'abandon si la variable MokListXRT ne peut pas être créée ; pas d'abandon lors d'échec d'installation de grub : avertissement à la place">
<correction shim-helpers-i386-signed "Ajout d'un correctif d'arm64 pour adapter l'affichage des sections et supprimer des problèmes de plantage ; en mode non sûr, pas d'abandon si la variable MokListXRT ne peut pas être créée ; pas d'abandon lors d'échec d'installation de grub : avertissement à la place">
<correction shim-signed "Contournement de problèmes d'interruption d'amorçage sur arm64 par l'inclusion d'une version plus ancienne, connue pour fonctionner, de shim non signé sur cette plateforme ; retour en arrière d'arm64 pour utiliser une construction actuelle non signée ; ajout d'un correctif d'arm64 pour adapter l'affichage des sections et supprimer des problèmes de plantage ; en mode non sûr, pas d'abandon si la variable MokListXRT ne peut pas être créée ; pas d'abandon lors d'échec d'installation de grub : avertissement à la place">
<correction shiro "Correction de problèmes de contournement d'authentification [CVE-2020-1957 CVE-2020-11989 CVE-2020-13933 CVE-2020-17510] ; mise à jour du correctif de la compatibilité avec le cadriciel Spring ; prise en charge de Guice 4">
<correction tzdata "Mise à jour des règles DST pour les Samoa et la Jordanie ; confirmation de l'absence de seconde intercalaire le 31 décembre 2021">
<correction ublock-origin "Nouvelle version amont stable ; correction d'un problème de déni de service [CVE-2021-36773]">
<correction ulfius "Initialisation assurée de la mémoire avant utilisation [CVE-2021-40540]">
<correction xmlgraphics-commons "Correction de contrefaçon de requête côté serveur [CVE-2020-11988]">
<correction yubikey-manager "Ajout à yubikey-manager de la dépendance à python3-pkg-resources">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
oldstable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2021 4842 thunderbird>
<dsa 2021 4866 thunderbird>
<dsa 2021 4876 thunderbird>
<dsa 2021 4897 thunderbird>
<dsa 2021 4927 thunderbird>
<dsa 2021 4931 xen>
<dsa 2021 4932 tor>
<dsa 2021 4933 nettle>
<dsa 2021 4934 intel-microcode>
<dsa 2021 4935 php7.3>
<dsa 2021 4936 libuv1>
<dsa 2021 4937 apache2>
<dsa 2021 4938 linuxptp>
<dsa 2021 4939 firefox-esr>
<dsa 2021 4940 thunderbird>
<dsa 2021 4941 linux-signed-amd64>
<dsa 2021 4941 linux-signed-arm64>
<dsa 2021 4941 linux-signed-i386>
<dsa 2021 4941 linux>
<dsa 2021 4942 systemd>
<dsa 2021 4943 lemonldap-ng>
<dsa 2021 4944 krb5>
<dsa 2021 4945 webkit2gtk>
<dsa 2021 4946 openjdk-11-jre-dcevm>
<dsa 2021 4946 openjdk-11>
<dsa 2021 4947 libsndfile>
<dsa 2021 4948 aspell>
<dsa 2021 4949 jetty9>
<dsa 2021 4950 ansible>
<dsa 2021 4951 bluez>
<dsa 2021 4952 tomcat9>
<dsa 2021 4953 lynx>
<dsa 2021 4954 c-ares>
<dsa 2021 4955 libspf2>
<dsa 2021 4956 firefox-esr>
<dsa 2021 4957 trafficserver>
<dsa 2021 4958 exiv2>
<dsa 2021 4959 thunderbird>
<dsa 2021 4961 tor>
<dsa 2021 4962 ledgersmb>
<dsa 2021 4963 openssl>
<dsa 2021 4964 grilo>
<dsa 2021 4967 squashfs-tools>
<dsa 2021 4969 firefox-esr>
<dsa 2021 4970 postorius>
<dsa 2021 4971 ntfs-3g>
<dsa 2021 4973 thunderbird>
<dsa 2021 4974 nextcloud-desktop>
<dsa 2021 4975 webkit2gtk>
<dsa 2021 4979 mediawiki>
</table>


<h2>Paquets supprimés</h2>

<p>Les paquets suivants ont été supprimés à cause de circonstances hors de
notre contrôle :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction birdtray "Incompatible avec les versions actuelles de Thunderbird">
<correction libprotocol-acme-perl "Prise en charge uniquement de la version 1 obsolète d'ACME">

</table>

<h2>Installateur Debian</h2>
<p>
L'installateur a été mis à jour pour inclure les correctifs incorporés dans
cette version de oldstable.
</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>
Adresse de l'actuelle distribution oldstable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>
Mises à jour proposées à la distribution oldstable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>
Informations sur la distribution oldstable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres qui
offrent volontairement leur temps et leurs efforts pour produire le système
d'exploitation complètement libre Debian.
</p>


<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a>, envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de publication de
la version stable à &lt;debian-release@lists.debian.org&gt;.
