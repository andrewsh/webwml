#use wml::debian::template title="Comment effectuer un don au projet Debian"
#use wml::debian::translation-check translation="77730b1058b58e76cb29910f6acf353ec5bf9847" maintainer="Jean-Pierre Giraud"
# Previous translation by Norbert Bottlaender-Prier
# Translators:
# Frédéric Bothamy, 2004-2007
# Baptiste Jammet, 2014-2021
# Jean-Pierre Giraud, 2022

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#spi">Software in the Public Interest</a></li>
  <li><a href="#debianfrance">Debian France</a></li>
  <li><a href="#debianch">debian.ch</a></li>
  <li><a href="#debian">Debian</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Merci à tous les donateurs qui soutiennent Debian avec de l'équipement ou des services : <a href="https://db.debian.org/machines.cgi">hébergement et sponsors de matériel</a>, <a href="mirror/sponsors">sponsors des miroirs</a>, <a href="partners/">partenaires <q>développement et services</q></a>.<p>
</aside>

<p>
Les dons sont gérés par le <a href="$(HOME)/devel/leader">chef de projet Debian</a>
(DPL) et permettent à Debian de posséder des
<a href="https://db.debian.org/machines.cgi">machines</a>, 
différents types de <a href="https://wiki.debian.org/Teams/DSA/non-DSA-HW">matériels</a>, 
des noms de domaine, des certificats chiffrés, 
d'organiser la <a href="https://www.debconf.org">conférence annuelle Debian</a>, 
des <a href="https://wiki.debian.org/MiniDebConf">mini-conférences Debian</a>, 
des <a href="https://wiki.debian.org/Sprints">rencontres pour le développement</a>, 
d’assister à différents types d'évènements et bien d'autres choses.
</p>

<p id="default">
Plusieurs <a href="https://wiki.debian.org/Teams/Treasurer/Organizations">organisations</a>
détiennent des actifs pour le compte de Debian et reçoivent des dons pour Debian.
Cette page liste les différentes manières d'effectuer un don au projet Debian. La
méthode la plus simple pour faire un don à Debian est de le faire par PayPal
(NdT : en dollars américains) à
<a href="https://www.spi-inc.org/" title="SPI">Software in the Public Interest</a>,
une organisation à but non lucratif de droit américain qui détient les actifs de
Debian.
</p>

<p style="text-align:center"><button type="button"><span class="fab fa-paypal fa-2x"></span> <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=86AHSZNRR29UU">Faire un don avec PayPal</a></button></p>

<aside class="light">
  <span class="fas fa-gifts fa-5x"></span>
</aside>

<table>
<tr>
<th>Organisation</th>
<th>Méthodes</th>
<th>Notes</th>
</tr>
<tr>
<td><a href="#spi"><acronym title="Software in the Public Interest">SPI</acronym></a></td>
<td>
 <a href="#spi-paypal">PayPal</a>,
 <a href="#spi-click-n-pledge">Click &amp; Pledge</a>,
 <a href="#spi-cheque">Chèque</a>,
 <a href="#spi-other">autre</a>
</td>
<td>USA, remise d'impôts (américains), à but non lucratif</td>
</tr>
<tr>
<td><a href="#debianfrance">Debian France</a></td>
<td>
 <a href="#debianfrance-bank">virement</a>,
 <a href="#debianfrance-paypal">PayPal</a>
</td>
<td>France, remise d'impôts (français), à but non lucratif</td>
</tr>
<tr>
<td><a href="#debianch">debian.ch</a></td>
<td>
 <a href="#debianch-bank">virement</a>,
 <a href="#debianch-other">autre</a>
</td>
<td>Suisse, à but non lucratif</td>
</tr>
<tr>
<td><a href="#debian">Debian</a></td>
<td>
 <a href="#debian-equipment">équipement</a>,
 <a href="#debian-time">temps</a>,
 <a href="#debian-other">autre</a>
</td>
<td></td>
</tr>

# Template:
#<tr>
#<td><a href="#"><acronym title=""></acronym></a></td>
#<td>
# <a href="#"></a>,
# <a href="#"></a> (autorise les dons réguliers),
# <a href="#cheque"></a> (devise)
#</td>
#<td>, remise d'impôts, à but non lucratif</td>
#</tr>

</table>

<h2 id="spi">Software in the Public Interest</h2>

<p>
<a href="https://www.spi-inc.org/" title="SPI">Software in the Public Interest, Inc.</a>
est une organisation à but non lucratif de droit américain, basée aux États-Unis,
fondée par des membres de Debian en 1997 pour soutenir les organismes investis
dans les logiciels et les matériels libres.
</p>

<h3 id="spi-paypal">PayPal</h3>

<p>
Les dons uniques ou réguliers peuvent être effectués sur la
<a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=86AHSZNRR29UU">page de SPI</a>
du site de PayPal. Pour faire un don régulier, cochez la case <em>Make this a
monthly donation</em> (faire un don mensuel, NdT).
</p>

<h3 id="spi-click-n-pledge">Click &amp; Pledge</h3>

<p>
Les dons uniques ou réguliers peuvent être effectués sur la
<a href="https://co.clickandpledge.com/advanced/default.aspx?wid=34115">page de SPI</a>
du site de Click &amp; Pledge (en anglais). Pour faire un don régulier,
choisissez la fréquence du don sur la droite, (<em>Repeat payment</em>),
descendez jusqu'à <em>Debian Project Donation</em>, entrez le montant que vous
souhaitez donner, cliquez sur le bouton <q>Add to cart</q> et continuez jusqu'à
la fin du processus.
</p>

<h3 id="spi-cheque">Chèque</h3>

<p>
Les dons peuvent être effectués par chèque ou par mandat en dollars américains
(<abbr title="United States dollar">USD</abbr> et en dollars canadiens
(<abbr title="Canadian dollar">CAD</abbr>). Veuillez noter Debian dans le champ
<q>memo</q> et envoyer à SPI à l'adresse indiquée sur la
<a href="https://www.spi-inc.org/donations/">page des dons de SPI</a>.
</p>

<h3 id="spi-other">Autre</h3>

<p>
Les dons par virement ou d'autres méthodes sont aussi possibles. Pour certains
pays, il peut être préférable de faire un don à une des organisations
partenaires de Software in the Public Interest. Pour plus d'informations,
veuillez consulter la <a href="https://www.spi-inc.org/donations/">page des dons de SPI</a>.
</p>

<h2 id="debianfrance">Debian France</h2>

<p>
L'<a href="https://france.debian.net/">association Debian France</a> est une
association française loi 1901, fondée pour soutenir et promouvoir le projet
Debian en France.
</p>

<h3 id="debianfrance-bank">Virement</h3>

<p>
Les dons par virement sont possibles sur le compte bancaire indiqué sur la
<a href="https://france.debian.net/soutenir/#compte">page des dons de Debian France</a>.
Des reçus pour ces dons sont disponibles en envoyant un courriel à
<a href="mailto:donation@france.debian.net">donation@france.debian.net</a>.
</p>

<h3 id="debianfrance-paypal">PayPal</h3>

<p>
Les dons peuvent être envoyés par
<a href="https://france.debian.net/galette/plugins/paypal/form">la page PayPal de Debian France</a>.
Ils peuvent être assignés spécifiquement à Debian France ou au projet Debian en
général.
</p>

<h2 id="debianch">debian.ch</h2>

<p>
<a href="https://debian.ch/">debian.ch</a> a été fondée pour représenter le
projet Debian en Suisse et dans la principauté du Liechtenstein.
</p>

<h3 id="debianch-bank">Virement</h3>

<p>
Les dons par virement depuis la Suisse ou les banques internationales sont
possibles vers les comptes listés sur <a href="https://debian.ch/">le site debian.ch</a>.
</p>

<h3 id="debianch-other">Autre</h3>

<p>
Les dons par d'autres méthodes sont possibles en contactant l'adresse dédiée
aux dons indiquée sur <a href="https://debian.ch/">le site debian.ch</a>.
</p>

# Template:
#<h3 id=""></h3>
#
#<p>
#</p>
#
#<h4 id=""></h4>
#
#<p>
#</p>

<h2 id="debian">Debian</h2>

<p>
Debian est en mesure de recevoir des dons <a href="#debian-equipment">d'équipement</a>,
mais pas <a href="#debian-other">d'autres types</a> de dons pour le moment.
</p>

<h3 id="debian-equipment">Dons de matériels et de services</h3>

<p>
Debian compte aussi sur le don de matériels et de services de la part de
personnes, d'entreprises et d'universités, etc. afin de rester connectée au
monde.
</p>

<p>
Si votre entreprise possède des machines inutilisées ou de l'équipement de
réserve (disques durs, contrôleurs SCSI, cartes réseau, etc.), nous vous
saurions gré d'envisager de les donner à Debian. Veuillez contacter notre
<a href="mailto:hardware-donations@debian.org">délégué aux dons de matériel</a>
pour demander des précisions.
</p>

<p>
Debian maintient une <a href="https://wiki.debian.org/Hardware/Wanted">liste des
matériels recherchés</a> pour différents services et groupes au sein du projet.
</p>

<h3 id="debian-time">Temps</h3>

<p>
Il y a plusieurs façons <a href="$(HOME)/intro/help">d'aider Debian</a> durant
vos loisirs ou pendant votre temps de travail.
</p>

<h3 id="debian-other">Autre</h3>

<p>
Debian n'est pas en mesure, pour le moment, de recevoir de la <q>crypto-monnaie</q>
mais nous cherchons une façon de prendre en charge cette méthode de
dons.
# Brian Gupta requested we discuss this before including it:
#If you have cryptocurrency to donate, or insights to share, please
#get in touch with <a href="mailto:madduck@debian.org">Martin f. krafft</a>.
