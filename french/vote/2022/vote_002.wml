<define-tag pagetitle>Élection du responsable du projet Debian 2022</define-tag>
<define-tag status>F</define-tag>
# signification des balises <status> :
# P: proposé
# D: débattu
# V: voté
# F: terminé
# O: autre (ou indiquez simplement autre chose)

#use wml::debian::translation-check translation="4745d69aeced534d69d6525056084177fc4635f9" maintainer="Jean-Pierre Giraud"
#use wml::debian::template title="<pagetitle>" BARETITLE="true" NOHEADER="true"
#use wml::debian::toc
#use wml::debian::votebar


    <h1><pagetitle></h1>
    <toc-display />



# The Tags beginning with v are will become H3 headings and are defined in
# english/template/debian/votebar.wml
# all possible Tags:

# vdate, vtimeline, vnominations, vdebate, vplatforms,
# Proposers
#          vproposer,  vproposera, vproposerb, vproposerc, vproposerd,
#          vproposere, vproposerf
# Seconds
#          vseconds,   vsecondsa, vsecondsb, vsecondsc, vsecondsd, vsecondse,
#          vsecondsf,  vopposition
# vtext, vtextb, vtextc, vtextd, vtexte, vtextf
# vchoices
# vamendments, vamendmentproposer, vamendmentseconds, vamendmenttext
# vproceedings, vmajorityreq, vstatistics, vquorum, vmindiscuss,
# vballot, vforum, voutcome


<vtimeline />
    <table class="vote">
     <tr>
      <th>Période de candidature :</th>
        <td>samedi 5 mars 2022 00:00:00 UTC</td>
        <td>vendredi 11 mars 2022 23:59:59 UTC</td>
    </tr>
    <tr>
        <th>Période de campagne :</th>
        <td>samedi 12 mars 2022 00:00:00 UTC</td>
        <td>vendredi 1er avril 2022 23:59:59 UTC</td>
    </tr>
    <tr>
        <th>Période de scrutin :</th>
        <td>dimanche 3 avril 2022 00:00:00 UTC</td>
        <td>samedi 16 avril 2022 23:59:59 UTC</td>
     </tr>
     </table>
<p>Veuillez noter que le nouveau mandat du responsable du projet débutera
le 21 avril 2022.</p>

<vnominations />
  <ol>
      <li>Felix Lechner [<email lechner@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/03/msg00093.html'>message de candidature</a>] [<a href="platforms/lechner">programme</a>]
      <li>Jonathan Carter [<email jcc@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/03/msg00098.html'>message de candidature</a>] [<a href="platforms/jcc">programme</a>]
      <li>Hideki Yamane [<email henrich@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/03/msg00099.html'>message de candidature</a>] [<a href="platforms/henrich">programme</a>]
  </ol>


<p>
Les bulletins, quand ils sont prêts, peuvent être demandés en envoyant un
courrier électronique signé à <a href="mailto:ballot@vote.debian.org">
ballot@vote.debian.org</a> avec pour sujet leader2022.
</p>

<vstatistics />
<p>
Cette année, comme d'habitude, des
#<a href="https://vote.debian.org/~secretary/leader2022/">statistiques</a>
<a href="suppl_002_stats">statistiques</a>
sur les bulletins et les accusés de réception seront rassemblées périodiquement
durant la période du scrutin.
De plus, la liste des <a href="vote_002_voters.txt">votants</a> sera
enregistrée. La <a href="vote_002_tally.txt">feuille d'émargement</a> sera
également disponible. Veuillez noter que l'élection du responsable du projet se
fait à bulletins secrets, la feuille d'émargement ne contiendra donc pas le nom
des votants mais un HMAC qui permet aux votants de vérifier que leur vote est
dans la liste. Une clef est générée pour chaque votant, et envoyée avec
l'accusé de réception de leur bulletin.
</p>


<vquorum />
<p>
Avec la liste actuelle des <a href="vote_002_quorum.log">développeurs
votants</a>, nous avons&nbsp;:

</p>
<pre>
#include 'vote_002_quorum.txt'
</pre>
#include 'vote_002_quorum.src'


<vmajorityreq />
<p> Le candidat a besoin d'une majorité simple pour être élu.</p>

#include 'vote_002_majority.src'


<voutcome />
#include 'vote_002_results.src'

<hrline>
  <address>
    <a href="mailto:secretary@debian.org"> Secrétaire du Projet Debian</a>
  </address>
