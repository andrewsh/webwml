<define-tag pagetitle>Résolution générale : secret du vote</define-tag>
<define-tag status>F</define-tag>
# meanings of the <status> tag:
# P: proposed
# D: discussed
# V: voted on
# F: finished
# O: other (or just write anything else)

#use wml::debian::translation-check translation="fd564451a031ab618242e44e27c73bef55adfc80" maintainer="Jean-Pierre Giraud"
#use wml::debian::template title="<pagetitle>" BARETITLE="true" NOHEADER="true"
#use wml::debian::toc
#use wml::debian::votebar


    <h1><pagetitle></h1>
    <toc-display />

# The Tags beginning with v are will become H3 headings and are defined in
# english/template/debian/votebar.wml
# all possible Tags:

# vdate, vtimeline, vnominations, vdebate, vplatforms,
# Proposers
#          vproposer,  vproposera, vproposerb, vproposerc, vproposerd,
#          vproposere, vproposerf
# Seconds
#          vseconds,   vsecondsa, vsecondsb, vsecondsc, vsecondsd, vsecondse,
#          vsecondsf,  vopposition
# vtext, vtextb, vtextc, vtextd, vtexte, vtextf
# vchoices
# vamendments, vamendmentproposer, vamendmentseconds, vamendmenttext
# vproceedings, vmajorityreq, vstatistics, vquorum, vmindiscuss,
# vballot, vforum, voutcome


    <vtimeline />
    <table class="vote">
      <tr>
        <th>Période de débat :</th>
		<td>23 février 2022</td>
		<td>11 mars 2022</td>
      </tr>
          <tr>
            <th>Période de vote :</th>
            <td>dimanche 13 mars 2022 00:00:00 UTC</td>
            <td>samedi 26 mars 2022 23:59:59 UTC</td>
    </table>

    <vproposera />
    <p>Sam Hartman [<email hartmans@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2022/02/msg00099.html'>texte de la proposition</a>]
	[<a href='https://lists.debian.org/debian-vote/2022/03/msg00018.html'>amendement</a>]
    </p>
    <vsecondsa />
    <ol>
        <li>Russ Allbery [<email rra@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/02/msg00101.html'>message</a>]</li>
        <li>Steve McIntyre [<email 93sam@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/02/msg00102.html'>message</a>]</li>
        <li>Bill Blough [<email bblough@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/02/msg00103.html'>message</a>]</li>
        <li>Filippo Rusconi [<email lopippo@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/02/msg00104.html'>message</a>]</li>
        <li>Sean Whitton [<email spwhitton@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/02/msg00105.html'>message</a>]</li>
        <li>Pierre-Elliott Bécue [<email peb@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/02/msg00106.html'>message</a>]</li>
    </ol>
    <vtexta />
	<h3>Cacher l'identité des développeurs votant à un scrutin particulier</h3>

<h4>Raison</h4>

<p>Durant le scrutin pour la résolution générale GR_2021_002, plusieurs
développeurs ont déclaré qu'ils n'étaient pas à l'aise pour voter, parce que,
selon la procédure en vigueur, leur nom et le classement exprimé seraient
publics.
Un certain nombre de participants à la discussion pensaient que nous
obtiendrions un résultat des élections qui refléterait plus fidèlement la
volonté des développeurs si nous ne publiions pas les noms associés à chacun
des votes à la feuille de scrutin.
Plusieurs personnes pensent que les votes de classement sans les noms des
développeurs apporteraient quand même des informations publiques précieuses.</p>

<p>Cette proposition s'appliquerait à toutes les élections comme les élections
du chef de projet.
En même temps, cela assouplit l'exigence pour le secrétaire de conduire un vote
par courriel. Si l'obligation de vote par courriel n'existe plus, une <a
href='https://lists.debian.org/YhoTRIxtz3AIpO+g@roeckx.be'>expérience</a> est
prévue au moins avec le système de vote Belenios.
Belenios peut fournir un meilleur secret de vote et un système de vote basé
sur le web plus facile que notre approche actuelle par courriel. Si cette
proposition passe, adopter une telle alternative devrait nécessiter un
soutien suffisant dans le projet, mais ne devrait pas avoir besoin d'un
autre amendement de la constitution.</p>

<p>Cette proposition accroît notre dépendance au pouvoir existant du
secrétaire de décider comment les scrutins sont menés. L'absence de
mécanisme pour outrepasser les décisions du secrétaire sur comment nous
menons nos votes n'a pas été un problème jusqu'à présent. Néanmoins, si
nous allons dépendre de ce pouvoir pour réfléchir à des questions comme de
savoir si le projet a atteint un consensus pour adopter un mécanisme de
vote alternatif, nous avons besoin d'un mécanisme de contestation. Aussi,
cette proposition introduit un tel mécanisme.</p>

<h4>Résumé des modifications</h4>

<p>1) L'identité de l'électeur émettant un vote particulier n'est pas rendu
      public.</p>
<p>2) Le vote par courriel n'est pas obligatoire.</p>
<p>3) Clarification du fait que les développeurs peuvent remplacer le secrétaire
      à tout moment.</p>
<p>4) Prévoir une procédure pour outrepasser la décision du secrétaire du projet
      ou de son délégué. Outrepasser la décision qu'une majorité qualifiée est
      requise ou outrepasser la décision du résultat du scrutin requiert
      une majorité à 3 contre 1. La présidence du comité technique décide qui
      conduit ce type de vote.</p>
<p>6) inscrire dans le code que notre système d'élection doit permettre une
      vérification du résultat du scrutin et doit permettre aux développeurs
      de confirmer que leur bulletin est inclus dans la liste des bulletins.</p>

<h4>Résolution générale</h4>

<p>Les développeurs prennent la résolution de procéder aux modifications de la
constitution Debian incluses dans le commit Git
ed88a1e3c1fc367ee89620a73047d84a797c9a1d.
À partir du 23 février 2022, ce commit peut être consulté à l'adresse
[<a href='https://salsa.debian.org/hartmans/webwml/-/commit/ed88a1e3c1fc367ee89620a73047d84a797c9a1d'>https://salsa.debian.org/hartmans/webwml/-/commit/ed88a1e3c1fc367ee89620a73047d84a797c9a1d</a>
</p>

<p>Pour des raisons de commodité, une liste des modifications est incluse
ci-dessous. Si la liste des modifications diffère du commit, c'est le
commit qui fait autorité. [Note du traducteur : les numéros de ligne du
diff sont ceux du texte original en anglais.]</p>

<pre>
@@ -179,9 +179,27 @@ ne peuvent pas tous outrepasser les décisions de tous ceux qui sont cités
ensuite.&lt;/cite&gt;&lt;/p&gt;
  &lt;/li&gt;

  &lt;li&gt;
    &lt;p&gt;Désigner un nouveau secrétaire [-en cas de désaccord entre-].
    {+Dans le cas normal ( &sect;7.2) où+} le chef de projet et le secrétaire
    [-auquel cela incombe-] {+sont d'accord sur le nouveau secrétaire, ce
    pouvoir des développeurs n'est pas utilisé+.&lt;/p&gt;+}
  &lt;/li&gt;
  {+&lt;li&gt;+}
{+    &lt;p&gt;Outrepasser une décision du secrétaire du projet ou de son+}
{+    délégué.&lt;/p&gt;+}

{+    &lt;p&gt;Outrepasser la décision sur quelle majorité qualifiée est+}
{+    requise pour une option de vote particulière ou outrepasser la décision+}
{+    sur le résultat d'une élection requiert un accord des développeurs+}
{+    par une majorité à trois contre un. La décision sur la majorité requise+}
{+    pour outrepasser une décision du secrétaire n'est pas sujette à+}
{+    annulation.&lt;/p&gt;+}

{+    &lt;p&gt;Le président du comité technique décide qui agit en tant que+}
{+    secrétaire pour une résolution générale pour outrepasser une décision+}
{+    du secrétaire du projet ou de son délégué. Si la décision n'est pas+}
{+    prise par le président du comité technique, le président du comité peut+}
{+    agir lui-même comme secrétaire. La décision de qui agit comme secrétaire+}
{+    pour ce type de résolution générale n'est pas sujette à annulation.&lt;/p&gt;+}
&lt;/ol&gt;

&lt;h3&gt;4.2. Procédure&lt;/h3&gt;
@@ -228,9 +246,10 @@ ne peuvent pas tous outrepasser les décisions de tous ceux qui sont cités
ensuite.&lt;/cite&gt;&lt;/p&gt;
    &lt;p&gt;
    Les bulletins sont reçus par le secrétaire du projet. Les bulletins, les
    contrôles et les résultats ne doivent pas être révélés pendant la durée du
    scrutin&nbsp;; après le scrutin, le secrétaire du projet liste tous les
    contenus des bulletins {+avec suffisamment de détail pour que chacun+}
    {+puisse vérifier le résultat de l'élection à partir de la liste des+}
    {+bulletins. L'identité d'un développeur émettant un vote particulier+}
    {+n'est pas rendue publique, mais les développeurs reçoivent une option+}
    {+pour confirmer que leur bulletin est inclus dans la liste des+}
    {+bulletins.+}
    La période de scrutin est de 2&nbsp;semaines, mais elle peut être
    modifiée d'au plus une semaine par le responsable du projet.
    &lt;/p&gt;
  &lt;/li&gt;

@@ -247,7 +266,7 @@ ne peuvent pas tous outrepasser les décisions de tous ceux qui sont cités
ensuite.&lt;/cite&gt;&lt;/p&gt;
  &lt;/li&gt;

  &lt;li&gt;
    &lt;p&gt;Les bulletins sont envoyés [-par courriel-] d'une façon qui
    convient au secrétaire. Le secrétaire détermine pour chaque scrutin si
    les votants peuvent changer leurs bulletins.&lt;/p&gt;
  &lt;/li&gt;
@@ -371,8 +390,7 @@ ne peuvent pas tous outrepasser les décisions de tous ceux qui sont cités
ensuite.&lt;/cite&gt;&lt;/p&gt;
  autant de fois que nécessaire.&lt;/li&gt;

  &lt;li&gt;Les deux semaines suivantes sont la période de scrutin pendant
  laquelle les développeurs peuvent envoyer leurs bulletins. [-Les-]
  [-bulletins de l'élection du responsable sont tenus secrets, même après que-]
  [-l'élection est finie.&lt;/li&gt;-]{+&lt;/li&gt;+}

  &lt;li&gt;Les choix possibles sur les bulletins sont les candidats qui se sont
  désignés et qui ne se sont pas encore retirés, plus le choix « None Of ... »
</pre>

    <vproposerb />
    <p>Judit Foglszinger [<email urbec@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2022/02/msg00108.html'>texte de la  proposition</a>]
    </p>
    <vsecondsb />
    <ol>
        <li>Scott Kitterman [<email kitterman@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/02/msg00110.html'>message</a>]</li>
        <li>Felix Lechner [<email lechner@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/02/msg00116.html'>message</a>]</li>
        <li>Louis-Philippe Véronneau [<email pollo@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/02/msg00123.html'>message</a>]</li>
        <li>Mathias Behrle [<email mbehrle@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/02/msg00124.html'>message</a>]</li>
        <li>Tiago Bortoletto Vaz [<email tiago@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/02/msg00125.html'>message</a>]</li>
    </ol>
    <vtextb />
<h3>Cacher l'identité des développeurs votant à un scrutin particulier et permettre la vérification</h3>
<h4>Raison</h4>

<p>Donner la possibilité de voter pour le secret du vote sans nécessiter de
voter en plus pour des modifications de la constitution sans rapport ou
avec seulement un rapport éloigné ; par exemple pour la modification du
mode de scrutin de vote par courriel à quelque chose d'indéfini.</p>

<p>Comme il a été mentionné durant la discussion, il pourrait ne pas y
avoir de consensus sur quelles options sont directement en rapport -
Cette option concerne le besoin de permettre une vérification (6)
comme directement lié au secret du vote, parce qu'autrement, les votes
deviendraient complètement invérifiables.</p>

<h4>Résumé des modifications</h4>

<p>1) L'identité de l'électeur émettant un vote particulier n'est pas
   rendue publique.</p>
<p>6) Inscrire dans le code que notre système d'élection doit permettre une
   vérification du résultat du scrutin et doit permettre aux développeurs
   de confirmer que leur bulletin est inclus dans la liste des bulletins.</p>


<pre>
&lt;h3&gt;4.2. Procédure&lt;/h3&gt;
@@ -228,9 +246,10 @@ ne peuvent pas tous outrepasser les décisions de tous ceux qui sont cités
ensuite.&lt;/cite&gt;&lt;/p&gt;
    &lt;p&gt;
    Les bulletins sont reçus par le secrétaire du projet. Les bulletins, les
    contrôles et les résultats ne doivent pas être révélés pendant la durée du
    scrutin&nbsp;; après le scrutin, le secrétaire du projet liste tous les
    contenus des bulletins {+avec suffisamment de détail pour que chacun+}
    {+puisse vérifier le résultat de l'élection à partir de la liste des+}
    {+bulletins. L'identité d'un développeur émettant un vote particulier+}
    {+n'est pas rendu publique, mais les développeurs reçoivent une option+}
    {+pour confirmer que leur bulletin est inclus dans la liste des+}
    {+bulletins.
@@ -371,8 +390,7 @@ ne peuvent pas tous outrepasser les décisions de tous ceux qui sont cités
ensuite.&lt;/cite&gt;&lt;/p&gt;
  autant de fois que nécessaire.&lt;/li&gt;

  &lt;li&gt;Les deux semaines suivantes sont la période de scrutin pendant
  laquelle les développeurs peuvent envoyer leurs bulletins. [-Les-]
  [-bulletins de l'élection du responsable sont tenus secrets, même après que-]
  [-l'élection est finie.&lt;/li&gt;-]{+&lt;/li&gt;+}
</pre>

    <vproposerc />
    <p>Holger Levsen [<email holger@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2022/03/msg00021.html'>texte de la proposition</a>]
    </p>
    <vsecondsc />
    <ol>
        <li>Mattia Rizzolo [<email mattia@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/03/msg00022.html'>message</a>]</li>
        <li>Philip Hands [<email philh@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/03/msg00030.html'>message</a>]</li>
        <li>Mathias Behrle [<email mbehrle@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/03/msg00031.html'>message</a>]</li>
        <li>Santiago Ruano Rincón [<email santiago@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/03/msg00033.html'>message</a>]</li>
        <li>Gunnar Wolf [<email gwolf@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/03/msg00037.html'>message</a>]</li>
        <li>Sven Bartscher [<email kritzefitz@debian.org>] [<a href='https://lists.debian.org/debian-vote/2022/03/msg00058.html'>message</a>]</li>
    </ol>
    <vtextc />
<h3>Réaffirmer les scrutins publics</h3>

<p>Dans la mesure où nous pouvons tenir à la fois des scrutins secrets et
sans transparence et des scrutins ouverts et transparents, le projet prend
la résolution de conserver notre système de vote tel qu'il est.</p>

<h4>Raison</h4>

<p>La proposition de résolution générale sur le secret du vote est muette
sur les détails de sa mise en œuvre, probablement parce qu'en fait, un vote
à la fois secret et transparent est impossible à réaliser parfaitement,
aussi cette résolution générale est vouée au même sort que le vote de la
résolution générale « rendre publique la liste de diffusiondebian-private »
qui a été adoptée puis n'a jamais été mise en œuvre.</p>

<p>Un système de vote qui ne serait transparent que pour certains est non
démocratique et mènerait à ce que seulement peu de personnes soient dans le
secret, ce qui est à l'opposé des objectifs d'ouverture et de transparence
de Debian.</p>

<p>Et puis, en ce début de 2022, ce n'est pas le moment d'apporter des
changements aussi précipités, c'est aussi pourquoi je souhaite que soit
mentionné sur le bulletin « conserver le statu quo » et pas seulement comme
« NOTA » (None of the above), mais comme une véritable option.</p>


    <vquorum />
     <p>
        Avec la liste actuelle des <a href="vote_001_quorum.log">développeurs
         ayant voté</a>, nous avons :
     </p>
    <pre>
#include 'vote_001_quorum.txt'
    </pre>
#include 'vote_001_quorum.src'


    <vstatistics />
    <p>
	Pour cette résolution générale, comme d'habitude,
#       <a href="https://vote.debian.org/~secretary/gr_vote_secrecy/">des statistiques</a>
             des <a href="suppl_001_stats">statistiques</a>
             sur les bulletins et les accusés de réception sont rassemblées
             périodiquement durant la période du scrutin.
             De plus, la liste des <a href="vote_001_voters.txt">votants</a>
             sera enregistrée. La <a href="vote_001_tally.txt">feuille
             de compte</a> pourra être aussi consultée.
         </p>

    <vmajorityreq />
    <p>
      Les propositions 1 et 2 ont besoin d’une majorité qualifiée à 3 contre 1
    </p>
#include 'vote_001_majority.src'

    <voutcome />
#include 'vote_001_results.src'

    <hrline />
      <address>
        <a href="mailto:secretary@debian.org">Secrétaire du projet Debian</a>
      </address>
