#use wml::debian::template title="LTS Säkerhetsinformation" GEN_TIME="yes"
#use wml::debian::toc
#use wml::debian::recent_list_security
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="51b1f2756d728aa0beb3ba7e5f67a15ab3b8db98"

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

<toc-display/>

<toc-add-entry name="keeping-secure">Hålla ditt Debian LTS-system säkert</toc-add-entry>

<p>För att få de senaste Debian LTS-säkerhetsbulletinerna, prenumerera på
sändlistan
<a href="https://lists.debian.org/debian-lts-announce/">\
debian-lts-announce</a>.</p>

<p>För ytterligare information om säkerhetsfrågor i Debian, se
the <a href="../../security">Debians säkerhetsinformation</a>.</p>

<a class="rss_logo" href="dla">RSS</a>
<toc-add-entry name="DLAS">De senaste säkerhetsbulletinerna</toc-add-entry>

<p> Dessa webbsidor innehåller ett förkortat arkiv över de säkerhetsbulletiner
som postats på sändlistan <a 
href="https://lists.debian.org/debian-lts-announce/">\
debian-lts-announce</a> list.

<p>
<:= get_recent_security_list( '1m', '6', '.', '$(ENGLISHDIR)/lts/security' ) :>
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian LTS Security Advisories (endast titlar)" href="dla">
<link rel="alternate" type="application/rss+xml"
 title="Debian LTS Security Advisories (sammanfattningar)" href="dla-long">
:#rss#}
<p>De senaste Debian LTS säkerhetsbulletinerna finns även tillgängliga i
<a href="dla">RDF format</a>. Vi erbbjuder också en
<a href="dla-long">andra fil</a> som innehåller första stycket i den 
aktuella bulletinen så att det går att se vad det handlar om.</p>

#include "$(ENGLISHDIR)/lts/security/index.include"
<p>Även äldre säkerhetsbulletiner finns tillgängliga:
<:= get_past_sec_list(); :>

<p>Debiandistributioner är inte sårbara för alla säkerhetsproblem. 
<a href="https://security-tracker.debian.org/">Debians säkerhetsspårare</a> 
samlar all information om sårbarheters status i Debianpaket, och sökas
med hjälp av CVE eller paketnamn.</p>


<toc-add-entry name="contact">Kontaktinformation</toc-add-entry>

<p>Vänligen läs <a href="https://wiki.debian.org/LTS/FAQ">Debian LTS FAQ</a> innan ni
kontaktar oss. Era frågor kan redan vara besvarade där.</p>

<p>Även <a href="https://wiki.debian.org/LTS/FAQ">kontaktinformation kan hittas i FAQ</a>.</p>
