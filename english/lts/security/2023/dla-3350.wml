<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>node-css-what was vulnerable to Regular Expression Denial of Service
(ReDoS) due to the usage of insecure regular expression in the
re_attr variable.
The exploitation of this vulnerability could be triggered
via the parse function.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2.1.0-1+deb10u1.</p>

<p>We recommend that you upgrade your node-css-what packages.</p>

<p>For the detailed security status of node-css-what please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/node-css-what">https://security-tracker.debian.org/tracker/node-css-what</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3350.data"
# $Id: $
