<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities have been discovered in Wireshark, a
network traffic analyzer. An attacker could cause a denial of service
(infinite loop or application crash) via packet injection or a crafted
capture file.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4345">CVE-2022-4345</a>

    <p>Infinite loops in the BPv6, OpenFlow, and Kafka protocol dissectors in
    Wireshark 4.0.0 to 4.0.1 and 3.6.0 to 3.6.9 allows denial of service via
    packet injection or crafted capture file</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0411">CVE-2023-0411</a>

    <p>Excessive loops in multiple dissectors in Wireshark 4.0.0 to 4.0.2 and
    3.6.0 to 3.6.10 and allows denial of service via packet injection or
    crafted capture file</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0412">CVE-2023-0412</a>

    <p>TIPC dissector crash in Wireshark 4.0.0 to 4.0.2 and 3.6.0 to 3.6.10 and
    allows denial of service via packet injection or crafted capture file</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0413">CVE-2023-0413</a>

    <p>Dissection engine bug in Wireshark 4.0.0 to 4.0.2 and 3.6.0 to 3.6.10
    and allows denial of service via packet injection or crafted capture
    file</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0415">CVE-2023-0415</a>

    <p>iSCSI dissector crash in Wireshark 4.0.0 to 4.0.2 and 3.6.0 to 3.6.10
    and allows denial of service via packet injection or crafted capture
    file</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0417">CVE-2023-0417</a>

    <p>Memory leak in the NFS dissector in Wireshark 4.0.0 to 4.0.2 and 3.6.0
    to 3.6.10 and allows denial of service via packet injection or crafted
    capture file</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2.6.20-0+deb10u5.</p>

<p>We recommend that you upgrade your wireshark packages.</p>

<p>For the detailed security status of wireshark please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/wireshark">https://security-tracker.debian.org/tracker/wireshark</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3313.data"
# $Id: $
