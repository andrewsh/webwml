<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities were discovered in Werkzeug, a collection of
utilities for WSGI (web) applications. An attacker may inject cookies
in specific situations, and cause a denial of service (DoS).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-23934">CVE-2023-23934</a>

    <p>Werkzeug will parse the cookie `=__Host-test=bad` as
    __Host-test=bad`. If a Werkzeug application is running next to a
    vulnerable or malicious subdomain which sets such a cookie using a
    vulnerable browser, the Werkzeug application will see the bad
    cookie value but the valid cookie key. Browsers may allow
    <q>nameless</q> cookies that look like `=value` instead of
    `key=value`. A vulnerable browser may allow a compromised
    application on an adjacent subdomain to exploit this to set a
    cookie like `=__Host-test=bad` for another subdomain.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-25577">CVE-2023-25577</a>

    <p>Werkzeug's multipart form data parser will parse an unlimited
    number of parts, including file parts. Parts can be a small amount
    of bytes, but each requires CPU time to parse and may use more
    memory as Python data. If a request can be made to an endpoint
    that accesses `request.data`, `request.form`, `request.files`, or
    `request.get_data(parse_form_data=False)`, it can cause
    unexpectedly high resource usage. This allows an attacker to cause
    a denial of service by sending crafted multipart data to an
    endpoint that will parse it.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
0.14.1+dfsg1-4+deb10u2.</p>

<p>We recommend that you upgrade your python-werkzeug packages.</p>

<p>For the detailed security status of python-werkzeug please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python-werkzeug">https://security-tracker.debian.org/tracker/python-werkzeug</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3346.data"
# $Id: $
