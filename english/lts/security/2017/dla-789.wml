<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Brief introduction</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5208">CVE-2017-5208</a>

    <p>Choongwoo Han reported[0] an exploitable crash in wrestool from
    icoutils. The command line tools is e.g. used in KDE's
    metadataparsing.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5331">CVE-2017-5331</a>

    <p>It turned out that the correction for <a href="https://security-tracker.debian.org/tracker/CVE-2017-5208">CVE-2017-5208</a> was not enough
    so an additional correction was needed.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5332">CVE-2017-5332</a>

    <p>But as I see it there are still combinations of the arguments which
    make the test succeed even though the the memory block identified by
    offset size is not fully inside memory total_size.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5333">CVE-2017-5333</a>

    <p>The memory check was not stringent enough on 64 bit systems.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.29.1-5deb7u1.</p>

<p>We recommend that you upgrade your icoutils packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-789.data"
# $Id: $
