<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>tiff3 is affected by multiple issues that can result at least in denial of
services of applications using libtiff4. Crafted TIFF files can be
provided to trigger: abort() calls via failing assertions, buffer overruns
(both in read and write mode).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8781">CVE-2015-8781</a>

    <p>tif_luv.c in libtiff allows attackers to cause a denial of service
    (out-of-bounds write) via an invalid number of samples per pixel in a
    LogL compressed TIFF image.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8782">CVE-2015-8782</a>

    <p>tif_luv.c in libtiff allows attackers to cause a denial of service
    (out-of-bounds writes) via a crafted TIFF image.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8783">CVE-2015-8783</a>

    <p>tif_luv.c in libtiff allows attackers to cause a denial of service
    (out-of-bounds reads) via a crafted TIFF image.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-8784">CVE-2015-8784</a>

    <p>The NeXTDecode function in tif_next.c in LibTIFF allows remote
    attackers to cause a denial of service (out-of-bounds write) via a
    crafted TIFF image.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9533">CVE-2016-9533</a>

    <p>tif_pixarlog.c in libtiff 4.0.6 has out-of-bounds write
    vulnerabilities in heap allocated buffers.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9534">CVE-2016-9534</a>

    <p>tif_write.c in libtiff 4.0.6 has an issue in the error code path of
    TIFFFlushData1() that didn't reset the tif_rawcc and tif_rawcp
    members. </p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9535">CVE-2016-9535</a>

    <p>tif_predict.h and tif_predict.c in libtiff 4.0.6 have assertions
    that can lead to assertion failures in debug mode, or buffer
    overflows in release mode, when dealing with unusual tile size
    like YCbCr with subsampling.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.9.6-11+deb7u4.</p>

<p>We recommend that you upgrade your tiff3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-880.data"
# $Id: $
