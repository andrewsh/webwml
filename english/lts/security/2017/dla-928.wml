<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were found in libsndfile, a popular library
for reading/writing audio files.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7585">CVE-2017-7585</a>

    <p>In libsndfile before 1.0.28, an error in the "flac_buffer_copy()"
    function (flac.c) can be exploited to cause a stack-based buffer
    overflow via a specially crafted FLAC file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7586">CVE-2017-7586</a>

    <p>In libsndfile before 1.0.28, an error in the "header_read()"
    function (common.c) when handling ID3 tags can be exploited to
    cause a stack-based buffer overflow via a specially crafted FLAC
    file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7741">CVE-2017-7741</a>

    <p>In libsndfile before 1.0.28, an error in the "flac_buffer_copy()"
    function (flac.c) can be exploited to cause a segmentation
    violation (with write memory access) via a specially crafted FLAC
    file during a resample attempt, a similar issue to 
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-7585">CVE-2017-7585</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7742">CVE-2017-7742</a>

    <p>In libsndfile before 1.0.28, an error in the "flac_buffer_copy()"
    function (flac.c) can be exploited to cause a segmentation
    violation (with read memory access) via a specially crafted FLAC
    file during a resample attempt, a similar issue to
    <a href="https://security-tracker.debian.org/tracker/CVE-2017-7585">CVE-2017-7585</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-9496">CVE-2014-9496</a>

    <p>The sd2_parse_rsrc_fork function in sd2.c in libsndfile allows
    attackers to have unspecified impact via vectors related to a (1)
    map offset or (2) rsrc marker, which triggers an out-of-bounds
    read.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-9756">CVE-2014-9756</a>

    <p>The psf_fwrite function in file_io.c in libsndfile allows
    attackers to cause a denial of service (divide-by-zero error and
    application crash) via unspecified vectors related to the
    headindex variable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-7805">CVE-2015-7805</a>

    <p>Heap-based buffer overflow in libsndfile 1.0.25 allows remote
    attackers to have unspecified impact via the headindex value in
    the header in an AIFF file.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.0.25-9.1+deb7u1.</p>

<p>We recommend that you upgrade your libsndfile packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-928.data"
# $Id: $
