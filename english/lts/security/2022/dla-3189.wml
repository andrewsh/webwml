<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several bugs were discovered in PostgreSQL, a relational database server
system. This new LTS minor version update fixes over 25 bugs that were
reported in the last several months. The complete and detailed list of
issues could be found at: <a href="https://www.postgresql.org/docs/release/11.18.">https://www.postgresql.org/docs/release/11.18.</a></p>

<p>For Debian 10 buster, this problem has been fixed in version
11.18-0+deb10u1.</p>

<p>We recommend that you upgrade your postgresql-11 packages.</p>

<p>For the detailed security status of postgresql-11 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/postgresql-11">https://security-tracker.debian.org/tracker/postgresql-11</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3189.data"
# $Id: $
