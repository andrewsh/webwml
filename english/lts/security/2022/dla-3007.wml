<define-tag description>LTS security update</define-tag>
<define-tag moreinfo></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3596">CVE-2021-3596</a>

    <p>A NULL pointer dereference flaw was found in
    ImageMagick in versions prior to 7.0.10-31 in ReadSVGImage() in
    coders/svg.c. This issue is due to not checking the return value from
    libxml2's xmlCreatePushParserCtxt() and uses the value directly, which
    leads to a crash and segmentation fault.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28463">CVE-2022-28463</a>

    <p>ImageMagick is vulnerable to Buffer Overflow.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
8:6.9.7.4+dfsg-11+deb9u14.</p>

<p>We recommend that you upgrade your imagemagick packages.</p>

<p>For the detailed security status of imagemagick please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/imagemagick">https://security-tracker.debian.org/tracker/imagemagick</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3007.data"
# $Id: $
