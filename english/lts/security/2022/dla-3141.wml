<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities were discovered in Wordpress, a popular
content management framework. Server Side Request Forgery and cross-site
scripting (XSS) attacks may facilitate the bypass of access controls or the
injection of client-side scripts.</p>

<p>For Debian 10 buster, this problem has been fixed in version
5.0.17+dfsg1-0+deb10u1.</p>

<p>We recommend that you upgrade your wordpress packages.</p>

<p>For the detailed security status of wordpress please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/wordpress">https://security-tracker.debian.org/tracker/wordpress</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3141.data"
# $Id: $
