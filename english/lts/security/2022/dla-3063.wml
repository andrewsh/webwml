<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A heap use-after-free vulnerability was found in systemd, a system and
service manager, where asynchronous Polkit queries are performed while
handling dbus messages. A local unprivileged attacker can abuse this
flaw to crash systemd services or potentially execute code and elevate
their privileges, by sending specially crafted dbus messages.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
232-25+deb9u14.</p>

<p>We recommend that you upgrade your systemd packages.</p>

<p>For the detailed security status of systemd please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/systemd">https://security-tracker.debian.org/tracker/systemd</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3063.data"
# $Id: $
