<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update fixes bzdiff when using it with two compressed files. It also
includes a fix to support large files on 32 bit systems.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.0.6-9.2~deb10u2.</p>

<p>We recommend that you upgrade your bzip2 packages.</p>

<p>For the detailed security status of bzip2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/bzip2">https://security-tracker.debian.org/tracker/bzip2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3112.data"
# $Id: $
