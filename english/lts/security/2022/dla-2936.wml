<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were found in libgit2, a low-level Git library,
and are as follows:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8098">CVE-2018-8098</a>

    <p>Integer overflow in the index.c:read_entry() function while
    decompressing a compressed prefix length in libgit2 before
    v0.26.2 allows an attacker to cause a denial of service
    (out-of-bounds read) via a crafted repository index file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-8099">CVE-2018-8099</a>

    <p>Incorrect returning of an error code in the index.c:read_entry()
    function leads to a double free in libgit2 before v0.26.2, which
    allows an attacker to cause a denial of service via a crafted
    repository index file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10887">CVE-2018-10887</a>

    <p>It has been discovered that an unexpected sign extension in
    git_delta_apply function in delta-apply.c file may lead to an
    integer overflow which in turn leads to an out of bound read,
    allowing to read before the base object. An attacker may use
    this flaw to leak memory addresses or cause a Denial of Service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10888">CVE-2018-10888</a>

    <p>A missing check in git_delta_apply function in delta-apply.c file,
    may lead to an out-of-bound read while reading a binary delta file.
    An attacker may use this flaw to cause a Denial of Service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15501">CVE-2018-15501</a>

    <p>In ng_pkt in transports/smart_pkt.c in libgit2, a remote attacker
    can send a crafted smart-protocol <q>ng</q> packet that lacks a '\0'
    byte to trigger an out-of-bounds read that leads to DoS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12278">CVE-2020-12278</a>

    <p>path.c mishandles equivalent filenames that exist because of NTFS
    Alternate Data Streams. This may allow remote code execution when
    cloning a repository. This issue is similar to <a href="https://security-tracker.debian.org/tracker/CVE-2019-1352">CVE-2019-1352</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12279">CVE-2020-12279</a>

    <p>checkout.c mishandles equivalent filenames that exist because of
    NTFS short names. This may allow remote code execution when cloning
    a repository. This issue is similar to <a href="https://security-tracker.debian.org/tracker/CVE-2019-1353">CVE-2019-1353</a>.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
0.25.1+really0.24.6-1+deb9u1.</p>

<p>We recommend that you upgrade your libgit2 packages.</p>

<p>For the detailed security status of libgit2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libgit2">https://security-tracker.debian.org/tracker/libgit2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2936.data"
# $Id: $
