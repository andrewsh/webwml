<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two vulnerabilities were discovered in gerbv, a Gerber file viewer. Most
Printed Circuit Board (PCB) design programs can export data to a Gerber
file.</p>

<ul>
  <li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40401">CVE-2021-40401</a>:
  A use-after-free vulnerability existed in the RS-274X aperture definition
  tokenization functionality. A specially-crafted gerber file could have led to
  code execution.</li>
 
  <li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40403">CVE-2021-40403</a>:
  An information disclosure vulnerability existed in the pick-and-place
  rotation parsing functionality. A specially-crafted pick-and-place file could
  have exploited the missing initialization of a structure in order to leak
  memory contents.</li>
</ul>

<p>For Debian 10 <q>Buster</q>, these problems have been fixed in version
2.7.0-1+deb10u2.</p>

<p>We recommend that you upgrade your gerbv packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3210.data"
# $Id: $
