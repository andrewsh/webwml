<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that wkhtmltopdf, a command line utility to render HTML
files into PDF, allowed local filesystem access by default. This update
disables local filesystem access, but it can be enabled if necessary
with the --enable-local-file-access or the --allow <path> options.</p>

<p>For Debian 10 buster, this problem has been fixed in version
0.12.5-1+deb10u1.</p>

<p>We recommend that you upgrade your wkhtmltopdf packages.</p>

<p>For the detailed security status of wkhtmltopdf please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/wkhtmltopdf">https://security-tracker.debian.org/tracker/wkhtmltopdf</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3158.data"
# $Id: $
