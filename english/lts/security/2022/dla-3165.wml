<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>In src:expat, an XML parsing C library, there is a use-after free
caused by overeager destruction of a shared DTD in
XML_ExternalEntityParserCreate in out-of-memory situations.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2.2.6-2+deb10u6.</p>

<p>We recommend that you upgrade your expat packages.</p>

<p>For the detailed security status of expat please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/expat">https://security-tracker.debian.org/tracker/expat</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3165.data"
# $Id: $
