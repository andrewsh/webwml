<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that twisted, a framework for internet applications
written in Python, was prone to an HTML injection when displaying the
HTTP Host header in an error page.</p>

<p>For Debian 10 buster, this problem has been fixed in version
18.9.0-3+deb10u2.</p>

<p>We recommend that you upgrade your twisted packages.</p>

<p>For the detailed security status of twisted please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/twisted">https://security-tracker.debian.org/tracker/twisted</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3212.data"
# $Id: $
