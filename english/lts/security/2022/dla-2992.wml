<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several issues were discovered in OpenVPN, a Virtual Private Network server
and client, that could lead to authentication bypass when using deferred
auth plugins.</p>

<p>Note that this upload disables support for multiple deferred auth plugins,
following the upstream fix for <a href="https://security-tracker.debian.org/tracker/CVE-2022-0547">CVE-2022-0547</a>.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
2.4.0-6+deb9u4.</p>

<p>We recommend that you upgrade your openvpn packages.</p>

<p>For the detailed security status of openvpn please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openvpn">https://security-tracker.debian.org/tracker/openvpn</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2992.data"
# $Id: $
