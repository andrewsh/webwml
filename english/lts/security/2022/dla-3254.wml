<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A flaw was found in the way the exubertant-ctags source code parser handled
the "-o" command-line option which specifies the tag filename. A crafted tag
filename specified in the command line or in the configuration file could have
resulted in arbitrary command execution because the externalSortTags() in
sort.c calls the system(3) function in an unsafe way.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4515">CVE-2022-4515</a>

    <p>A flaw was found in Exuberant Ctags in the way it handles the "-o"
    option. This option specifies the tag filename. A crafted tag filename
    specified in the command line or in the configuration file results in
    arbitrary command execution because the externalSortTags() in sort.c calls
    the system(3) function in an unsafe way.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
1:5.9~svn20110310-12+deb10u1.</p>

<p>We recommend that you upgrade your exuberant-ctags packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3254.data"
# $Id: $
