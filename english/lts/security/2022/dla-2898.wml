<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that nss, the Mozilla Network Security Service library, was
vulnerable to a NULL pointer dereference when parsing empty PKCS 7
sequences, which could result in denial of service.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2:3.26.2-1.1+deb9u5.</p>

<p>We recommend that you upgrade your nss packages.</p>

<p>For the detailed security status of nss please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/nss">https://security-tracker.debian.org/tracker/nss</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2898.data"
# $Id: $
