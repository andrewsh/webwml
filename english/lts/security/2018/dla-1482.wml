<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several issues were discovered in libx11, the client interface to the
X Windows System. The functions XGetFontPath, XListExtensions, and
XListFonts are vulnerable to an off-by-one override on malicious
server responses. A malicious server could also send a reply in which
the first string overflows, causing a variable set to NULL that will
be freed later on, leading to a segmentation fault and Denial of
Service. The function XListExtensions in ListExt.c interprets a
variable as signed instead of unsigned, resulting in an out-of-bounds
write (of up to 128 bytes), leading to a Denial of Service or possibly
remote code execution.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2:1.6.2-3+deb8u2.</p>

<p>We recommend that you upgrade your libx11 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1482.data"
# $Id: $
