<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-1239">CVE-2015-1239</a>

      <p>Fix for denial of service (process crash) via a crafted PDF.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5139">CVE-2016-5139</a>

      <p>Fix for integer overflows, allowing a denial of service
      (heap-based buffer overflow) or possibly have unspecified
      other impact via crafted JPEG 2000 data.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.1.0-2+deb8u4.</p>

<p>We recommend that you upgrade your openjpeg2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1433.data"
# $Id: $
