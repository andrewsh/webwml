<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An integer overflow vulnerability was discovered in libidn, the GNU library for
Internationalized Domain Names (IDNs), in its Punycode handling (a Unicode
characters to ASCII encoding) allowing a remote attacker to cause a denial of
service against applications using the library.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.29-1+deb8u3.</p>

<p>We recommend that you upgrade your libidn packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1447.data"
# $Id: $
