<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Tim Brown discovered a shared memory permissions vulnerability in the
Mesa 3D graphics library.  Some Mesa X11 drivers use shared-memory
XImages to implement back buffers for improved performance, but Mesa
creates shared memory regions with permission mode 0777.  An attacker
can access the shared memory without any specific permissions.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
10.3.2-1+deb8u2.</p>

<p>We recommend that you upgrade your mesa packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1993.data"
# $Id: $
