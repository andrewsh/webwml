<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Vulnerabilities have been identified in libspring-java, a modular
Java/J2EE application framework.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-3578">CVE-2014-3578</a>

    <p>A directory traversal vulnerability that allows remote attackers to
    read arbitrary files via a crafted URL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-3625">CVE-2014-3625</a>

    <p>A directory traversal vulnerability that allows remote attackers to
    read arbitrary files via unspecified vectors, related to static
    resource handling.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-3192">CVE-2015-3192</a>

    <p>Improper processing of inline DTD declarations when DTD is not
    entirely disabled, which allows remote attackers to cause a denial
    of service (memory consumption and out-of-memory errors) via a
    crafted XML file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2015-5211">CVE-2015-5211</a>

    <p>Reflected File Download (RFD) attack vulnerability, which allows a
    malicious user to craft a URL with a batch script extension that
    results in the response being downloaded rather than rendered and
    also includes some input reflected in the response.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9878">CVE-2016-9878</a>

    <p>Improper path sanitization in ResourceServlet, which allows
    directory traversal attacks.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.0.6.RELEASE-17+deb8u1.</p>

<p>We recommend that you upgrade your libspring-java packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1853.data"
# $Id: $
