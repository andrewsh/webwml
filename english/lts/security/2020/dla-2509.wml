<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update includes the changes in tzdata 2020e. Notable
changes are:</p>

<p>- Volgograd switched to Moscow time on 2020-12-27 at 02:00.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2020e-0+deb9u1.</p>

<p>We recommend that you upgrade your tzdata packages.</p>

<p>For the detailed security status of tzdata please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/tzdata">https://security-tracker.debian.org/tracker/tzdata</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2509.data"
# $Id: $
