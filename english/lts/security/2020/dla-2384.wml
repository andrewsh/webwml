<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two issues have been found in yaws, a high performance HTTP 1.1 webserver
written in Erlang.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24379">CVE-2020-24379</a>

      <p>Reject external resource requests in DAV in order to avoid
      XML External Entity (XXE) attackes.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24916">CVE-2020-24916</a>

      <p>Sanitize CGI executable in order to avoid command injection
      via CGI requests.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
2.0.4+dfsg-1+deb9u1.</p>

<p>We recommend that you upgrade your yaws packages.</p>

<p>For the detailed security status of yaws please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/yaws">https://security-tracker.debian.org/tracker/yaws</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2384.data"
# $Id: $
