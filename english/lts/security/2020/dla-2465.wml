<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a filename sanitisation issue in
<tt>php-pear</tt>, a distribution system for reusable PHP components.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28948">CVE-2020-28948</a>

    <p>Archive_Tar through 1.4.10 allows an unserialization attack because
    phar: is blocked but PHAR: is not blocked.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28949">CVE-2020-28949</a>

    <p>Archive_Tar through 1.4.10 has :// filename sanitization only to address
    phar attacks, and thus any other stream-wrapper attack (such as file:// to
    overwrite files) can still succeed.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
1:1.10.1+submodules+notgz-9+deb9u2.</p>

<p>We recommend that you upgrade your php-pear packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2465.data"
# $Id: $
