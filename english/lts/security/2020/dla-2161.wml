<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two security issues have been detected in tika.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1950">CVE-2020-1950</a>

    <p>carefully crafted or corrupt PSD file can cause excessive memory
    usage in Apache.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1951">CVE-2020-1951</a>

    <p>Infinite Loop (DoS) vulnerability in Apache Tika's PSDParser.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.5-1+deb8u1.</p>

<p>We recommend that you upgrade your tika packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2161.data"
# $Id: $
