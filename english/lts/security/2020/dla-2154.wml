<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The following packages CVE(s) were reported against phpmyadmin.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10802">CVE-2020-10802</a>

    <p>In phpMyAdmin 4.x before 4.9.5, a SQL injection vulnerability
    has been discovered where certain parameters are not properly
    escaped when generating certain queries for search actions in
    libraries/classes/Controllers/Table/TableSearchController.php.
    An attacker can generate a crafted database or table name. The
    attack can be performed if a user attempts certain search
    operations on the malicious database or table.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10803">CVE-2020-10803</a>

    <p>In phpMyAdmin 4.x before 4.9.5, a SQL injection vulnerability
    was discovered where malicious code could be used to trigger
    an XSS attack through retrieving and displaying results (in
    tbl_get_field.php and libraries/classes/Display/Results.php).
    The attacker must be able to insert crafted data into certain
    database tables, which when retrieved (for instance, through the
    Browse tab) can trigger the XSS attack.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
4:4.2.12-2+deb8u9.</p>

<p>We recommend that you upgrade your phpmyadmin packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2154.data"
# $Id: $
