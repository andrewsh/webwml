<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The following security issues have been found in qemu, which could
potentially result in DoS and execution of arbitrary code.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1711">CVE-2020-1711</a>

    <p>An out-of-bounds heap buffer access flaw was found in the way the iSCSI
    Block driver in QEMU handled a response coming from an iSCSI server
    while checking the status of a Logical Address Block (LBA) in an
    iscsi_co_block_status() routine. A remote user could use this flaw to
    crash the QEMU process, resulting in a denial of service or potential
    execution of arbitrary code with privileges of the QEMU process on the
    host.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13253">CVE-2020-13253</a>

    <p>An out-of-bounds read access issue was found in the SD Memory Card
    emulator of the QEMU. It occurs while performing block write commands
    via sdhci_write(), if a guest user has sent <q>address</q> which is OOB of
    's->wp_groups'. A guest user/process may use this flaw to crash the
    QEMU process resulting in DoS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14364">CVE-2020-14364</a>

    <p>An out-of-bounds read/write access issue was found in the USB emulator
    of the QEMU. It occurs while processing USB packets from a guest, when
    'USBDevice->setup_len' exceeds the USBDevice->data_buf[4096], in
    do_token_{in,out} routines.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16092">CVE-2020-16092</a>

    <p>An assertion failure can occur in the network packet processing. This
    issue affects the e1000e and vmxnet3 network devices. A malicious guest
    user/process could use this flaw to abort the QEMU process on the host,
    resulting in a denial of service condition in
    net_tx_pkt_add_raw_fragment in hw/net/net_tx_pkt.c</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1:2.8+dfsg-6+deb9u11.</p>

<p>We recommend that you upgrade your qemu packages.</p>

<p>For the detailed security status of qemu please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/qemu">https://security-tracker.debian.org/tracker/qemu</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2373.data"
# $Id: $
