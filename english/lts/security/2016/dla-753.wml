<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Paul Szabo discovered a potential privilege escalation that could be
exploited in the situation envisaged in DLA-622-1. This update also
addresses several regressions stemming from incomplete fixes for
<a href="https://security-tracker.debian.org/tracker/CVE-2015-5345">CVE-2015-5345</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2016-5018">CVE-2016-5018</a> and <a href="https://security-tracker.debian.org/tracker/CVE-2016-6797">CVE-2016-6797</a>.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
7.0.28-4+deb7u8.</p>

<p>We recommend that you upgrade your tomcat7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-753.data"
# $Id: $
