<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A security issue has been discovered in the Squid chaching proxy, on its
2.7.STABLE9 version branch.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4554">CVE-2016-4554</a>

    <p>Jianjun Chen found that Squid was vulnerable to a header smuggling
    attack that could lead to cache poisoning and to bypass of same-origin
    security policy in Squid and some client browsers.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, this issue has been fixed in version
2.7.STABLE9-4.1+deb7u2.</p>

<p>We recommend that you upgrade your squid packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-558.data"
# $Id: $
