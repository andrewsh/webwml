<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Tobias Stoeckmann from the OpenBSD project discovered the following
vulnerability in libXi, the X11 input extension library:</p>

   <p>Insufficient validation of data from the X server
   can cause out of boundary memory access or
   endless loops (Denial of Service).</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2:1.6.1-1+deb7u2.</p>

<p>We recommend that you upgrade your libxi packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-685.data"
# $Id: $
