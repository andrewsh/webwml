<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Jonathan Schlue discovered a vulnerability in Salt, a powerful remote execution
manager. A user who has control of the source, and source_hash URLs can gain
full file system access as root on a salt minion.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2016.11.2+ds-1+deb9u8.</p>

<p>We recommend that you upgrade your salt packages.</p>

<p>For the detailed security status of salt please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/salt">https://security-tracker.debian.org/tracker/salt</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2823.data"
# $Id: $
