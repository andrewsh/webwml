<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in mediawiki, a wiki
website engine for collaborative work.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20270">CVE-2021-20270</a>

    <p>An infinite loop in SMLLexer in Pygments used by mediawiki as
    one if its lexers may lead to denial of service when performing
    syntax highlighting of a Standard ML (SML) source file, as
    demonstrated by input that only contains the <q>exception</q> keyword.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27291">CVE-2021-27291</a>

    <p>pygments, the lexers used by mediawiki rely heavily on regular
    expressions. Some of the regular expressions have exponential or
    cubic worst-case complexity and are vulnerable to ReDoS. By
    crafting malicious input, an attacker can cause a denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30152">CVE-2021-30152</a>

    <p>An issue was discovered in MediaWiki. When using the MediaWiki
    API to <q>protect</q> a page, a user is currently able to protect to a
    higher level than they currently have permissions for.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30155">CVE-2021-30155</a>

    <p>An issue was discovered in MediaWiki before. ContentModelChange
    does not check if a user has correct permissions to create and set
    the content model of a nonexistent page.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30158">CVE-2021-30158</a>

    <p>An issue was discovered in MediaWiki. Blocked users are unable to
    use Special:ResetTokens. This has security relevance because a
    blocked user might have accidentally shared a token, or might know
    that a token has been compromised, and yet is not able to block
    any potential future use of the token by an unauthorized party.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30159">CVE-2021-30159</a>

    <p>An issue was discovered in MediaWiki. Users can bypass intended
    restrictions on deleting pages in certain <q>fast double move</q>
    situations. MovePage::isValidMoveTarget() uses FOR UPDATE, but
    it's only called if Title::getArticleID() returns non-zero with no
    special flags. Next, MovePage::moveToInternal() will delete the
    page if getArticleID(READ_LATEST) is non-zero. Therefore, if the
    page is missing in the replica DB, isValidMove() will return true,
    and then moveToInternal() will unconditionally delete the page if
    it can be found in the master.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1:1.27.7-1~deb9u8.</p>

<p>We recommend that you upgrade your mediawiki packages.</p>

<p>For the detailed security status of mediawiki please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mediawiki">https://security-tracker.debian.org/tracker/mediawiki</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2648.data"
# $Id: $
