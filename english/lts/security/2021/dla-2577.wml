<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several issues have been found in python-pysaml2, a pure python
implementation of SAML Version 2 Standard.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000433">CVE-2017-1000433</a>

     <p>pysaml2 accept any password when run with python optimizations
     enabled. This allows attackers to log in as any user without
     knowing their password.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21239">CVE-2021-21239</a>

     <p>pysaml2 has an improper verification of cryptographic signature
     vulnerability. Users of pysaml2 that use the default
     CryptoBackendXmlSec1 backend and need to verify signed SAML
     documents are impacted. PySAML2 does not ensure that a signed
     SAML document is correctly signed. The default
     CryptoBackendXmlSec1 backend is using the xmlsec1 binary to
     verify the signature of signed SAML documents, but by default
     xmlsec1 accepts any type of key found within the given document.
     xmlsec1 needs to be configured explicitly to only use only _x509
     certificates_ for the verification process of the SAML document signature.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
3.0.0-5+deb9u2.</p>

<p>We recommend that you upgrade your python-pysaml2 packages.</p>

<p>For the detailed security status of python-pysaml2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python-pysaml2">https://security-tracker.debian.org/tracker/python-pysaml2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2577.data"
# $Id: $
