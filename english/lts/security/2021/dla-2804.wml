<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerability have been fixed in libsdl2, the older version of
the Simple DirectMedia Layer library that provides low level access to
audio, keyboard, mouse, joystick, and graphics hardware.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7572">CVE-2019-7572</a>

    <p>Buffer over-read in IMA_ADPCM_nibble in audio/SDL_wave.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7573">CVE-2019-7573</a>

    <p>Heap-based buffer over-read in InitMS_ADPCM in audio/SDL_wave.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7574">CVE-2019-7574</a>

    <p>Heap-based buffer over-read in IMA_ADPCM_decode in audio/SDL_wave.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7575">CVE-2019-7575</a>

    <p>Heap-based buffer overflow in MS_ADPCM_decode in audio/SDL_wave.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7576">CVE-2019-7576</a>

    <p>Heap-based buffer over-read in InitMS_ADPCM in audio/SDL_wave.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7577">CVE-2019-7577</a>

    <p>Buffer over-read in SDL_LoadWAV_RW in audio/SDL_wave.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7578">CVE-2019-7578</a>

    <p>Heap-based buffer over-read in InitIMA_ADPCM in audio/SDL_wave.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7635">CVE-2019-7635</a>

    <p>Heap-based buffer over-read in Blit1to4 in video/SDL_blit_1.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7636">CVE-2019-7636</a>

    <p>Heap-based buffer over-read in SDL_GetRGB in video/SDL_pixels.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7637">CVE-2019-7637</a>

    <p>Heap-based buffer overflow in SDL_FillRect in video/SDL_surface.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7638">CVE-2019-7638</a>

    <p>Heap-based buffer over-read in Map1toN in video/SDL_pixels.c</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13616">CVE-2019-13616</a>

    <p>Heap-based buffer over-read in BlitNtoN in video/SDL_blit_N.c</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1.2.15+dfsg1-4+deb9u1.</p>

<p>We recommend that you upgrade your libsdl1.2 packages.</p>

<p>For the detailed security status of libsdl1.2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libsdl1.2">https://security-tracker.debian.org/tracker/libsdl1.2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2804.data"
# $Id: $
