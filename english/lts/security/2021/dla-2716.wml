<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in pillow (Python Imaging
Library - PIL).</p>

<p>Affected binary packages:</p>

    <p>python-imaging
    python-pil-dbg
    python-pil-doc
    python-pil.imagetk-dbg
    python-pil.imagetk
    python-pil
    python3-pil-dbg
    python3-pil.imagetk-dbg
    python3-pil.imagetk
    python3-pil</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35653">CVE-2020-35653</a>

    <p>Pillow through 8.2.0 and PIL (aka Python Imaging Library) through
    1.1.7 allow an attacker to pass controlled parameters directly into
    a convert function to trigger a buffer overflow in Convert.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-25290">CVE-2021-25290</a>

    <p>An issue was discovered in Pillow before 8.1.1. In TiffDecode.c,
    there is a negative-offset memcpy with an invalid size.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28676">CVE-2021-28676</a>

    <p>An issue was discovered in Pillow before 8.2.0. For FLI data,
    FliDecode did not properly check that the block advance was
    non-zero, potentially leading to an infinite loop on load.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28677">CVE-2021-28677</a>

    <p>An issue was discovered in Pillow before 8.2.0. For EPS data, the
    readline implementation used in EPSImageFile has to deal with any
    combination of \r and \n as line endings. It used an accidentally
    quadratic method of accumulating lines while looking for a line
    ending. A malicious EPS file could use this to perform a DoS of
    Pillow in the open phase, before an image was accepted for opening.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-34552">CVE-2021-34552</a>

    <p>Pillow through 8.2.0 and PIL (aka Python Imaging Library) through
    1.1.7 allow an attacker to pass controlled parameters directly into
    a convert function to trigger a buffer overflow in Convert.c.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
4.0.0-4+deb9u3.</p>

<p>We recommend that you upgrade your pillow packages.</p>

<p>For the detailed security status of pillow please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/pillow">https://security-tracker.debian.org/tracker/pillow</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2716.data"
# $Id: $
