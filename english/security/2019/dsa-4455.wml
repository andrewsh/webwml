<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in Heimdal, an implementation of
Kerberos 5 that aims to be compatible with MIT Kerberos.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16860">CVE-2018-16860</a>

    <p>Isaac Boukris and Andrew Bartlett discovered that Heimdal was
    susceptible to man-in-the-middle attacks caused by incomplete
    checksum validation. Details on the issue can be found in the Samba
    advisory at <a href="https://www.samba.org/samba/security/CVE-2018-16860.html">\
    https://www.samba.org/samba/security/CVE-2018-16860.html</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12098">CVE-2019-12098</a>

    <p>It was discovered that failure of verification of the PA-PKINIT-KX key
    exchange client-side could permit to perform man-in-the-middle attack.</p></li>

</ul>

<p>For the stable distribution (stretch), these problems have been fixed in
version 7.1.0+dfsg-13+deb9u3.</p>

<p>We recommend that you upgrade your heimdal packages.</p>

<p>For the detailed security status of heimdal please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/heimdal">\
https://security-tracker.debian.org/tracker/heimdal</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4455.data"
# $Id: $
