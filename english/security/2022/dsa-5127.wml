<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4197">CVE-2021-4197</a>

    <p>Eric Biederman reported that incorrect permission checks in the
    cgroup process migration implementation can allow a local attacker
    to escalate privileges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0168">CVE-2022-0168</a>

    <p>A NULL pointer dereference flaw was found in the CIFS client
    implementation which can allow a local attacker with CAP_SYS_ADMIN
    privileges to crash the system. The security impact is negligible as
    CAP_SYS_ADMIN inherently gives the ability to deny service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1016">CVE-2022-1016</a>

    <p>David Bouman discovered a flaw in the netfilter subsystem where the
    nft_do_chain function did not initialize register data that
    nf_tables expressions can read from and write to. A local attacker
    can take advantage of this to read sensitive information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1048">CVE-2022-1048</a>

    <p>Hu Jiahui discovered a race condition in the sound subsystem that
    can result in a use-after-free. A local user permitted to access a
    PCM sound device can take advantage of this flaw to crash the
    system or potentially for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1158">CVE-2022-1158</a>

    <p>Qiuhao Li, Gaoning Pan, and Yongkang Jia discovered a bug in the
    KVM implementation for x86 processors. A local user with access to
    /dev/kvm could cause the MMU emulator to update page table entry
    flags at the wrong address. They could exploit this to cause a
    denial of service (memory corruption or crash) or possibly for
    privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1195">CVE-2022-1195</a>

    <p>Lin Ma discovered race conditions in the 6pack and mkiss hamradio
    drivers, which could lead to a use-after-free. A local user could
    exploit these to cause a denial of service (memory corruption or
    crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1198">CVE-2022-1198</a>

    <p>Duoming Zhou discovered a race condition in the 6pack hamradio
    driver, which could lead to a use-after-free. A local user could
    exploit this to cause a denial of service (memory corruption or
    crash) or possibly for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1199">CVE-2022-1199</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-1204">CVE-2022-1204</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-1205">CVE-2022-1205</a>

    <p>Duoming Zhou discovered race conditions in the AX.25 hamradio
    protocol, which could lead to a use-after-free or null pointer
    dereference. A local user could exploit this to cause a denial of
    service (memory corruption or crash) or possibly for privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1353">CVE-2022-1353</a>

    <p>The TCS Robot tool found an information leak in the PF_KEY
    subsystem. A local user can receive a netlink message when an
    IPsec daemon registers with the kernel, and this could include
    sensitive information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1516">CVE-2022-1516</a>

    <p>A NULL pointer dereference flaw in the implementation of the X.25
    set of standardized network protocols, which can result in denial
    of service.</p>

    <p>This driver is not enabled in Debian's official kernel
    configurations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26490">CVE-2022-26490</a>

    <p>Buffer overflows in the STMicroelectronics ST21NFCA core driver can
    result in denial of service or privilege escalation.</p>

    <p>This driver is not enabled in Debian's official kernel
    configurations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-27666">CVE-2022-27666</a>

    <p><q>valis</q> reported a possible buffer overflow in the IPsec ESP
    transformation code. A local user can take advantage of this flaw to
    cause a denial of service or for privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28356">CVE-2022-28356</a>

    <p>Beraphin discovered that the ANSI/IEEE 802.2 LLC type 2 driver did
    not properly perform reference counting on some error paths. A
    local attacker can take advantage of this flaw to cause a denial
    of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28388">CVE-2022-28388</a>

    <p>A double free vulnerability was discovered in the 8 devices USB2CAN
    interface driver.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28389">CVE-2022-28389</a>

    <p>A double free vulnerability was discovered in the Microchip CAN BUS
    Analyzer interface driver.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28390">CVE-2022-28390</a>

    <p>A double free vulnerability was discovered in the EMS CPC-USB/ARM7
    CAN/USB interface driver.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29582">CVE-2022-29582</a>

    <p>Jayden Rivers and David Bouman discovered a user-after-free
    vulnerability in the io_uring subystem due to a race condition in
    io_uring timeouts. A local unprivileged user can take advantage of
    this flaw for privilege escalation.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 5.10.113-1.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5127.data"
# $Id: $
