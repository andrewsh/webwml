#use wml::debian::template title="Огледала на Дебиан по света" MAINPAGE=true
#use wml::debian::translation-check translation="b554c22abdbc4a6253951c9bf9610405d0c4f2cd"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#per-country">Огледала на Дебиан по държави</a></li>
  <li><a href="#complete-list">Пълен списък на огледалните сървъри</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Дебиан се
<a href="https://www.debian.org/mirror/">разпространява</a> от стотици сървъри
в Интернет. Ако планирате да <a href="../download">изтеглите</a> Дебиан,
първо опитайте с по-близък сървър. Това вероятно ще помогне за по-бързото
изтегляне и ще намали натоварването на централните ни сървъри.<p>
</aside>

<p class="centerblock">
  Дебиан има огледални сървъри в много държави и за някои от
  тях сме осигурили лесно запомнящо се име от вида
  <code>ftp.&lt;страна&gt;.debian.org</code>. Обикновено това са огледала,
  които се синхронизират редовно и бързо и поддържат налични всички архитектури,
  поддържани от Дебиан. Архивът с пакети е достъпен чрез HTTP в директория
  <code>/debian</code> на сървъра.
</p>

<p class="centerblock">
  Другите огладални сървъри може да имат ограничения и да не включват всички
  архитектури, например заради недостатъчно дисково пространство.
  Фактът, че даден сървър не е с име от вида
  <code>ftp.&lt;страна&gt;.debian.org</code> не означава непременно, че
  по-бавен или по-малко актуален от основните сървъри. Един близък (и
  следователно по-бърз) сървър често е за предпочитане пред по-далечените.
</p>

<p>Използвайте най-близкият до вас сървър, независимо дали използва име от рода на
<code>ftp.&lt;страна&gt;.debian.org</code> или не. Програмата <a
href="https://packages.debian.org/stable/net/netselect"><code>netselect</code></a>
може да помогне за откриване на сървъра с най-малко забавяне. Използвайте
програма за изтегляне като <a
href="https://packages.debian.org/stable/web/wget"><code>wget</code></a> или <a
href="https://packages.debian.org/stable/net/rsync"><code>rsync</code></a> за
определяне на сървъра с най-бърз достъп. Обърнете внимание, че географската
близост не винаги е определяща кой е най-подходящия сървър.</p>

<p>
Ако системата ви е мобилна, най-удобно може да е да използвате
<abbr title="Content Delivery Network - Мрежа за доставка на съдържание">CDN</abbr>.
За тази цел Дебиан поддържа
<code>deb.debian.org</code>. Погледнете
<a href="https://deb.debian.org/">страницата на проекта</a> за повече информация
как да добавите адреса към файла <code>sources.list</code>.

<h2 class="center">Огледални сървъри на Дебиан по страни</h2>

<table border="0" class="center">
<tr>
  <th>Страна</th>
  <th>Адрес</th>
  <th>Архитектури</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-primary.inc"
</table>

<h2 class="center">Пълен списък на огледалните сървъри на Дебиан</h2>

<table border="0" class="center">
<tr>
  <th>Име на хост</th>
  <th>HTTP</th>
  <th>Архитектури</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-secondary.inc"
</table>

#include "$(ENGLISHDIR)/mirror/list-footer.inc"
