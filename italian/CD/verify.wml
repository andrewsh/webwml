#use wml::debian::cdimage title="Verificare l'autenticità delle iimmagini Debian" BARETITLE=true
#use wml::debian::translation-check translation="e96ee42901d20a8878ded2c462204cd1b7a7210f" maintainer="Luca Monducci"

<p>
I rilasci ufficiali delle immagini Debian dell'installatore e live sono
accompagnati da file di controllo firmati;
sono disponibili nelle stesse directory delle immagini <code>iso-cd</code>,
<code>jigdo-dvd</code>, <code>iso-hybrid</code>, ecc.
Questi file permettono di controllare se le immagini scaricate sono corrette.
Innanzi tutto, il codice di controllo può essere utilizzato per verificare
che i CD non si siano danneggiati durante il download.
In secondo luogo, le firme dei file di controllo permettono di confermare
che i file sono quelli ufficialmente creati e rilasciati da Debian e che
non sono stati manomessi.
</p>

<p>
Per convalidare il contenuto di un'immagine occorre utilizzare lo
strumento di checksum appropriato.
Le checksum calcolate con algoritmi crittograficamente forti (SHA256 e
SHA512) sono disponibili per tutti i rilasci;
per utilizzarle si deve impiegare lo strumenti di verifica
<code>sha256sum</code> o <code>sha512sum</code> corrispondente.
</p>

<p>
Utilizzare GnuPG per assicurarsi che i file di controllo siano a loro
volta corretti, verificandoli con il file di firma che li accompagna
(es. <code>SHA512SUMS.sign</code>).
Le chiavi utilizzate per queste firme sono tutte nel <a
href="https://keyring.debian.org">portachiavi GPG Debian</a> e il modo
migliore per verificarle è impiegare questo portachiavi per convalidarle
tramite la rete di fiducia.
Al fine di rendere la vita più semplice a coloro che ancora non hanno
un pronto accesso a una macchina con Debian, questi sono i dettagli
delle chiavi utilizzate per firmare i rilasci degli anni recenti e i
collegamenti diretti alle rispettive chiavi pubbliche:
</p>

#include "$(ENGLISHDIR)/CD/CD-keys.data"
