#use wml::debian::template title="Instalar Debian a través de Internet" BARETITLE=true
#use wml::debian::translation-check translation="f917b9adf4a1c15cca8405e010043d380e4b1b83"
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"

<p>Este tipo de instalación requiere un acceso a Internet
<em>durante</em> la instalación. Comparado con otros métodos en éste se
descargan menos datos ya que el proceso se adapta a lo que necesita. 
Desafortunadamente, <em>no</em> permite usar tarjetas RDSI internas.</p>
<p>Hay tres opciones para la instalación sobre red:</p>

<toc-display />
<div class="line">
<div class="item col50">

<toc-add-entry name="smallcd">Imagen pequeña para CD o memoria USB</toc-add-entry>

<p>A continuación dispone de imágenes de CD.
Escoja la arquitectura de su procesador debajo.</p>

<stable-netinst-images />
</div>
<div class="clear"></div>
</div>

<p>Para más detalles, véase <a href="../CD/netinst/">Instalación por red
desde un CD mínimo</a></p>

<div class="line">
<div class="item col50">

<toc-add-entry name="verysmall">CD pequeños, memorias USB flexibles, etc.</toc-add-entry>

<p>Puede descargar un par de archivos de imagen de pequeño tamaño, adecuado para
memorias USB y dispositivos similares. Grábelos en los dispositivos
e inicie luego la instalación desde ellos.</p>

<p>Existen diferencias en el soporte para instalar desde imágenes pequeñas
entre las diferentes arquitecturas.
</p>

<p>Si desea más detalles, consulte el
<a href="$(HOME)/releases/stable/installmanual">manual de instalación para su
arquitectura</a>, especialmente el capítulo <q>Obtener los medios de
instalación del sistema</q>.</p>

<p>
Aquí están los enlaces a ellas (véase el
archivo MANIFEST para más información):
</p>

<stable-verysmall-images />
</div>
<div class="item col50 lastcol">

<toc-add-entry name="netboot">Arranque por red</toc-add-entry>

<p>Configure un servidor TFTP y uno DHCP (o BOOTP, o RARP), para disponer de
los medios necesarios para la instalación en las máquinas de su red local.
Si la BIOS de la máquina cliente lo permite, podrá arrancar el sistema de
instalación Debian por red (usando PXE y TFTP), y continuar el resto de la
instalación por red.</p>

<p>No todas las máquinas permiten arrancar por red. Debido a la necesidad de
trabajo adicional, este método no es recomendable para usuarios inexpertos.</p>

<p>Si desea información más detallada, consulte el
<a href="$(HOME)/releases/stable/installmanual">manual de instalación para su
arquitectura</a>, en concreto, el capítulo <q>Preparar los archivos para
arrancar por red con TFTP</q>.</p>
<p>Aquí están los enlaces a las imágenes (véase el archivo MANIFEST para más
información):</p>

<stable-netboot-images />
</div>
</div>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
# 
<div id="firmware_nonfree" class="important">
<p>
Si algún componente hardware de su sistema <strong>requiere cargar «firmware»
no libre</strong> con el controlador de dispositivo, puede usar uno de los
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">\
archivos comprimidos con paquetes de «firmware» común</a> o descargar una imagen <strong>no oficial</strong>
que incluya estos «firmwares» <strong>no libres</strong>. En la <a href="../releases/stable/amd64/ch06s04">guía de instalación</a> puede encontrar
instrucciones sobre cómo usar los archivos comprimidos e información general sobre cómo cargar
el «firmware» durante la instalación.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current/">imágenes
no oficiales de instalación de <q>estable</q> con «firmware» incluido</a>
</p>
</div>
