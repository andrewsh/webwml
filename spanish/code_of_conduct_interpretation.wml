#use wml::debian::template title="Interpretación del código de conducta de Debian" BARETITLE=true
#use wml::debian::translation-check translation="8ec27f98071e8313b1a32572c961a2417fc0f36f"

{#meta#:
<meta name="keywords" content="code of conduct, coc">
:#meta#}

<h2>Algunos comentarios generales</h2>

<p>El propósito de este documento es proporcionar algunas explicaciones y
ejemplos sobre cómo se interpreta el <a href="$(HOME)/code_of_conduct">
código de conducta</a> (CdC) dentro del proyecto Debian. Si
tiene alguna pregunta, póngase en contacto con el equipo de comunidad
(community@debian.org). Póngase también en contacto con el equipo de
comunidad si le preocupa que algo que está pensando hacer pudiera
violar el CdC.</p>

<p>El propósito del CdC es la creación de un espacio comunitario en el que las personas
se sientan cómodas. Esto nos ayuda a mantener un colectivo de contribuidores y contribuidoras
que participan en Debian con estusiasmo y que coadyuvan a cumplir nuestros objetivos
de crear y mantener Debian. La comunidad Debian es, al
mismo tiempo, un grupo de amigos y amigas colaborando en un proyecto y un grupo de
colegas haciendo su trabajo. Debian es tanto una enorme agrupación social
como un proyecto técnico.</p>

<p>El objetivo del CdC, y de los esfuerzos por hacer que se cumpla, es ayudar a que las personas
se alineen con los valores compartidos que, mediante el proceso de resoluciones generales,
ha adoptado Debian como comunidad. Para participar en Debian tiene que
cumplir y respetar el CdC. No tiene que ser perfecto
&mdash;todo el mundo comete errores o tiene un mal día&mdash;, el objetivo de hacer
cumplir el CdC es ayudar a las personas a hacerlo mejor. Esto es todo lo
que le pedimos: haga lo posible por tratar a sus amigos, amigas y colegas con
consideración. El CdC se aplica a todas sus actividades dentro de Debian y a aquellas
que lleva a cabo en representación del proyecto Debian. También
pueden derivarse acciones disciplinarias, tales como la expulsión (temporal o permanente) o
la pérdida de estatus, si sus actividades externas a Debian tienen impacto en el proyecto o
crean un espacio inseguro o dañino en Debian.</p>

<p>Este es un <strong>documento vivo</strong>. Cambiará a lo largo del
tiempo según vaya evolucionando lo que se considere normal e ideal tanto dentro como
fuera de Debian. El código de conducta de Debian se ratificó en
2014. Desde entonces no ha cambiado, aunque las expectativas y las buenas
prácticas en las comunidades de software libre y de código abierto, y en
la tecnología en general, lo han hecho.</p>

<h2>1. Sea respetuoso</h2>

<p><strong>En un proyecto del tamaño de Debian, inevitablemente habrá
personas con las cuales esté en desacuerdo o le resulte difícil
cooperar. Acéptelo, pero, aun así, siga siendo respetuoso. El desacuerdo
con las acciones y opiniones de alguien no es excusa para un comportamiento pobre o
para ataques personales. Una comunidad en la que la gente se sienta amenazada no es una
comunidad sana.</strong></p>

<p>Todos los miembros de la comunidad y, en términos más amplios, todo el mundo merece
respeto. En Debian el respeto no es algo que se gane, es
algo que merecen todos y cada uno de los miembros de la comunidad, independientemente
de su edad, género, talla, educación, origen étnico o cualquier otro
factor.</p>

<p>Debian es una comunidad abierta. Todas las culturas y creencias son bienvenidas
y reconocidas siempre y cuando no perjudiquen a otros. Sus propias
expectativas o su propio entorno cultural no son excusa para
violar el CdC o para ser irrespetuoso hacia otra persona dentro de
la comunidad Debian o desde su rol como miembro de la comunidad
Debian. Debian tiene su propia cultura. Cuando trabaje con contribuidores
o usuarios de Debian, guíese por las normas de Debian y represente
positivamente a Debian.</p>

<p>En Debian, las personas provienen de diferentes entornos culturales, tienen
diferentes experiencias, y podrían no sentirse cómodas o no hablar con fluidez el
idioma usado en una discusión determinada. Es importante
asumir las mejores intenciones (ver más adelante) y ser comprensivo con
las diferencias en estilos comunicativos. Sin embargo, esto no significa que
sea aceptable comunicarse de manera inapropiada intencionadamente o no
cambiar su estilo comunicativo con el fin de cumplir las normas de la comunidad una vez que ha
salido el tema.</p>

<h3>Ejemplos de comportamiento irrespetuoso</h3>

<p>Esta es una lista <strong>incompleta</strong> de ejemplos
de comportamiento irrespetuoso:</p>

<ul>
  <li>Expresar groserías dirigidas a una persona o a su trabajo.</li>
  <li>Insultar a alguien por su edad, discapacidad, género, sexualidad,
      talla, religión, nacionalidad, raza, origen étnico, casta, tribu,
      educación, tipos de contribuciones o estatus en Debian o
      en relación con el software libre y de código abierto; utilizar una de estas características como un
      insulto o hacer declaraciones despectivas sobre un grupo.</li>
  <li>Usar intencionadamente los pronombres o nombres incorrectos
      (por ejemplo, hacer <q>deadnaming</q>, o «uso del nombre pasado») de un individuo.</li>
  <li>Ponerse en contacto agresiva o repetidamente con alguien tras habernos pedido
      que dejemos de hacerlo.</li>
  <li>No hacer esfuerzos de buena fe para llegar a acuerdos o
      para cambiar comportamientos contrarios a los valores de Debian.</li>
</ul>

<h2>2. Asuma buena fe</h2>

<p><strong>Los contribuidores de Debian tienen muchas maneras de alcanzar nuestro objetivo común de un
sistema operativo libre. Las maneras de otros de hacer algo pueden
diferir de las suyas propias. Asuma que las otras personas están trabajando
colaborativamente hacia este objetivo. Tenga en cuenta que muchos de nuestros contribuidores
no son hablantes nativos de inglés o pueden tener distintos rasgos
culturales.</strong></p>

<p><strong>Debian es un proyecto global.</strong> Debian cuenta con personas
de diferentes entornos, experiencias, estilos de comunicación
y normas culturales. Como tal, es especialmente importante asumir
buena fe. Esto significa asumir, dentro de lo razonable, que la persona
con la que está hablando no está intentando herirle o insultarle.</p>

<p>Para asumir buena fe, también nosotros debemos actuar de buena fe. Esto
significa también que debería asumir que el otro está intentando actuar de la mejor manera y
que no debería herirle o insultarle. Molestar intencionadamente
a alguien dentro de Debian no es aceptable.</p>

<p>Asumir buena fe incluye comunicación, comportamiento y
contribución. Esto significa asumir que todos los que contribuyen, sean cuales sean
sus contribuciones, están esforzándose de acuerdo a sus capacidades y
con integridad.</p>

<h3>Ejemplo de comportamientos que no son de buena fe</h3>

<p>Como antes, la siguiente es una lista <strong>incompleta</strong>:</p>

<ul>
  <li>Trolear.</li>
  <li>Asumir que alquien está troleando.</li>
  <li>Usar frases como <q>Sé que no eres estúpido</q> o <q>No
      podrías haber hecho esto intencionadamente, así que debes de ser
      estúpido</q>.</li>
  <li>Insultar las contribuciones de alguien.</li>
  <li>Suscitar rivalidad en otros (<q>tocar su fibra 
      sensible</q>) para producir un efecto.</li>
</ul>

<h2>3. Sea colaborador</h2>

<p><strong>Debian es un proyecto grande y complejo; siempre hay algo
que aprender dentro de Debian. Es bueno pedir ayuda si la necesita.
Igualmente, las propuestas de ayuda deben verse en el contexto de nuestro
objetivo común de mejorar Debian.</strong></p>

<p><strong>Cuando haga algo para beneficio del proyecto, esté
dispuesto a explicar a otros cómo funciona, así pueden construir en base a
su trabajo, e incluso mejorarlo.</strong></p>

<p>Nuestras contribuciones ayudan a otros contribuidores, al proyecto y a nuestros
usuarios y usuarias. Trabajamos al descubierto bajo el ethos de que todo el que quiera
contribuir debería poder hacerlo, dentro de lo razonable. Todos en Debian tenemos
diferentes entorno y habilidades. Esto significa que usted debería ser
positivo y constructivo y que, siempre que sea posible, debería proporcionar
asistencia, consejo u orientación. Valoramos el consenso, aunque hay
ocasiones en las que se toman decisiones democráticas o en las que el rumbo puede
decidirse por las personas que están dispuestas y capacitadas para emprender una
actividad.</p>

<p>Los distintos equipos utilizan herramientas diferentes y tienen normas de colaboración
diferentes. Ejemplos de esto podrían ser cosas como reuniones de seguimiento
semanales, notas compartidas o procesos de revisión de código. Que las cosas
se hayan venido haciendo de una determinada manera no significa que esa sea la única o la mejor forma de
hacerlas, y los equipos deberían estar abiertos a debatir nuevos métodos
de colaboración.</p>

<p>También es parte de ser colaborador la asunción de buena fe (ver
más arriba) de que los demás también son colaboradores, en lugar de
asumir que los demás están <q>en su contra</q> o ingorándole.</p>

<p>La buena colaboración es más valiosa que las habilidades técnicas. Ser un
buen contribuidor desde el punto de vista técnico no hace que sea aceptable ser un miembro dañino
de la comunidad.</p>

<h3>Ejemplos de mala colaboración</h3>

<ul>
  <li>Rehusar adoptar las normas de contribución de un equipo.</li>
  <li>Insultar a otros contribuidores o colegas.</li>
  <li>Rehusar trabajar con otros salvo que suponga una amenaza para su propia
    seguridad o bienestar.</li>
</ul>

<h2>4. Intente expresarse concisamente</h2>

<p><strong>Tenga presente que lo que escribe una vez lo leerán
cientos de personas. Escribir un correo electrónico corto significa que la gente pueda entender
la conversación de la manera más eficiente posible. Cuando se necesite una
explicación larga, considere añadir un resumen.</strong></p>

<p><strong>Intente proporcionar argumentos nuevos a una conversación, para que cada
correo agregue algo único al hilo, teniendo presente que el
resto del hilo sigue conteniendo los otros mensajes con los
argumentos que ya se han dado.</strong></p>

<p><strong>Intente mantenerse dentro del tema, especialmente en discusiones que ya
son bastante largas.</strong></p>

<p>Algunos temas no son apropiados para Debian, incluyendo algunos
temas conflictivos de naturaleza política o religiosa. Debian es tanto
un entorno de colegas como de amigos y amigas. Los intercambios
públicos colectivos deben ser respetuosos, ajustados al tema del que se trate y
profesionales. El uso de un lenguaje conciso y accesible es importante,
especialmente teniendo en cuenta que muchos contribuidores a Debian no son hablantes nativos
de inglés y que muchas de las comunicaciones del proyecto se realizan en inglés. Es
importante ser claro y explícito y, en la medida de lo posible, explicar o
evitar los modismos. (Nota: el uso de modismos no es una violación del
código de conducta. Evitarlos es simplemente una buena práctica general
por claridad).</p>

<p>No siempre es fácil expresar significado y tono mediante texto o
entre culturas. Ser de mentalidad abierta, asumir buenas intenciones e
intentarlo son lo más importante en la conversación.</p>

<h2>5. Sea abierto</h2>

<p><strong>Muchas vías de comunicación usadas en Debian permiten
comunicación pública y privada. Según el párrafo tres del contrato
social, debería usar preferentemente métodos públicos de comunicación
para mensajes relacionados con Debian, a no ser que publique información
sensible.</strong></p>

<p><strong>Esto también se aplica para mensajes de ayuda o soporte
relacionados con Debian; una petición pública de soporte no solo le
proporcionará una respuesta con más probabilidad, sino que también posibilita que
los errores inadvertidos que cometan las personas que responden a su pregunta se
detecten más fácilmente y se corrijan.</strong></p>

<p>Es importante mantener tantas comunicaciones públicas como sea
posible. Muchas listas de correo de Debian permiten que cualquiera se una a ellas o tienen
archivos accesibles públicamente. Los archivos pueden ser guardados y almacenados por
medios ajenos a Debian (por ejemplo, en el Archivo de Internet). Debería asumir
que lo dicho en una lista de correo de Debian
es <q>permanente</q>. Mucha gente conserva registros de conversaciones de IRC
también.</p>

<h3>Privacidad</h3>

<p>También las conversaciones privadas en el contexto del proyecto se
consideran sujetas al código de conducta. Animamos a informar de que
algo dicho en una conversación privada es inapropiado o inseguro (vea
ejemplos más arriba).</p>

<p>Al mismo tiempo, es importante respetar la privacidad de estas conversaciones,
y no deberían compartirse salvo por razones de seguridad. Algunos
lugares, como la lista de correo debian-private, están incluidos en esta
categoría.</p>

<h2>6. En caso de problemas</h2>

<p><strong>Aunque los participantes deberían adherirse a este código de
conducta, reconocemos que a veces las personas pueden tener un mal día,
o no estar al tanto de alguna de las directrices de este código de conducta. Cuando
eso ocurre, puede responderles y dirigirles a este código de
conducta. Esos mensajes pueden ser públicos o privados, como sea
más apropiado. Sin embargo, ya sea el mensaje público
o privado, debería seguir las partes relevantes de este código de
conducta; en particular, no debería ser abusivo o
irrespetuoso. Asuma buena fe, es más probable que los participantes
no se den cuenta de su mal comportamiento que que intenten conscientemente
degradar la calidad de la discusión.</strong></p>

<p><strong>Se inhabilitará temporal o permanentemente la comunicación
por cualquiera de los medios de Debian a los ofensores graves o
reincidentes. Las quejas deben dirigirse (en privado) a los administradores
del foro de comunicación de Debian en cuestión. Para encontrar información
de contacto sobre los administradores, vea la página de la estructura
organizativa de Debian.</strong></p>

<p>El propósito del código de conducta es proporcionar una guía
sobre cómo mantener Debian como una comunidad acogedora. Las personas
deberían sentirse bienvenidas a participar tal y como son y no como los demás
esperan que sean. El código de conducta de Debian resume lo que las personas
deberían hacer, más que las maneras de las que no se deberían comportar. Este documento
proporciona información sobre los términos en los que el equipo de comunidad interpreta el código de
conducta. Cada miembro de la comunidad Debian puede interpretar este
documento de forma diferente. Lo más importante es que la gente se sienta
cómoda, segura y bienvenida en Debian. Independientemente de si
aparece reflejado específicamente en este documento o no, si alguien no se siente
cómodo, seguro o bienvenido, debería ponerse en contacto con el <a
href="https://wiki.debian.org/Teams/Community">equipo de comunidad</a>.</p>

<p>Si alguien piensa que ha podido hacer algo
inapropiado o que lo que va a hacer puede resultar
inapropiado, le animamos también a ponerse en contacto con el equipo de
comunidad.</p>

<p>Dado el tamaño de Debian, el equipo de comunidad no puede y, de hecho, no
monitoriza de forma proactiva todas las comunicaciones.
Por eso, es importante que la comunidad Debian
colabore con el equipo de comunidad.</p>

<h2>Incumplimiento del código de conducta</h2>

<p>No se espera de nadie que sea perfecto siempre. Cometer un error no
es el fin del mundo, aunque puede dar lugar a que alguien
pida una mejora. En Debian se esperan esfuerzos
de buena fe para actuar bien y mejorar. El incumplimiento repetido del CdC
puede dar lugar a represalias o a restricciones en la interacción con la comunidad,
incluyendo, pero no limitadas a:</p>

<ul>
  <li>amonestaciones;</li>
  <li>expulsiones temporales o permanentes de listas de correo electrónico, canales IRC u
  otros medios de comunicación;</li>
  <li>supresión temporal o permanente de derechos y privilegios; o</li>
  <li>degradación temporal o permanente de estatus en el proyecto
  Debian.</li>
</ul>
