#use wml::debian::translation-check translation="a871b5fd7d3849d5e29ce5a2e7e4dd60e8408d58"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el núcleo Linux que
pueden dar lugar a elevación de privilegios, a denegación de servicio o a fugas de
información.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-2182">CVE-2019-2182</a>

    <p>Hanjun Guo y Lei Li informaron de un condición de carrera en el código
    de gestión de memoria virtual de arm64 que podría dar lugar a revelación
    de información, a denegación de servicio (caída) o, posiblemente, a elevación
    de privilegios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5108">CVE-2019-5108</a>

    <p>Mitchell Frank, de Cisco, descubrió que cuando se usaba la pila
    IEEE 802.11 (WiFi) en modo AP con itinerancia, se podría desencadenar la
    itinerancia para una estación recién asociada antes de que la estación estuviera
    autenticada. Un atacante dentro del alcance del AP podría usar esto
    para provocar denegación de servicio, bien llenando una tabla de
    conmutación, o bien redirigiendo tráfico procedente de otras estaciones.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19319">CVE-2019-19319</a>

    <p>Jungyeon descubrió que un sistema de archivos manipulado puede provocar que la
    implementación de ext4 libere o reasigne bloques del journal. Un usuario
    autorizado a montar sistemas de archivos podría usar esto para provocar denegación de
    servicio (caída) o, posiblemente, elevación de privilegios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19462">CVE-2019-19462</a>

    <p>La herramienta syzbot encontró una ausencia de comprobación de errores en la biblioteca
    <q>relay</q>, utilizada para implementar varios ficheros bajo debugfs. Un usuario
    local autorizado a acceder a debugfs podría usar esto para provocar denegación
    de servicio (caída) o, posiblemente, elevación de privilegios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19768">CVE-2019-19768</a>

    <p>Tristan Madani informó de una condición de carrera en la utilidad de depuración
    blktrace que podía dar lugar a «uso tras liberar» («use-after-free»). Un usuario local capaz
    de desencadenar la eliminación de dispositivos de bloques podía, posiblemente, usar esto para
    provocar denegación de servicio (caída) o elevación de privilegios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20806">CVE-2019-20806</a>

    <p>Se descubrió una potencial desreferencia de puntero nulo en el controlador
    multimedia tw5864. No está claro el impacto de esto en la seguridad.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20811">CVE-2019-20811</a>

    <p>La herramienta Hulk Robot encontró un fallo de conteo de referencias en una ruta de
    error en el subsistema de red. No está claro el impacto de esto en la
    seguridad.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-0543">CVE-2020-0543</a>

    <p>Investigadores de la VU Amsterdam descubrieron que en algunas CPU Intel
    con soporte de las instrucciones RDRAND y RDSEED, parte de un valor
    aleatorio generado por estas instrucciones puede ser usado en una ejecución
    especulativa posterior en cualquier núcleo de la misma CPU física.
    Dependiendo de cómo utilicen las aplicaciones estas instrucciones, un
    usuario local o un huésped VM podrían usar esto para obtener información
    sensible, como claves criptográficas de otros usuarios o de otras VM.</p>

    <p>Esta vulnerabilidad se puede mitigar mediante una actualización de microcódigo, ya sea
    como parte del firmware del sistema (BIOS) o a través del paquete
    intel-microcode de la sección non-free del archivo de Debian. Esta actualización del núcleo
    solo proporciona mecanismos para informar de la vulnerabilidad y la opción para
    inhabilitar la mitigación si esta no es necesaria.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-2732">CVE-2020-2732</a>

    <p>Paulo Bonzini descubrió que la implementación de KVM para procesadores
    Intel no realizaba correctamente la emulación de instrucciones para
    huéspedes L2 cuando estaba habilitada la virtualización anidada. Esto podía permitir que
    un huésped L2 provocara elevación de privilegios, denegación de servicio o
    fugas de información en el huésped L1.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8428">CVE-2020-8428</a>

    <p>Al Viro descubrió un potencial «uso tras liberar» («use-after-free») en el núcleo del
    sistema de archivos (vfs). Un usuario local podía explotar esto para provocar denegación de
    servicio (caída) o, posiblemente, para obtener información sensible
    del núcleo.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8647">CVE-2020-8647</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2020-8649">CVE-2020-8649</a>

    <p>La herramienta Hulk Robot encontró un potencial acceso MMIO fuera de límites en
    el controlador vgacon. Un usuario local autorizado a acceder a un terminal
    virtual (/dev/tty1, etc.) en un sistema que utilice el controlador vgacon 
    podría usar esto para provocar denegación de servicio (caída o corrupción
    de memoria) o, posiblemente, elevación de privilegios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8648">CVE-2020-8648</a>

    <p>La herramienta syzbot encontró una condición de carrera en el controlador de terminal
    virtual que podría dar lugar a un «uso tras liberar» («use-after-free»). Un usuario local
    autorizado a acceder a un terminal virtual podría usar esto para provocar
    denegación de servicio (caída o corrupción de memoria) o, posiblemente,
    elevación de privilegios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9383">CVE-2020-9383</a>

    <p>Jordy Zomer informó de una comprobación de rango incorrecta en el controlador de disquete
    que podía dar lugar a un acceso estático fuera de límites. Un usuario local
    autorizado a acceder a la disquetera podría usar esto para provocar
    denegación de servicio (caída o corrupción de memoria) o, posiblemente,
    elevación de privilegios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10711">CVE-2020-10711</a>

    <p>Matthew Sheets informó de problemas de desreferencia de punteros NULL en el
    subsistema SELinux al recibir paquetes CIPSO con categoría nula. Un
    atacante remoto puede aprovechar este defecto para provocar denegación de
    servicio (caída). Tenga en cuenta que este problema no afecta a los paquetes
    binarios distribuidos en Debian ya que en ellos CONFIG_NETLABEL no está habilitado.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10732">CVE-2020-10732</a>

    <p>Se encontró una fuga de información de la memoria privada del núcleo al espacio de usuario
    en la implementación del volcado de la memoria de procesos del espacio de usuario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10751">CVE-2020-10751</a>

    <p>Dmitry Vyukov informó de que el subsistema SELinux no gestionaba
    correctamente la validación de múltiples mensajes, lo que podía permitir que un atacante
    con privilegios eludiera las restricciones SELinux de netlink.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10757">CVE-2020-10757</a>

    <p>Fan Yang informó de un defecto en la manera en que mremap manejaba las páginas gigantes DAX
    que permitía que un usuario local elevara sus privilegios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10942">CVE-2020-10942</a>

    <p>Se descubrió que el controlador vhost_net no validaba
    correctamente el tipo de sockets definidos como backends. Un usuario local
    autorizado a acceder a /dev/vhost-net podía usar esto para provocar corrupción
    de pila a través de llamadas al sistema manipuladas, dando lugar a denegación de
    servicio (caída) o, posiblemente, a elevación de privilegios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11494">CVE-2020-11494</a>

    <p>Se descubrió que el controlador de red slcan (CAN sobre línea serie)
    no inicializaba completamente las cabeceras CAN para los paquetes recibidos,
    dando lugar a fugas de información del kernel al espacio de usuario o
    a través de la red CAN.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11565">CVE-2020-11565</a>

    <p>Entropy Moe informó de que el sistema de archivos en memoria compartida (tmpfs)
    no trataba correctamente una opción de montaje <q>mpol</q> especificando una lista
    de nodos vacía, lo que daba lugar a escritura fuera de límites en la pila. Si los espacios
    de nombres de usuario estaban habilitados, un usuario local podía utilizar esto para provocar
    denegación de servicio (caída) o, posiblemente, elevación de privilegios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11608">CVE-2020-11608</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2020-11609">CVE-2020-11609</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2020-11668">CVE-2020-11668</a>

    <p>Se descubrió que los controladores multimedia ov519, stv06xx y
    xirlink_cit no validaban correctamente los descriptores de los dispositivos USB. Un
    usuario presente físicamente con un dispositivo USB especialmente construido
    podía usar esto para provocar denegación de servicio (caída) o, posiblemente,
    elevación de privilegios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12114">CVE-2020-12114</a>

    <p>Piotr Krysiuk descubrió una condición de carrera entre las operaciones
    umount y pivot_root en el núcleo del sistema de archivos (vfs). Un usuario local
    con capacidad CAP_SYS_ADMIN en cualquier espacio de nombres de usuario podía usar
    esto para provocar denegación de servicio (caída).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12464">CVE-2020-12464</a>

    <p>Kyungtae Kim informó de una condición de carrera en el núcleo USB que podía
    dar lugar a un «uso tras liberar» («use-after-free»). No está clara la forma de explotar
    esto, pero podría dar lugar a denegación de servicio (caída o
    corrupción de memoria) o a elevación de privilegios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12652">CVE-2020-12652</a>

    <p>Tom Hatskevich informó de un fallo en los controladores de almacenamiento mptfusion.
    Un gestor de ioctl leía un parámetro de la memoria de usuario dos veces,
    creando una condición de carrera que podía dar lugar al bloqueo incorrecto
    de estructuras de datos internas. Un usuario local autorizado a acceder a
    /dev/mptctl podía usar esto para provocar denegación de servicio (caída o
    corrupción de memoria) o elevación de privilegios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12653">CVE-2020-12653</a>

    <p>Se descubrió que el controlador WiFi mwifiex no
    validaba suficientemente las solicitudes de escaneo, dando lugar a un potencial desbordamiento
    de memoria dinámica («heap»). Un usuario local con capacidad CAP_NET_ADMIN podía
    usar esto para provocar denegación de servicio (caída o corrupción de memoria)
    o, posiblemente, elevación de privilegios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12654">CVE-2020-12654</a>

    <p>Se descubrió que el controlador WiFi mwifiex no
    validaba suficientemente los parámetros WMM recibidos de un punto de acceso
    (AP, por sus siglas en inglés), dando lugar a un potencial desbordamiento de memoria dinámica («heap»). Un AP malicioso
    podía usar esto para provocar denegación de servicio (caída o corrupción
    de memoria) o, posiblemente, para ejecutar código en un sistema vulnerable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12770">CVE-2020-12770</a>

    <p>Se descubrió que el controlador sg (SCSI «generic») no
    liberaba correctamente recursos internos en un caso concreto de error.
    Un usuario local autorizado a acceder a un dispositivo sg podía, posiblemente, usar
    esto para provocar denegación de servicio (agotamiento de recursos).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13143">CVE-2020-13143</a>

    <p>Kyungtae Kim informó de una potencial escritura fuera de límites en la memoria dinámica («heap»)
    del subsistema de dispositivos («gadget») USB. Un usuario local autorizado a escribir en
    el sistema de archivos de configuración de gadgets podía usar esto para provocar
    denegación de servicio (caída o corrupción de memoria) o, potencialmente,
    elevación de privilegios.</p></li>

</ul>

<p>Para la distribución «antigua estable» (stretch), estos problemas se han
corregido en la versión 4.9.210-1+deb9u1. Esta versión corrige también algunos
fallos relacionados que no tienen identificativo CVE propio y una regresión en
el controlador macvlan introducida en la versión anterior (fallo #952660).</p>

<p>Le recomendamos que actualice los paquetes de linux.</p>

<p>Para información detallada sobre el estado de seguridad de linux, consulte su
página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4698.data"
