#use wml::debian::translation-check translation="26497e59c7b2d67a683e46d880d05456d453f794"
<define-tag pagetitle>Atualização Debian 10: 10.2 lançado</define-tag>
<define-tag release_date>2019-11-16</define-tag>
<define-tag frontpage>yes</define-tag>

#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.2</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>O projeto Debian está feliz em anunciar a segunda atualização de sua versão 
estável (stable) Debian <release> (codinome <q><codename></q>).
Esta versão pontual adiciona principalmente correções para problemas de 
segurança, além de pequenos ajustes para problemas sérios. Avisos de
segurança já foram publicados em separado e são referenciados quando
disponíveis.</p>

<p>Por favor note que a versão pontual não constitui uma nova versão do Debian
<release>, mas apenas atualiza alguns dos pacotes já incluídos. Não há
necessidade de jogar fora as antigas mídias do <q><codename></q>. Após a
instalação, os pacotes podem ser atualizados para as versões atuais usando um
espelho atualizado do Debian.</p>

<p>Aquelas pessoas que frequentemente instalam atualizações a partir do
security.debian.org não terão que atualizar muitos pacotes, e a maioria de tais
atualizações estão incluídas na versão pontual.</p>

<p>Novas imagens de instalação logo estarão disponíveis nos locais habituais.</p>

<p>A atualização de uma instalação existente para esta revisão pode ser feita
apontando o sistema de gerenciamento de pacotes para um dos muitos espelhos
HTTP do Debian. Uma lista abrangente de espelhos está disponível em:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Correções de bugs gerais</h2>

<p>Esta atualização da versão estável (stable) adiciona algumas correções
importantes para os seguintes pacotes:</p>

<table border=0>
<tr><th>Pacote</th>               <th>Justificativa</th></tr>
<correction aegisub "Correção de falha ao selecionar um idioma na parte inferior da lista <q>Linguagem do corretor ortográfico</q>; Correção de falha ao clicar com o botão direito do mouse na caixa de texto de legendas">
<correction akonadi "Correção de várias falhas / problemas de travamento">
<correction base-files "Atualização de /etc/debian_version para o lançamento pontual">
<correction capistrano "Correção de falha em remover versões antigas quando houverem muitas">
<correction cron "Parar a utilização de API SELinux obsoleta.">
<correction cyrus-imapd "Corrige perda de dados em atualização a partir da versão 3.0.0 ou anterior">
<correction debian-edu-config "Tratar condicionalmente os arquivos de configuração mais recentes do Firefox ESR; adicionar a entrada post-up em /etc/network/interfaces eth0 condicionalmente">
<correction debian-installer "Corrige fontes ilegíveis em monitores hidpi em imagens netboot inicializadas com EFI">
<correction debian-installer-netboot-images "Recompilando conforme proposed-updates">
<correction distro-info-data "Adicionado Ubuntu 20.04 LTS, Focal Fossa">
<correction dkimpy-milter "Nova versão estável do desenvolvedor original; correção do suporte sysvinit; detecta mais erros de codificação ASCII de forma a melhorar a resiliência contra dados ruins; corrige mensagem de extração de forma que assinar o mesmo passo pelo milter como verificação funcione corretamente">
<correction emacs "Corrige chave de empacotamento EPLA">
<correction fence-agents "Corrige a remoção incompleta de fence_amt_ws">
<correction flatpak "Nova versão estável do desenvolvedor">
<correction flightcrew "Correções de segurança [CVE-2019-13032 CVE-2019-13241]">
<correction fonts-noto-cjk "Correção de seleção agressiva de fontes Noto CJK em navegadores web modernos sob locales Chineses">
<correction freetype "Manipular adequadamente os pontos fantasmas para fontes com sugestões variáveis">
<correction gdb "Recompilado de acordo com o novo libbabeltrace, com número de versão mais alto para evitar conflito com carregamento anterior">
<correction glib2.0 "Assegurar que os clientes da libdbus possam se autenticar com um GDBusServer como o que está em ibus">
<correction gnome-shell "Nova versão estável do desenvolvedor; corrigir o truncamento de mensagens longas nos diálogos modais da shell; evitar o colapso na realocação de atores mortos">
<correction gnome-sound-recorder "Corrige falha quando selecionado uma gravação">
<correction gnustep-base "Desabilita o daemon gdomap que foi acidentalmente habilitado em atualizações a partir do stretch">
<correction graphite-web "Remove função <q>send_email</q> não utilizada [CVE-2017-18638]; evita erros de hora em hora no cron quando não há whisper database">
<correction inn2 "Corrige negociação de suítes de cifragem DHE ">
<correction libapache-mod-auth-kerb "Corrige Fbug de uso após livre que leva a falha">
<correction libdate-holidays-de-perl "Marca o Dia internacional das Crianças (20 de setembro) como um feriado na Turíngia de 2019 em diante">
<correction libdatetime-timezone-perl "Atualização de dados inclusos">
<correction libofx "Corrige problema de desreferência de ponteiro nulo [CVE-2019-9656]">
<correction libreoffice "Corrige driver do postgresql com PostgreSQL 12">
<correction libsixel "Corrige diversos problemas de segurança [CVE-2018-19756 CVE-2018-19757 CVE-2018-19759 CVE-2018-19761 CVE-2018-19762 CVE-2018-19763 CVE-2019-3573 CVE-2019-3574]">
<correction libxslt "Corrige ponteiro pendurado em xsltCopyText  [CVE-2019-18197]">
<correction lucene-solr "Desabilita chamada obsoleta para ContextHandler em solr-jetty9.xml; corrige permissões de Jetty no índice SOLR">
<correction mariadb-10.3 "Nova versão do desenvolvedor original">
<correction modsecurity-crs "Corrige regras de upload em PHP script [CVE-2019-13464]">
<correction mutter "Nova versão do desenvolvedor original">
<correction ncurses "Corrige diversos problemas de segurança [CVE-2019-17594 CVE-2019-17595] e outras questões em tic">
<correction ndppd "Evita arquivo PID editável de forma global que estava quebrando os daemon init scripts">
<correction network-manager "Corrige permissões de arquivo para <q>/var/lib/NetworkManager/secret_key</q> e /var/lib/NetworkManager">
<correction node-fstream "Corrige problema de sobrescrita arbitrária de arquivo [CVE-2019-13173]">
<correction node-set-value "Corrige poluição de protótipo [CVE-2019-10747]">
<correction node-yarnpkg "Obrigar a utilização de HTTPS para registros regulares">
<correction nx-libs "Corrige regressões introduzidas no upload anterior, afetando x2go">
<correction open-vm-tools "Corrige vazamentos de memória e manipulação de erros">
<correction openvswitch "Atualiza debian/ifupdown.sh para permitir a criação da MTU; corrige as dependências Python para usar Python 3">
<correction picard "Atualiza traduções para corrigir falha com locale espanhol">
<correction plasma-applet-redshift-control "Corrige o modo manual quando usado com versões redshift acima de 1,12">
<correction postfix "Nova versão original do desenvolvedor; contornar o baixo desempenho do TCP loopback">
<correction python-cryptography "Corrige falhas do conjunto de teste quando compilado para versões OpenSSL mais recentes; corrige um vazamento de memória que pode ser acionado ao analisar extensões de certificados x509 como AIA">
<correction python-flask-rdf "Adiciona dependências em python{3,}-rdflib">
<correction python-oslo.messaging "Nova versão original do desenvolvedor; Corrige o destino da conexão do interruptor quando um nó de agrupamento de rabbitqm desaparece">
<correction python-werkzeug "Assegura que contêineres Docker tenham PINs únicos para debugger PINs [CVE-2019-14806]">
<correction python2.7 "Corrige diversos problemas de segurança [CVE-2018-20852 CVE-2019-10160 CVE-2019-16056 CVE-2019-16935 CVE-2019-9740 CVE-2019-9947]">
<correction quota "Corrige rpc.rquotad consumindo 100% de CPU">
<correction rpcbind "Permite que chamados remotas sejam habilitadas em tempo de execução">
<correction shelldap "Repara autenticações SASL, adiciona uma opção 'sasluser'">
<correction sogo "Corrige a apresentação de e-mails PGP-signed">
<correction spf-engine "Nova versão do desenvolvedor original; Corrige suporte a sysvinit">
<correction standardskriver "Corrige aviso de depreciação do config.RawConfigParser; utiliza comando externo <q>ip</q> em vez de comando obsoleto <q>ifconfig</q>">
<correction swi-prolog "Utiliza HTTPS quanto contactando servidores de pacote upstream">
<correction systemd "core: nunca propaga erros de falha de recarregamento ao resultado do serviço; corrige falha de sync_file_range em contêineres nspawn em arm, ppc; corrige o não funcionamento da diretiva RootDirectory quando utilizada em combinação com User; assegura que  controles de acesso na interface D-Bus do systemd-resolved sejam impostas corretamente [CVE-2019-15718]; Corrige StopWhenUnneeded=true para unidades montadas; faz MountFlags=shared funcionarem novamente">
<correction tmpreaper "Previne a falha dos serviços do sistema que utilizam PrivateTmp=true">
<correction trapperkeeper-webserver-jetty9-clojure "Restaurar a compatibilidade SSL com versões mais recentes do Jetty">
<correction tzdata "Nova versão do desenvolvedor original">
<correction ublock-origin "Nova versão do desenvolvedor original, compatível com Firefox ESR68">
<correction uim "Ressuscita libuim-data como um pacote de transição, resolvendo alguns problemas após atualizações para o buster">
<correction vanguards "Nova versão do desenvolvedor original; previne a recarga da configuração do tor através de SIGHUP causando negação de serviço para as proteções do vanguards">
</table>


<h2>Atualização de Segurança</h2>


<p>Esta revisão adiciona as seguintes atualizações de segurança para a versão 
estável (stable). 
A Equipe de Segurança já lançou um aviso para cada uma destas atualizações:</p>

<table border=0>
<tr><th>ID do aviso</th>  <th>Pacote</th></tr>
<dsa 2019 4509 apache2>
<dsa 2019 4511 nghttp2>
<dsa 2019 4512 qemu>
<dsa 2019 4514 varnish>
<dsa 2019 4515 webkit2gtk>
<dsa 2019 4516 firefox-esr>
<dsa 2019 4517 exim4>
<dsa 2019 4518 ghostscript>
<dsa 2019 4519 libreoffice>
<dsa 2019 4520 trafficserver>
<dsa 2019 4521 docker.io>
<dsa 2019 4523 thunderbird>
<dsa 2019 4524 dino-im>
<dsa 2019 4525 ibus>
<dsa 2019 4526 opendmarc>
<dsa 2019 4527 php7.3>
<dsa 2019 4528 bird>
<dsa 2019 4530 expat>
<dsa 2019 4531 linux-signed-amd64>
<dsa 2019 4531 linux-signed-i386>
<dsa 2019 4531 linux>
<dsa 2019 4531 linux-signed-arm64>
<dsa 2019 4532 spip>
<dsa 2019 4533 lemonldap-ng>
<dsa 2019 4534 golang-1.11>
<dsa 2019 4535 e2fsprogs>
<dsa 2019 4536 exim4>
<dsa 2019 4538 wpa>
<dsa 2019 4539 openssl>
<dsa 2019 4539 openssh>
<dsa 2019 4541 libapreq2>
<dsa 2019 4542 jackson-databind>
<dsa 2019 4543 sudo>
<dsa 2019 4544 unbound>
<dsa 2019 4545 mediawiki>
<dsa 2019 4547 tcpdump>
<dsa 2019 4549 firefox-esr>
<dsa 2019 4550 file>
<dsa 2019 4551 golang-1.11>
<dsa 2019 4553 php7.3>
<dsa 2019 4554 ruby-loofah>
<dsa 2019 4555 pam-python>
<dsa 2019 4556 qtbase-opensource-src>
<dsa 2019 4557 libarchive>
<dsa 2019 4558 webkit2gtk>
<dsa 2019 4559 proftpd-dfsg>
<dsa 2019 4560 simplesamlphp>
<dsa 2019 4561 fribidi>
<dsa 2019 4562 chromium>
</table>


<h2>Pacotes removidos</h2>

<p>Os seguintes pacotes foram removidos devido circunstâncias além do nosso
controle:</p>

<table border=0>
<tr><th>Pacote</th>               <th>Razão</th></tr>
<correction firefox-esr "[armel] Não mais suportável devido a dependência de compilação do nodejs">

</table>

<h2>Instalador do Debian</h2>
<p>O instalador foi atualizado para incluir as incorporadas
na versão estável (stable) pela versão pontual.</p>

<h2>URLs</h2>

<p>As listas completas dos pacotes que foram alterados por esta revisão:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>A atual versão estável (stable):</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Atualizações propostas (proposed updates) para a versão estável (stable):</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>informações da versão estável (stable) (notas de lançamento, errata, etc):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Anúncios de segurança e informações:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Sobre o Debian</h2>

<p>O projeto Debian é uma associação de desenvolvedores(as) de Software Livre
que dedicam seu tempo e esforço como voluntários(as) para produzir o sistema
operacional livre Debian.</p>

<h2>Informações de contato</h2>

<p>Para mais informações, por favor visite as páginas web do Debian em
<a href="$(HOME)/">https://www.debian.org/</a>, envie um e-mail (em inglês) para
&lt;press@debian.org&gt;, ou entre em contato (em inglês) com a equipe de
lançamento da versão estável (stable) em
&lt;debian-release@lists.debian.org&gt;.</p>
