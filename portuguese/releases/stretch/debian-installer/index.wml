#use wml::debian::template title="Informação de instalação do Debian &ldquo;stretch&rdquo;" NOHEADER="true"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/stretch/release.data"
#use wml::debian::translation-check translation="73724899f30de2473094581fc7b72d9858bdb469"

<h1>Instalando o Debian <current_release_stretch></h1>

<if-stable-release release="buster">
<p><strong>O Debian 9 foi substituído pelo
<a href="../../buster/">Debian 10 (<q>buster</q>)</a>. Algumas destas
imagens de instalação podem não estar mais disponíveis, ou podem não funcionar
mais. Recomendamos que você instale o buster em vez disso.
</strong></p>
</if-stable-release>

<p>
<strong>Para instalar o Debian</strong> <current_release_stretch>
(<em>stretch</em>), baixe qualquer uma das seguintes imagens (todas as imagens
de CD/DVD i386 e amd64 também podem ser usadas em pendrives):
</p>

<div class="line">
<div class="item col50">
	<p><strong>imagem de CD netinst (geralmente 150-280 MB)</strong></p>
		<netinst-images />
</div>


</div>

<div class="line">
<div class="item col50">
	<p><strong>conjuntos completos de CD</strong></p>
		<full-cd-images />
</div>

<div class="item col50 lastcol">
	<p><strong>conjuntos completos de DVD</strong></p>
		<full-dvd-images />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (via <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-cd-torrent />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (via <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-dvd-torrent />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-dvd-jigdo />
</div>


</div>

<div class="line">
<div class="item col50">
<p><strong>Blu-ray  (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-bluray-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>outras imagens (netboot, pendrive flexível, etc.)</strong></p>
<other-images />
</div>
</div>

<div id="firmware_nonfree" class="warning">
<p>
Caso qualquer parte do hardware em seu sistema <strong>necessite que algum firmware
não livre seja carregado</strong> com o controlador (driver) de dispositivo, você pode usar um dos
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stretch/current/">\
arquivos compactados de pacotes de firmwares comuns</a> ou baixar uma imagem
<strong>não oficial</strong> que incluem esses firmwares <strong>não livres</strong>.
As instruções de como usar os arquivos compactados e informações gerais sobre
carregamento de firmwares durante uma instalação podem ser encontradas no guia
de instalação (veja a documentação abaixo).
</p>
<div class="line">
<div class="item col50">
<p><strong>imagens de CD netinst (geralmente 240-290 MB) <strong>não livres</strong>
<strong>com firmwares</strong></strong></p>
<small-non-free-cd-images />
</div>
</div>
</div>



<p>
<strong>Notas</strong>
</p>
<ul>
    <li>
        Para baixar imagens completas de CD e DVD é recomendado o uso de BitTorrent
        ou jigdo.
    </li><li>
        Para arquiteturas menos comuns, apenas um número limitado de imagens dos
        conjuntos de CDs e DVDs está disponível como arquivo ISO ou via BitTorrent.
        Os conjuntos completos estão disponíveis apenas via jigdo.
    </li><li>
        As imagens de <em>CD</em> multi-arch suportam i386/amd64; o processo é
        semelhante à instalação a partir de uma imagem netinst de uma única arquitetura.
    </li><li>
        A imagem de <em>DVD</em> multi-arch suporta i386/amd64; o processo é
        semelhante à instalação a partir de uma imagem de CD de uma única arquitetura;
        o DVD também contém o código-fonte para todos os pacotes incluídos.
    </li><li>
        Para as imagens de instalação, arquivos de verificação (<tt>SHA256SUMS</tt>,
        <tt>SHA512SUMS</tt> e outros) estão disponíveis a partir do mesmo diretório das
        imagens.
    </li>
</ul>


<h1>Documentação</h1>

<p>
<strong>Caso você queira ler apenas um documento</strong> antes da instalação, leia
nosso <a href="../i386/apa">Howto de Instalação</a>, um rápido passo a passo
do processo de instalação. Outras documentações úteis incluem:
</p>

<ul>
<li><a href="../installmanual">Guia de instalação do Stretch</a><br />
instruções detalhadas de instalação</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">FAQ do instalador do
Debian</a> e <a href="$(HOME)/CD/faq/">FAQ do CD do Debian</a><br />
perguntas comuns e respostas</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Wiki do instalador do Debian</a><br />
documentação mantida pela comunidade</li>
</ul>

<h1 id="errata">Errata</h1>

<p>
Esta é uma lista de problemas conhecidos no instalador que acompanha o
Debian <current_release_stretch>. Se você teve algum problema instalando
o Debian e não vê seu problema listado aqui, por favor, envie-nos um
<a href="$(HOME)/releases/stable/i386/ch05s04.html#submit-bug">relatório de
instalação</a> descrevendo seu problema ou
<a href="https://wiki.debian.org/DebianInstaller/BrokenThings">verifique a
wiki</a> para outros problemas conhecidos.
</p>

## Translators: copy/paste from devel/debian-installer/errata
<h3 id="errata-r0">Errata para a versão 9.0</h3>

<dl class="gloss">

<!--
     <dt>Instalações de desktop podem não funcionar usando somente o CD#1</dt>

     <dd>Devido a limitações de espaço no primeiro CD, nem todos os pacotes
     esperados da área de trabalho GNOME cabem no CD#1. Para uma instalação
     com sucesso, utilize fontes extras de pacotes (por exemplo, um segundo
     CD ou um espelho de rede) ou utilize um DVD em vez disso.

     <br /> <b>Situação:</b> É improvável que mais esforços possam ser feitos
     para caber mais pacotes no CD#1.</dd>
-->

<dt>O pkgsel não instalou atualizações com alterações da Interface Binária de Aplicativos (ABI) (por padrão)</dt>

     <dd>Bug <a href="https://bugs.debian.org/908711">#908711</a>:
     durante a instalação com a rede de internet ativada, as
     atualizações de segurança instaladas não incluem atualizações que dependem
     de um novo pacote binário, devido a uma alteração na ABI do kernel ou da biblioteca.

     <br /> <b>Estado:</b> Este bug foi corrigido na atualização 9.6</dd>

     <dt>O APT estava vulnerável a um ataque man-in-the-middle</dt>

     <dd>Um bug no método de transporte APT HTTP
     (<a href="https://www.debian.org/security/2019/dsa-4371">CVE-2019-3462</a>)
     podia ser explorado por um invasor localizado entre o APT e um espelho,
     possibilitando a instalação de pacotes maliciosos adicionais.

     <br /> Isto pode ser atenuado, desativando o uso da rede durante
     instalação inicial e atualização seguindo as instruções em
     <a href="$(HOME)/security/2019/dsa-4371">DSA-4371</a>.

     <br /> <b>Estado:</b> Este bug foi corrigido na atualização 9.7</dd>

</dl>

<if-stable-release release="stretch">
<p>
Versões melhoradas do sistema de instalação estão sendo desenvolvidas para a
próxima versão do Debian, e também podem ser usadas para instalar o stretch.
Para detalhes, veja
<a href="$(HOME)/devel/debian-installer/">a página do projeto do instalador do
Debian</a>.
</p>
</if-stable-release>
