# Emmanuel Galatoulas <galas@tee.gr>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2019-07-14 01:29+0200\n"
"Last-Translator: Emmanuel Galatoulas <galas@tee.gr>\n"
"Language-Team: Greek <debian-www@lists.debian.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 2.0\n"

#: ../../english/consultants/consultant.defs:6
msgid "Name:"
msgstr "Όνομα:"

#: ../../english/consultants/consultant.defs:9
msgid "Company:"
msgstr "Εταιρεία:"

#: ../../english/consultants/consultant.defs:12
msgid "Address:"
msgstr "Διεύθυνση"

#: ../../english/consultants/consultant.defs:15
msgid "Contact:"
msgstr "Επαφή:"

#: ../../english/consultants/consultant.defs:19
msgid "Phone:"
msgstr "Τηλέφωνο:"

#: ../../english/consultants/consultant.defs:22
msgid "Fax:"
msgstr "Τηλεομοιοτυπία:"

#: ../../english/consultants/consultant.defs:25
msgid "URL:"
msgstr "Διεύθυνση:"

#: ../../english/consultants/consultant.defs:29
msgid "or"
msgstr "ή"

#: ../../english/consultants/consultant.defs:34
msgid "Email:"
msgstr "Ηλ.Διεύθυνση:"

#: ../../english/consultants/consultant.defs:52
msgid "Rates:"
msgstr "Χρεώσεις:"

#: ../../english/consultants/consultant.defs:55
msgid "Additional Information"
msgstr "Πρόσθετες Πληροφορίες"

#: ../../english/consultants/consultant.defs:58
msgid "Willing to Relocate"
msgstr "Ενδιαφέρεται για μεταφορά"

#: ../../english/consultants/consultant.defs:61
msgid ""
"<total_consultant> Debian consultants listed in <total_country> countries "
"worldwide."
msgstr ""
"Υπάρχουν <total_consultant>  σύμβουλοι του Debian σε <total_country> χώρες "

#: ../../english/template/debian/consultant.wml:6
msgid "List of Consultants"
msgstr "Κατάλογος συμβούλων"

#: ../../english/template/debian/consultant.wml:9
msgid "Back to the <a href=\"./\">Debian consultants page</a>."
msgstr "Πίσω στην σελίδα των <a href=\"./\">συμβούλων του Debian</a>."
