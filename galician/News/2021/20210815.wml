<define-tag pagetitle>Debian Edu / Skolelinux Bullseye — unha solución Linux completa para a túa escola </define-tag>
<define-tag release_date>2021-08-15</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="b6e1b7f61abd59aa2dfcb8494afd20f6146915e4" maintainer="parodper@gmail.com"

<p>
Es o administrador dunha sala de ordenadores ou da rede de toda unha escola?
Gustaríache que os servidores, estacións de traballo e portátiles que instalen podan funcionar xuntos?
Queres a estabilidade de Debian cos servizos de redes preconfigurados?
Gustaríache ter unha ferramenta na rede para xestionar sistemas e centos de contas de usuario?
Algunha vez pensaches en de que forma, e como, poderías darlle novos usos aos ordenadores vellos?
</p>

<p>
Entón Debian Edu é o que buscas. Os mesmos profesores ou o seu servizo técnico
poden preparar un sistema completo de estudo multiusuario para varias máquinas
nuns poucos días. Debian Edu ven con centos de aplicacións preinstaladas, e
sempre podes engadirlle máis paquetes de Debian.
</p>

<p>
O equipo de desenvolvemento de Debian Edu está ledo por poder
anunciar Debian Edu 11 <q>Bullseye</q>, a versión de
Debian Edu / Skolelinux baseada na versión
Debian 11 <q>Bullseye</q>. Por favor pensa en probala e
cóntanos que che parece (&lt;debian-edu@lists.debian.org&gt;)
para poder axudarnos a mellorala máis.
</p>

<h2>Sobre Debian Edu e Skolelinux</h2>

<p>
<a href="https://blends.debian.org/edu"> Debian Edu, tamén chamado
Skolelinux</a>, é unha distribución de Linux baseada en Debian que
fornece un ambiente preconfigurado do trinque dunha rede escolar
completamente configurada.
Inmediatamente despois da instalación, un servidor da escola executando
todos os servizos necesarios para a rede escolar configúrase, esperando
a que se engadan usuarios e máquinas a través de GOsa², unha cómoda
interface na rede.
Un ambiente netboot (arrancar dende a rede) é preparado, para que
despois da instalación inicial do servidor principal dende
un CD / DVD / BD ou memoria USB o resto das máquinas póidanse instalar
a través da rede.
Os ordenadores máis vellos (sobre os 10 anos de idade) pódense usar como
clientes lixeiros ou estacións de traballo LTSP (Proxecto Linux de Servidor con Terminal),
arrancando dende a rede sin necesitar instalación ou configuración algunha.
O servidor escolar Debian Edu mantén unha base de datos LDAP e un servizo
de autenticación Kerberos; tamén centraliza os cartafoles persoais;
xestiona un servidor DHCP, un proxy web e moitos servizos máis.
O ambiente de escritorio contén máis de 80 paquetes con programas educacionais,
e moitos máis están dispoñibles no arquivo de Debian. As escolas poden elixir entre
os ambientes de escritorio Xfce, GNOME, LXDE, MATE, KDE Plasma, Cinnamon, e LXQt.
</p>

<h2>Novas características en Debian Edu 11 <q>Bullseye</q></h2>

<p>Estes son algúns exemplos sacados das notas da versión Debian Edu 11 <q>Bullseye</q>,
baseada na versión 11 <q>Bullseye</q> de Debian.
A lista completa incluíndo máis detalles é parte do relacionado
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/Features#New_features_in_Debian_Edu_Bullseye">capítulo do manual de Debian Edu</a>.
</p>

<ul>
<li>
Novo <a href="https://ltsp.org">LTSP</a> para ser compatíble coas estacións de traballo sen
disqueteira. Inda é compatible cos clientes lixeiros, pero agora usando a tecnoloxía <a href="https://wiki.x2go.org">X2Go</a>
</li>
<li>
Pódese arrancar dende a rede usando iPXE, en lugar de PXELINUX,
para ser compatible con LTSP
</li>
<li>
O úsase o modo gráfico do Instalador de Debian para as instalacións por iPXE
</li>
<li>
Agora Samba configúrase coma un <q>servidor en si mesmo</q> con compatibilidade con SMB2/SMB3.
</li>
<li>
O buscador por defecto tanto en Firefox ESR e Chromium é DuckDuckGo.
</li>
<li>
Engadiuse unha nova ferramenta para configurar freeRADIUS con compatibilidade con EAP-TTLS/PAP e PEAP-MSCHAPV2.
</li>
<li>
Está dispoñible unha ferramenta mellorada para configurar un novo sistema co
perfil <q>Minimal</q> (Mínimo) como porta de enlace dedicada.
</li>
</ul>

<h2>Opcións para descargar, pasos para instalar e manual</h2>

<p>
Están dispoñibles imaxes de CD para as versións do Instalador por Rede Oficial de Debian
para ordenadores de 64 e 32 bits. A antiga imaxe de 32 bits só será necesaria
en casos especiais (ordenadores de máis de 15 anos). As imaxes pódense descargar nos seguintes sitios:
</p>
<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-cd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-cd/>
</li>
</ul>

<p>
Tamén están dispoñibles coma alternativa imaxes BD oficiais de Debian
(máis de 5 GB). É posible configurar unha rede Debian Edu completa sen
unha conexión a Internet (incluíndo todos os ambientes de escritorio e
localización para todas as linguas que permite Debian). As imaxes
pódense descargar dos seguintes sitios:
</p>

<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-bd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-bd/>
</li>
</ul>

<p>
As imaxes pódense verificar usando as sumas de comprobación que están
no cartafol de descargas.
<br />
Cando teñas descargada a imaxe, podes comprobar que
</p>
<ul>
<li>
a súa suma de comprobación coincide coa indicada no ficheiro coas sumas,
</li>
<li>
e que o ficheiro coas sumas non foi manipulado.
</li>
</ul>
<p>
Para máis información sobre como realizar estes pasos consulta a
<a href="https://www.debian.org/CD/verify">guía de verificación</a>.
</p>

<p>
Debian Edu 11 <q>Bullseye</q> está completamente baseado en Debian 11 <q>Bullseye</q>;
polo tanto as fontes de todos os paquetes están dispoñibles no arquivo de Debian.
</p>

<p>
Por favor consulte a
<a href="https://wiki.debian.org/DebianEdu/Status/Bullseye">páxina co estado de Debian Edu Bullseye</a>.
para obter información de última hora sobre Debian Edu 11 <q>Bullseye</q>, incluíndo
instrucións sobre como usar <code>rsync</code> para descargar as imaxes ISO.
</p>

<p>
Se actualizas dende Debian Edu 10 <q>Buster</q> por favor lea o
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/Upgrades">capítulo no manual de Debian Edu</a>.
</p>

<p>
Para as notas da instalación por favor lea o
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/Installation#Installing_Debian_Edu">capítulo no manual de Debian Edu axeitado</a>.
</p>

<p>
Despois da instalación necesitas facer estes
<a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/GettingStarted">primeiros pasos</a>.
</p>

<p>
Por favor vexa as <a href="https://wiki.debian.org/DebianEdu/Documentation/Bullseye/">páxinas wiki de Debian Edu</a>
para obter a última versión en inglés do manual de Debian Edu <q>Bullseye</q>.
O manual está traducido ao alemán, francés, italiano, danés, neerlandés,
noruegués bokmål, xaponés, chino simplificado e portugués de Portugal.
Existen traducións parciais en castelán, romanés e polaco.
Está dispoñible unha vista xeral de <a href="https://jenkins.debian.net/userContent/debian-edu-doc/"> as últimas versións publicadas do manual</a>.
</p>

<p>
Pódese atopar máis información sobre Debian 11 <q>Bullseye</q> nas notas da versión
e no manual de instalación. Véxase <a href="$(HOME)/">https://www.debian.org/</a>.
</p>

<h2>Sobre Debian</h2>

<p>O proxecto Debian é unha asociación de desenvolvedores de Software Libre que
traballan voluntariamente para producir o sistema operativo completamente libre Debian.</p>

<h2>Información de Contacto</h2>

<p>Para máis información, por favor visite as páxinas web de Debian en <a href="$(HOME)/">https://www.debian.org/</a> ou envíe un correo a
&lt;press@debian.org&gt;.</p>
