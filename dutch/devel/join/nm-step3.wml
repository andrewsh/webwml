#use wml::debian::template title="Stap 3: Filosofie en procedures" NOHEADER="true"
#include "$(ENGLISHDIR)/devel/join/nm-steps.inc"
#use wml::debian::translation-check translation="517d7a4f999561b8f28e207214a40990fdc3da49"

<p>De informatie op deze pagina, hoewel openbaar, zal voornamelijk van belang
zijn voor toekomstige Debian-ontwikkelaars.</p>

<h2>Stap 3: Filosofie en procedures</h2>

<h3>Filosofie</h3>
<p>
Van de <a href="./newmaint#Applicant">kandidaat</a> wordt verwacht dat hij/zij
past in de Debian-gemeenschap, die is opgebouwd rond de filosofie van Vrije
Software. Wat Debian onder "vrij" verstaat en hoe dit wordt toegepast wordt
uitgelegd in het <a href="$(HOME)/social_contract">Sociaal contract</a> en de
<a href="$(HOME)/social_contract#richtlijnen">Richtlijnen voor Vrije Software
van Debian</a>.
<br />
Toekomstige ontwikkelaars moeten deze documenten goed genoeg begrijpen om de
daarin beschreven ideeën en idealen in hun eigen woorden weer te geven. Het
bepalen van hoe dit begrip precies wordt bereikt en gecommuniceerd, wordt aan
de kandidaat en de beheerder van zijn kandidatuur overgelaten. De volgende
methoden zijn slechts bedoeld als suggestie, niet als vereiste, maar zijn
voorbeelden van manieren om deze stap van het proces te voltooien. De
kandidaat zal verscheidene gelegenheden krijgen om te laten zien dat hij deze
zaken begrijpt.
</p>

<p>Opmerking: hoewel we van kandidaten verlangen dat ze akkoord gaan met de
filosofie van Debian, is dit beperkt tot werk dat voor Debian wordt gedaan. We
begrijpen dat mensen hun brood moeten verdienen en soms moeten werken aan
niet-vrije projecten voor hun werkgever of klant.</p>

<p>Debian doet geen poging om controle te verwerven over wat de kandidaat denkt
over deze onderwerpen, maar het is belangrijk voor de stabiliteit van zo'n
groot en amorf project dat alle deelnemers binnen dezelfde reeks basisprincipes
en overtuigingen werken.</p>

<p>De <a href="./newmaint#AppMan">Kandidatuurbeheerder</a>
(Application Manager - AM) beslist wanneer aan de criteria voor elke stap is
voldaan. De volgende richtlijnen proberen alleen bruikbare voorbeelden te
geven. In de meeste gevallen zal een mix van al deze richtlijnen worden
gebruikt.
<br>
De AM en de <a href="./newmaint#Applicant">kandidaat</a> kunnen beslissen over
andere taken dan deze welke hier vermeld worden. Deze taken moeten duidelijk
worden gedocumenteerd in het eindrapport aan de
<a href="./newmaint#DAM">Accountbeheerder van Debian</a>.</p>

<dl>
 <dt>1. Het <a href="$(HOME)/social_contract">Sociaal Contract</a></dt>
 <dd><p>
  Het Sociaal Contract vermeldt de doelen en ambities van Debian. Het probeert
  ook onze zelfopgelegde verantwoordelijkheden naar de rest van de gemeenschap
  uit te drukken.
  <br />
  Een goed begrip van de prioriteiten die wij aan deze verschillende
  verantwoordelijkheden toekennen en overeenstemming daarmee is essentieel voor
  elke kandidaat.
  </p>

  <p>Het begrip kan op verschillende manieren worden aangetoond:</p>

  <ul>
   <li>Een discussie met de AM over de verschillende termen in het Sociaal
   Contract, waarin wordt uitgedrukt hoe ze zich verhouden tot elkaar en tot de
   organisatie van Debian.</li>

   <li>Een discussie over de persoonlijke doelstellingen van de kandidaat voor
   Debian en hoe die passen in het Sociaal Contract kan in sommige gevallen
   voldoende zijn.</li>

   <li>De kandidaat kan het Sociaal Contract in zijn eigen woorden verwoorden,
   en enkele van de meer complexe onderdelen uitleggen en uitleggen hoe Debian
   ernaar streeft hieraan te voldoen.<br />
   Opmerking: dit is de meest gekozen manier.
   </li>
  </ul>
  <br>
 </dd>

 <dt>2. De <a href="$(HOME)/social_contract#guidelines">Richtlijnen van Debian
voor Vrije Software</a> (Debian Free Software Guidelines - DFSG)</dt>
 <dd>
  <p>Deze principes fungeren als richtlijnen voor het bepalen van de vrijheid
  die een bepaalde licentie biedt.</p>

  <p>Hoewel de meeste kandidaten geen advocaat zijn, zou iedereen in staat
  moeten zijn om de basisprincipes die in deze richtlijnen uiteengezet worden,
  uit te drukken en te gebruiken.</p>

  <p>Het begrip kan op verschillende manieren worden aangetoond:</p>

  <ul>
   <li>De kandidaat bespreekt verschillende licenties en probeert aan te tonen
   of ze al dan niet vrij zijn. Daarbij kan de AM wijzen op bijzondere gevallen
   en verdere vragen stellen over de DFSG.
   </li>

   <li>De kandidaat vergelijkt de Richtlijnen van Debian voor Vrije Software
   met andere verklaringen over Vrije Software en wijst op overeenkomsten en
   verschillen.
   #FIXME: "verklaringen" is zo verkeerd, maar ik gebruikte al richtlijnen...
   </li>
  </ul>
 </dd>
</dl>

<p>Welke methode ook wordt gebruikt, de kandidaat moet het eens zijn met deze
beginselen en blijk geven van inzicht in de betekenis en de inhoud ervan.</p>

<p>
Als men niet akkoord kan gaan met deze voorwaarden, betekent dit het beëindigen
van het proces voor nieuwe leden.
</p>

<h3>Procedures</h3>

<p>De standaardprocedures en beleidslijnen die zich hebben ontwikkeld bij de
creatie van het Debian-systeem zijn erg belangrijk om het gedistribueerde werk
van vrijwilligers te beheren. Ze verzekeren de algehele kwaliteit van Debian en
helpen vaak om problemen tussen ontwikkelaars te voorkomen door een reeks
richtlijnen te bieden voor de interactie in speciale gevallen.</p>

<p>Hoe de <a href="./newmaint#Applicant">kandidaat</a> zijn begrip moet
aantonen is aan de <a href="./newmaint#AppMan">kandidatuurbeheerder</a>, maar
er zijn enkele essentiële zaken die altijd aan bod moeten komen. De volgende
lijst beschrijft wat zeker aan bod moet komen bij de controle van het aspect
procedures:</p>

<ul>
 <li><h4>Werken met het bugvolgsysteem</h4>
  Debian gebruikt het <a href="https://bugs.debian.org/">bugvolgsysteem</a>
  (Bug Tracking System - BTS) niet alleen om bugs in pakketten bij te houden,
  maar ook om verzoeken over de infrastructuur te verzamelen en de pakketten
  waaraan werk is en mogelijke toekomstige pakketten te beheren.
  <br />
  Toekomstige ontwikkelaars moeten het BTS kunnen beheersen en kunnen uitleggen
  hoe het kan worden gebruikt om alle beschikbare gegevens over problemen weer
  te geven.
 </li>

 <li><h4>Het release-proces van Debian</h4>
  Het release-proces van Debian is de basis voor zijn stabiliteit en
  veiligheid, dus toekomstige ontwikkelaars moeten begrijpen hoe het werkt,
  waarom het is gestructureerd zoals het is en welke uitzonderingen mogelijk
  zijn.
 </li>

 <li><h4>Debian's inspanningen op het gebied van internationalisering en
lokalisatie</h4>
  Aangezien slechts een klein deel van de wereld Engels spreekt, investeren
  ontwikkelaars en vertalers een aanzienlijke hoeveelheid tijd om Debian voor
  iedereen bruikbaar te maken. Er zijn veel specifieke hulpmiddelen en regels
  en toekomstige ontwikkelaars moeten hiervan op de hoogte zijn.
 </li>
</ul>

<p>Er zijn natuurlijk veel andere onderwerpen die bij de controle van nieuwe
leden aan bod kunnen komen, maar de AM moet alleen die onderwerpen kiezen die
relevant zijn voor het gebied waarin de kandidaat werkzaam wil zijn. De
belangrijkste kwaliteit van een kandidaat-ontwikkelaar is dat hij/zij weet waar
hij/zij daarover informatie kan vinden.</p>

<p><a href="./newmaint#Applicant">Kandiddaten</a> moeten ook
<a href="$(DEVEL)/dmup">het beleid van Debian over het gebruik van machines</a>
(Debian Machine Usage Policy - DMUP) lezen en ermee instemmen zich eraan te
houden.</p>

<hr />
#include "$(ENGLISHDIR)/devel/join/nm-steps.inc"
