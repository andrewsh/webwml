#use wml::debian::template title="Specifieke uitgaven van Debian" BARETITLE="true"
#use wml::debian::translation-check translation="6d2e0afb56e760364713c2cca2c9f6407c9a744f"

<p>
Specifieke uitgaven van Debian (Debian Pure Blends in het jargon &mdash; <q>zuivere mengelingen</q> in een letterlijke vertaling ervan) vormen een oplossing voor groepen mensen met specifieke noden.
Zij bieden niet enkel handige collecties (metapakketten) van specifieke pakketten,
maar zij vergemakkelijken ook de installatie en de configuratie in het licht van het beoogde doel.
Zij komen tegemoet aan de belangen van verschillende groepen mensen. Dit kunnen kinderen zijn, wetenschappers, liefhebbers van spelletjes, juristen, medische beroepen, mensen met een visuele beperking, enz.
Zij hebben als gemeenschappelijke bedoeling om voor de beoogde doelgroep de installatie en het beheer van computers te vereenvoudigen, en een band te smeden tussen die doelgroep en de mensen welke de door hen gebruikte software ontwikkelen en verpakken.
</p>

<p>U leest meer over Debian Pure Blends, de specifieke uitgaven van Debian, in de <a
href="https://blends.debian.org/blends/">Pure Blends Manual</a>.</p>

<ul class="toc">
<li><a href="#released">Uitgebrachte specifieke uitgaven</a></li>
<li><a href="#unreleased">Te verwachten specifieke uitgaven</a></li>
<li><a href="#development">Ontwikkeling</a></li>
</ul>

<div class="card" id="released">
<h2>Uitgebrachte specifieke uitgaven</h2>
<div>
<p>"Uitgebracht" kan een verschillende betekenis hebben voor verschillende uitgaven. In de meeste gevallen betekent het dat er voor de uitgave metapakketten bestaan of een installatieprogramma dat in een stabiele (stable) release uitgebracht werd.
Specifieke uitgaven kunnen ook zorgen voor installatiemedia of de basis vormen voor een afgeleide distributie. Raadpleeg de aparte uitgave-pagina's voor bijkomende informatie over een bepaalde specifieke uitgave.</p>

<table class="tabular" summary="">
<tbody>
 <tr>
  <th>Uitgave</th>
  <th>Beschrijving</th>
  <th>Links</th>
 </tr>
#include "../../english/blends/released.data"
</tbody>
</table>
</div>
</div>

<div class="card" id="unreleased">
  <h2>Te verwachten specifieke uitgaven</h2>
  <div>
   <p>Deze uitgaven zijn werk-in-uitvoering en hebben het stadium van een stabiele (stable) uitgave nog niet bereikt, hoewel ze beschikbaar kunnen zijn in de distributie <a
href="https://www.debian.org/releases/testing/">testing</a> (testfase) of <a
href="https://www.debian.org/releases/unstable">unstable</a> (onstabiel). Bepaalde te verwachten specifieke uitgaven kunnen nog steeds steunen op niet-vrije componenten.</p>
  
<table class="tabular" summary="">
<tbody>
 <tr>
  <th>Uitgave</th>
  <th>Beschrijving</th>
  <th>Links</th>
 </tr>
#include "../../english/blends/unreleased.data"
</tbody>
</table>
  </div>
 </div>
 
 <div class="card" id="development">
  <h2>Ontwikkeling</h2>
  <div>
   <p>Indien u geïnteresseerd bent om mee te werken aan de ontwikkeling van specifieke uitgaven van Debian, kunt u informatie over het ontwikkelingswerk vinden op de <a
href="https://wiki.debian.org/DebianPureBlends">wiki-pagina's</a>.
   </p>
  </div>
</div><!-- #main -->

