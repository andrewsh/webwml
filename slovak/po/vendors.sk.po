# Translation of vendors.sk.po
# Copyright (C)
# This file is distributed under the same license as the webwml package.
# Ivan Masár <helix84@centrum.sk>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"Report-Msgid-Bugs-To: debian-www@lists.debian.org\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2009-02-11 23:37+0100\n"
"Last-Translator: Ivan Masár <helix84@centrum.sk>\n"
"Language-Team: Slovak <debian-l10n-slovak@lists.debian.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/CD/vendors/vendors.CD.def:14
msgid "Vendor"
msgstr "Dodávateľ"

#: ../../english/CD/vendors/vendors.CD.def:15
msgid "Allows Contributions"
msgstr "Umožňuje príspevky"

#: ../../english/CD/vendors/vendors.CD.def:16
#, fuzzy
msgid "CD/DVD/BD/USB"
msgstr "CD/DVD"

#: ../../english/CD/vendors/vendors.CD.def:17
msgid "Architectures"
msgstr "Architektúry"

#: ../../english/CD/vendors/vendors.CD.def:18
msgid "Ship International"
msgstr "Medzinárodné dodanie"

#: ../../english/CD/vendors/vendors.CD.def:19
msgid "Contact"
msgstr "Kontakt"

#. ###################
#. Vendor home page link, Debian page link
#: ../../english/CD/vendors/vendors.CD.def:44
msgid "Vendor Home"
msgstr "Domovská stránka dodávateľa"

#: ../../english/CD/vendors/vendors.CD.def:70
msgid "page"
msgstr "stránka"

#: ../../english/CD/vendors/vendors.CD.def:71
msgid "email"
msgstr "email"

#: ../../english/CD/vendors/vendors.CD.def:114
msgid "within Europe"
msgstr "v rámci Európy"

#: ../../english/CD/vendors/vendors.CD.def:118
msgid "To some areas"
msgstr "Do niektorých oblastí"

#: ../../english/CD/vendors/vendors.CD.def:122
msgid "source"
msgstr "zdroj"

#: ../../english/CD/vendors/vendors.CD.def:126
msgid "and"
msgstr "a"

#~ msgid "Architectures:"
#~ msgstr "Architektúry:"

#~ msgid "DVD Type:"
#~ msgstr "Typ DVD:"

#~ msgid "CD Type:"
#~ msgstr "Typ CD:"

#~ msgid "email:"
#~ msgstr "email:"

#~ msgid "Ship International:"
#~ msgstr "Medzinárodné dodanie:"

#~ msgid "Country:"
#~ msgstr "Krajina:"

#~ msgid "Allows Contribution to Debian:"
#~ msgstr "Umožňuje príspevky do Debianu:"

#~ msgid "URL for Debian Page:"
#~ msgstr "URL stránky Debianu:"

#~ msgid "Vendor:"
#~ msgstr "Dodávateľ:"
