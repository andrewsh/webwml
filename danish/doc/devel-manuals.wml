#use wml::debian::ddp title="Debians udviklerhåndbøger"
#use wml::debian::translation-check translation="fca004f92b97a053bc89c121827943f1eae0531b"
#include "$(ENGLISHDIR)/doc/manuals.defs"
#include "$(ENGLISHDIR)/doc/devel-manuals.defs"

<document "Debians fremgangsmådehåndbog" "policy">

<div class="centerblock">
<p>
  Denne håndbog beskriver Debian GNU/Linux-distributionens fremgangsmåder 
  (Policy). Dette inkluderer strukturen og indholdet af Debian-arkivet, flere
  designproblemstillinger vedrørende styresystemet, foruden tekniske krav som
  hver enkelt pakke skal leve op til, for at kunne blive optaget i 
  distributionen.
</p>
<doctable>
  <authors "Ian Jackson, Christian Schwarz, David A. Morris">
  <maintainer "The Debian Policy group">
  <status>
  klar
  </status>
  <availability>
  <inpackage "debian-policy">
  <inddpvcs-debian-policy>
  <p><a href="https://bugs.debian.org/debian-policy">Foreslåede tilføjelser</a>
  til fremgangsmåderne</p>
  <p>Supplerende policy-dokumentation:</p>
  <ul>
    <li><a href="packaging-manuals/fhs/fhs-3.0.html">Filsystemhierarkistandard</a>
    [<a href="packaging-manuals/fhs/fhs-3.0.pdf">PDF</a>]
    [<a href="packaging-manuals/fhs/fhs-3.0.txt">ren tekst</a>]</li>
    <li><a href="debian-policy/upgrading-checklist.html">Opgraderingstjekliste</a>
    <li><a href="packaging-manuals/virtual-package-names-list.yaml">Liste over virtuelle pakkenavne</a></li>
    <li><a href="packaging-manuals/menu-policy/">Menu-policy</a>
    [<a href="packaging-manuals/menu-policy/menu-policy.txt.gz">ren tekst</a>]</li>
    <li><a href="packaging-manuals/perl-policy/">Perl-policy</a>
     [<a href="packaging-manuals/perl-policy/perl-policy.txt.gz">ren tekst</a>]</li>
    <li><a href="packaging-manuals/debconf_specification.html">debconf-specifikation</a></li>
    <li><a href="packaging-manuals/debian-emacs-policy">Emacsen-policy</a></li>
    <li><a href="packaging-manuals/java-policy/">Java-policy</a></li>
    <li><a href="packaging-manuals/python-policy/">Python-policy</a>
    <li><a href="packaging-manuals/copyright-format/1.0/">specifikation af copyright-format</a>
  </ul>
  </availability>
</doctable>
</div>

<hr>

<document "Debians opslagsbog for udviklere" "devref">

<div class="centerblock">
<p>
  Denne håndbog beskriver procedurer og ressourcer for Debian-vedligeholdere.
  Den beskriver hvordan man bliver en ny udvikler, uploadproceduren,
  hvordan man håndterer vores fejlrapporteringssystem, postlister,
  internetservere, osv.
</p>
<p>
  Håndbogen er en <em>opslagsbog</em> for alle Debian-udviklere
  (nybegyndere såvel som garvede).
</p>
<doctable>
  <authors "Ian Jackson, Christian Schwarz, Lucas Nussbaum, Rapha&euml;l Hertzog, Adam Di Carlo, Andreas Barth">
  <maintainer "Lucas Nussbaum, Hideki Yamane, Holger Levsen">
  <status>
  klar
  </status>
  <availability>
  <inpackage "developers-reference">
  <inddpvcs-developers-reference>
  </availability>
</doctable>
</div>

<hr>

<document "Håndbog til Debian-vedligeholdere" "debmake-doc">

<div class="centerblock">
<p>
    Dette øvelsesdokument beskriver over for almindelige brugere og potentielle
    udviklere, hvordan man ved hjælp af kommandoen <code>debmake</code>
    opbygger Debian-pakker.
</p>
<p>
    Der er fokus på den moderne pakningsstil, og der er mange simple eksempler.
</p>
<ul>
<li>pakning af POSIX-shellskript</li>
<li>pakning af Python3-skript</li>
<li>C med Makefile/Autotools/CMake</li>
<li>adskillige binære pakker med delt bibliotek osv.</li>
</ul>
<p>
    <q>Håndbog til Debian-udviklere</q> kan betragtes som efterfølgeren til
    <q>Debians håndbog for nye vedligeholdere</q>.
</p>
<doctable>
  <authors "Osamu Aoki">
  <maintainer "Osamu Aoki">
  <status>
  klar
  </status>
  <availability>
  <inpackage "debmake-doc">
  <inddpvcs-debmake-doc>
  </availability>
</doctable>
</div>

<hr>

<document "Debians vejledning til nye vedligeholdere" "maint-guide">

<div class="centerblock">
<p>
  Dette dokument forsøger at beskrive hvordan en Debian GNU/Linux-pakke
  opbygges, på et sprog så almindelige Debian-brugere (og kommende udviklere)
  kan være med, og med flere fungerende eksempler.
</p>
<p>
  I modsætning til tidligere forsøg, er dette dokument baseret på
  <code>debhelper</code> og de nye værktøjer som er tilgængelige til udviklere.
  Forfatteren har gjort sit yderste for at anvende og sammensmelte tidligere
  beskrivelser.
</p>
<doctable>
  <authors "Josip Rodin, Osamu Aoki">
  <maintainer "Osamu Aoki">
  <status>
  forældet, anvend ovenstående <q>Debians vejledning til nye vedligeholdere</q> (debmake-doc)
  </status>
  <availability>
  <inpackage "maint-guide">
  <inddpvcs-maint-guide>
  </availability>
</doctable>
</div>

<hr>

<document "Introduktion til Debian-pakning" "packaging-tutorial">

<div class="centerblock">
<p>
  Denne tutorial er en introduktion til Debian-pakning.  Den lærer vordende
  udviklere hvordan man ændrer eksisterende pakker, hvordan man opretter sine
  egne pakker og hvordan man kommunikerer med Debian-fællesskabet.  Ud over
  den primære tutorial, er der tre praktiske øvelser i ændring af pakken
  <code>grep</code>, pakning af spillet <code>gnujump</code> og et
  Java-bibliotek.
</p>
<doctable>
  <authors "Lucas Nussbaum">
  <maintainer "Lucas Nussbaum">
  <status>
  klar
  </status>
  <availability>
  <inpackage "packaging-tutorial">
  <inddpvcs-packaging-tutorial>
  </availability>
</doctable>
</div>

<hr>

<document "Debians menusystem" "menu">

<div class="centerblock">
<p>
  Denne vejledning beskriver Debians menusystem og pakken <strong>menu</strong>.
</p>
<p>
  Pakken menu er inspireret af programmet install-fvwm2-menu fra den gamle
  fvwm2-pakke. Men menu prøver at give opbygningen af menuer en mere generel
  brugerflade. Med kommandoen updates-menus i denne pakke, er der ikke
  længere nogen pakker som det er nødvendigt at opdatere for hver enkelt X
  Window-manager, og den giver en fælles brugerflade til både tekst- og
  X-orienterede programmer.
</p>
<doctable>
  <authors "Joost Witteveen, Joey Hess, Christian Schwarz">
  <maintainer "Joost Witteveen">
  <status>
  klar
  </status>
  <availability>
  <inpackage "menu">
  <a href="packaging-manuals/menu.html/">HTML online</a>
  </availability>
</doctable>
</div>

<hr>

<document "Debian Installer internals" "d-i-internals">

<div class="centerblock">
<p>
  Dette dokument har til formål at gøre Debian Installer mere tilgængelig for
  nye udviklere og desuden at være et centralt sted til dokumentering af
  tekniske oplysninger.
</p>
<doctable>
  <authors "Frans Pop">
  <maintainer "Debian Installer-holdet">
  <status>
  klar
  </status>
  <availability>
  <p><a href="https://d-i.debian.org/doc/internals/">HTML online</a>.</p>
  <p><a href="https://salsa.debian.org/installer-team/debian-installer/tree/master/doc/devel/internals">DocBook XML-kildekode online</a></p>
  </availability>
</doctable>
</div>

<hr>

<document "dbconfig-common documentation" "dbconfig-common">

<div class="centerblock">
<p>
  Dette dokument er rettet mod pakkevedligeholdere, som vedligeholder pakker der
  kræver en fungerende database.  I stedet for selv at implementere den krævede
  logik, kan man anvende dbconfig-common til at stille de rette spørgsmål under
  installering, opgradering, genopsætning og afinstallering, samt oprette og
  putte indhold i databasen.
</p>
<doctable>
  <authors "Sean Finney og Paul Gevers">
  <maintainer "Paul Gevers">
  <status>
  klar
  </status>
  <availability>
  <inpackage "dbconfig-common">
  <inddpvcs-dbconfig-common>
  Desuden også <a href="/doc/manuals/dbconfig-common/dbconfig-common-design.html">designdokumentet</a> også tilgængeligt.
  </availability>
</doctable>
</div>

<hr>

<document "dbapp-policy" "dbapp-policy">

<div class="centerblock">
<p>
  Foreslåede retningslinjer for pakker, som er afhængige af en fungerende
  database.
</p>
<doctable>
  <authors "Sean Finney">
  <maintainer "Paul Gevers">
  <status>
  udkast
  </status>
  <availability>
  <inpackage "dbconfig-common">
  <inddpvcs-dbapp-policy>
  </availability>
</doctable>
</div>
