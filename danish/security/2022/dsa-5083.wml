#use wml::debian::translation-check translation="6e870b6792da4e7e1a7e4057c3da1f0e3ca31df8" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Følgende sårbarheder opdaget i webmotoren WebKitGTK:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-22589">CVE-2022-22589</a>

    <p>Heige og Bo Qu opdagede at behandling af en ondsindet fremstillet 
    mailmeddelelse, kunne føre til afvikling af vilkårligt JavaScript.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-22590">CVE-2022-22590</a>

    <p>Toan Pham opdagede at behandling af ondsindet fremstillet webindhold, 
    kunne føre til udførelse af vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-22592">CVE-2022-22592</a>

    <p>Prakash opdagede at behandling af ondsindet fremstillet webindhold, kunne 
    forhindre håndhævelse af Content Security Policy.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-22620">CVE-2022-22620</a>

    <p>En anonum efterforsker opdagede at behandling af ondsindet fremstillet 
    webindhold, kunne føre til udførelse af vilkårlig kode.  Apple er bekendt 
    med en rapport om at dette problem kan have været under aktiv 
    udnyttelse.</p></li>

</ul>

<p>I den gamle stabile distribution (buster), er disse problemer rettet
i version 2.34.6-1~deb10u1.</p>

<p>I den stabile distribution (bullseye), er disse problemer rettet i
version 2.34.6-1~deb11u1.</p>

<p>Vi anbefaler at du opgraderer dine webkit2gtk-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende webkit2gtk, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5083.data"
